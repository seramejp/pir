<html> 
    <body>    

        @if(!empty($results)) 

            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Category</th>
                        <th>SKU</th>
                        <th>Supplier</th>
                        <th>Tab</th>
                        <th>Type</th>
                        <th>Note</th>
                    </tr>
                </thead>
                <tbody>
                    
                        
                        @foreach($results as $thesku)
                            
                                @if(!is_null($thesku->kbs))
                                    @foreach($thesku->kbs as $kb)
                                        <tr>
                                            <td>{{$kb->kbdate}}</td>
                                            <td>{{$thesku->category}}</td>
                                            <td>{{$thesku->sku}}</td>
                                            <td>{{$thesku->supplier}}</td>
                                            <td>KB</td>
                                            <td>{{$kb->issue}}</td>
                                            <td>{{$kb->advice}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            

                            
                                @if(!is_null($thesku->videos))
                                    @foreach($thesku->videos as $vid)
                                        <tr>
                                            <td>{{$vid->viddate}}</td>
                                            <td>{{$thesku->category}}</td>
                                            <td>{{$thesku->sku}}</td>
                                            <td>{{$thesku->supplier}}</td>
                                            <td>Video</td>
                                            <td>{{$vid->type}}</td>
                                            <td>{{$vid->details}}</td>
                                        </tr>
                                    @endforeach
                                @endif


                                @if(!is_null($thesku->pirs))
                                    @foreach($thesku->pirs as $pir)
                                        <tr>
                                            <td>{{$pir->date}}</td>
                                            <td>{{$thesku->category}}</td>
                                            <td>{{$thesku->sku}}</td>
                                            <td>{{$thesku->supplier}}</td>
                                            <td>PIR</td>
                                            <td>{{$pir->type}}</td>
                                            <td>{{$pir->note}}</td>
                                        </tr>
                                    @endforeach
                                @endif


                                @if(!is_null($thesku->emails))
                                    @foreach($thesku->emails as $mail)
                                        <tr>
                                            <td>{{$mail->edate}}</td>
                                            <td>{{$thesku->category}}</td>
                                            <td>{{$thesku->sku}}</td>
                                            <td>{{$thesku->supplier}}</td>
                                            <td>Email</td>
                                            <td>{{$mail->type}}</td>
                                            <td>{{$mail->notes}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            
                        @endforeach
                        
                    
                </tbody>
            </table> 

        @endif  
    </body> 


</html>
