<div class="modal fade" id="modalAddSku" tabindex="-1" role="dialog" aria-labelledby="modalAddLabel" aria-hidden="true" style="position: fixed; top: 20%;">
            
            <div class="modal-dialog">

                <!-- Modal content-->
                
                    <div class="modal-content">
                        <div class="modal-header" >
                            <a role="button" class="pull-right dismiss-modal" data-dismiss="modal">&times;</a>
                            <h5 class="modal-title miakoona-property-title"><i class="fa fa-plus"></i> New SKU</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-10 col-lg-offset-1">
                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">SKU</label>
                                            <input type="text" class="form-control" required="true" id="sku">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Supplier</label>
                                            <input type="text" class="form-control" id="supplier">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Status</label>
                                            <select name="" class="form-control" id="status">
                                                <option value="Active">Active</option>
                                                <option value="Inactive">Inactive</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Category</label>
                                            <div class="input-group">
                                                <select name="" class="form-control categoryselect" required="true" id="category">
                                                   @foreach($categories as $category)
                                                        <option value="{{ $category->name }}">{{ $category->name }}</option>
                                                   @endforeach
                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modalAddCategory">
                                                        <i class="glyphicon glyphicon-plus"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Wise ID</label>
                                            <input type="text" class="form-control" id="wise_id">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Description</label>
                                            <textarea name="" id="desc" cols="15" rows="3" class="form-control"></textarea>
                                        </div>
                                    
                                </div>
                               
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button onclick="addSku();" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                            
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                        </div>

                    </div>
               
            </div>
        </div>
