<div class="modal fade" id="modalAddStaff" tabindex="-1" role="dialog" aria-labelledby="modalAddStaffLabel" aria-hidden="true" style="position: fixed; top:25%;left:9%;">
            
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="width:70%">
            
            <div class="modal-header miakoona-property-title">
                <a role="button" class="pull-right dismiss-modal" data-dismiss="modal">&times;</a>
                <h5 class="modal-title miakoona-property-title"><span class="text-danger"><i class="fa fa-child"></i></span> New Staff</h5>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                            <div class="form-group">
                                <label for="">Staff Name</label>
                                <input type="text" class="form-control" required="true" id="settingaddstaff-name" autofocus>
                            </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-primary" onclick="settingsAddStaff();" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="modalEditStaff" tabindex="-1" role="dialog" aria-labelledby="modalEditStaffLabel" aria-hidden="true" style="position: fixed; top:25%;left:9%;">
            
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="width:70%">
            
            <div class="modal-header miakoona-property-title">
                <a role="button" class="pull-right dismiss-modal" data-dismiss="modal">&times;</a>
                <h5 class="modal-title miakoona-property-title"><span class="text-danger"><i class="fa fa-pencil-o"></i></span> Modify Staff</h5>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                            <div class="form-group">
                                <label for="">Staff Name</label>
                                <input type="text" class="form-control" required="true" id="settingeditstaff-name" autofocus>
                                <input type="text" class="hidden" required="true" id="settingeditstaff-oldname" >
                            </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-primary" onclick="settingsEditStaff();" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>

        </div>
    </div>
</div>