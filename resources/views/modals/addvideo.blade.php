<div class="modal fade" id="mAddVideo" tabindex="-1" role="dialog" aria-labelledby="mAddVideoLabel" aria-hidden="true" >
            
            <div class="modal-dialog">

                <!-- Modal content-->
                
                    <div class="modal-content">
                        <div class="modal-header">
                            <a role="button" class="pull-right dismiss-modal" data-dismiss="modal">&times;</a>
                            <h5 class="modal-title miakoona-property-title"><span class="text-danger"><i class="ti-alert"></i></span> <span class="glyphicon glyphicon-facetime-video"></span> New Video</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-10 col-lg-offset-1">
                                
                                    <form id="vid-form">
                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Date</label>
                                            <input type="date" class="form-control" value="<?php print(date('Y-m-d')); ?>" id="vid-viddate">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Type</label>
                                            <!-- <input type="text" class="form-control" id="pir-type"> -->
                                            <select name="" class="form-control videotype" id="vid-type">
                                                
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Title</label>
                                            <input type="text" class="form-control" value="" id="vid-title">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Status</label>
                                            <!-- <input type="text" class="form-control" id="pir-type"> -->
                                            <select name="" class="form-control videostatus" id="vid-status">
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Staff</label>
                                            <!-- <input type="text" class="form-control" id="pir-staff"> -->
                                            <select name="" class="form-control allstaff" id="vid-staff">
                                                
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Notes</label>
                                            <textarea cols="15" rows="3" class="form-control" id="vid-details"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">File Location</label>
                                            <input type="text" class="form-control" id="vid-filelocation">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Related SKUs</label>
                                            <select id="vid-relatedskus" class="form-control selectpicker" multiple>
                                                @foreach($skus as $sku)
                                                    <option value='{{$sku->id}}'>{{$sku->sku}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Shoot Date</label>
                                            <input type="date" class="form-control" value="<?php print(date('Y-m-d')); ?>" id="vid-shootdate">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Shoot Location</label>
                                            <input type="text" class="form-control" id="vid-shootlocation">
                                        </div>

                                        <div class="form-group">
                                            <label class="checkbox-inline miakoona-property-title">
                                                <input type="checkbox" value="addedtokb" id="vid-addedtokb">Added to KB
                                            </label>
                                            <label class="checkbox-inline miakoona-property-title">
                                                <input type="checkbox" value="manual" id="vid-manual">Manual
                                            </label>
                                            <label class="checkbox-inline miakoona-property-title">
                                                <input type="checkbox" value="customeremail" id="vid-customeremail">Customer Email
                                            </label>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Youtube Link</label>
                                            <input type="text" class="form-control" value="" id="vid-youtubelink">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Youtube </label>
                                            <div class="radio">
                                                <label class="radio-inline"><input type="radio" name="vid-optyoutube" value="unlisted" checked="checked">Unlisted (not public)</label>
                                                <label class="radio-inline"><input type="radio" name="vid-optyoutube" value="public">Public</label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                               
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button onclick="addVideo();" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>

                    </div>
               
            </div>
        </div>