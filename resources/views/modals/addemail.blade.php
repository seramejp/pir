<div class="modal fade" id="mAddEmail" tabindex="-1" role="dialog" aria-labelledby="mAddEmailLabel" aria-hidden="true" >
            
            <div class="modal-dialog">

                <!-- Modal content-->
                
                    <div class="modal-content">
                        <div class="modal-header">
                            <a role="button" class="pull-right dismiss-modal" data-dismiss="modal">&times;</a>
                            <h5 class="modal-title miakoona-property-title"><span class="text-danger"><i class="ti-alert"></i></span> <span class="glyphicon glyphicon-envelope"></span> New Email</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-10 col-lg-offset-1">
                                
                                    <form id="em-form">
                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Date</label>
                                            <input type="date" class="form-control" value="<?php print(date('Y-m-d')); ?>" id="em-date">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Type</label>
                                            <select name="" class="form-control emailtype" id="em-type">
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Staff</label>
                                            <select name="" class="form-control allstaff" id="em-staff">
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Frequency</label>
                                            <input type="text" class="form-control" id="em-frequency">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">File Location</label>
                                            <input type="text" class="form-control" id="em-filelocation">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Related SKUs</label>
                                            <select id="em-relatedskus" class="form-control selectpicker" multiple>
                                                @foreach($skus as $sku)
                                                    <option value='{{$sku->id}}'>{{$sku->sku}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Cease Date</label>
                                            <input type="date" class="form-control" value="<?php print(date('Y-m-d')); ?>" id="em-ceasedate">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Notes</label>
                                            <textarea cols="15" rows="3" class="form-control" id="em-notes"></textarea>
                                        </div>
                                            
                                        
                                    </form>
                                </div>
                               
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button onclick="addEmail();" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>

                    </div>
               
            </div>
        </div>