<div class="modal fade" id="modalForImprovementEdit" tabindex="-1" role="dialog" aria-labelledby="modalForImprovementEditLabel" aria-hidden="true" style="position: fixed; top: 20%;">
            
            <div class="modal-dialog">

                <!-- Modal content-->
                
                    <div class="modal-content">
                        <div class="modal-header">
                            <a role="button" class="pull-right dismiss-modal" data-dismiss="modal">&times;</a>
                            <h5 class="modal-title miakoona-property-title"><span class="text-danger"><i class="fa fa-pencil"></i></span> Edit PIR</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-10 col-lg-offset-1">
                                
                                    
                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Date</label>
                                            <input type="date" class="form-control" value="<?php print(date('Y-m-d')); ?>" id="e-pir-date">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Type</label>
                                            <select name="" class="form-control pirtype" id="e-pir-type">
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Staff</label>
                                            <select name="" class="form-control allstaff" id="e-pir-staff">
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Note</label>
                                            <textarea cols="15" rows="3" class="form-control" id="e-pir-note"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Related SKUs</label>
                                            <select id="e-pir-relatedsku" class="form-control selectpicker" multiple>
                                                @foreach($skus as $sku)
                                                    <option value="{{$sku->id}}">{{$sku->sku}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">File Location</label>
                                            <input type="text" class="form-control" id="e-pir-filelocation">
                                        </div>
                                            
                                        <input type="hidden" id="e-pir-id">
                                    
                                </div>
                               
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button onclick="deletepir('left');" class="btn btn-danger pull-left">Delete</button>
                            <button onclick="saveModifyPirLeft();" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>

                    </div>
               
            </div>
        </div>







<div class="modal fade" id="modalForImprovementEvaluate" tabindex="-1" role="dialog" aria-labelledby="modalForImprovementEvaluateLabel" aria-hidden="true" style="position: fixed; top: 20%;">
            
    <div class="modal-dialog">

        <!-- Modal content-->
        
            <div class="modal-content">
                <div class="modal-header">
                    <a role="button" class="pull-right dismiss-modal" data-dismiss="modal">&times;</a>
                    <h5 class="modal-title miakoona-property-title"><span class="text-danger"><i class="fa fa-pencil-square-o"></i></span> Evaluate PIR</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                        
                                <div class="form-group">
                                    <label for="" class="miakoona-property-title">Date Applied</label>
                                    <input type="date" class="form-control" value="<?php print(date('Y-m-d')); ?>" id="q-pir-date_applied">
                                </div>

                                <div class="form-group">
                                    <label for="" class="miakoona-property-title">Staff</label>
                                    <select name="" class="form-control allstaff" id="q-pir-staff">
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="" class="miakoona-property-title">Note</label>
                                    <textarea cols="15" rows="3" class="form-control" id="q-pir-note"></textarea>
                                </div>


                                <div class="form-group">
                                    <label for="" class="miakoona-property-title">PO #</label>
                                    <input type="text" class="form-control" id="q-pir-po_num">
                                </div>

                                <div class="form-group">
                                    <label for="" class="miakoona-property-title">ETA</label>
                                    <input type="date" class="form-control" value="<?php print(date('Y-m-d')); ?>" id="q-pir-eta">
                                </div>
                                <input type="hidden" id="q-pir-id">
                            
                        </div>
                       
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button onclick="deletepir('right');" class="btn btn-danger pull-left">Delete</button>
                    <button onclick="saveModifyPirRight();" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

            </div>
       
    </div>
</div>