<div class="modal fade" id="mEditKb" tabindex="-1" role="dialog" aria-labelledby="mEditKbLabel" aria-hidden="true" >
            
            <div class="modal-dialog">

                <!-- Modal content-->
                
                    <div class="modal-content">
                        <div class="modal-header">
                            <a role="button" class="pull-right dismiss-modal" data-dismiss="modal">&times;</a>
                            <h5 class="modal-title miakoona-property-title"><span class="text-danger"><i class="ti-alert"></i></span> <span class="glyphicon glyphicon-envelope"></span> Edit KB</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-10 col-lg-offset-1">
                                
                                    <form id="ekb-form">
                                        <input type="text" class="form-control hidden" id="ekb-id">
                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Date</label>
                                            <input type="date" class="form-control" value="<?php print(date('Y-m-d')); ?>" id="ekb-date">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Submitted By</label>
                                            <input type="text" class="form-control" id="ekb-subby">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Ticket ID</label>
                                            <input type="text" class="form-control" id="ekb-ticketid">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Issue</label>
                                            <textarea cols="15" rows="3" class="form-control" id="ekb-issue"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Notes</label>
                                            <textarea cols="15" rows="3" class="form-control" id="ekb-advice"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Related SKUs</label>
                                            <select id="ekb-relatedsku" class="form-control selectpicker" multiple>
                                                @foreach($skus as $sku)
                                                    <option value="{{$sku->id}}">{{$sku->sku}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Manual Link</label>
                                            <input type="text" class="form-control" id="ekb-manlink">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Approved By</label>
                                            <select name="" class="form-control kbapprovedby" id="ekb-approvedby">
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Status</label>
                                            <select name="" class="form-control kbstatus" id="ekb-status">
                                                
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Added by</label>
                                            <input type="text" class="form-control" id="ekb-addedby">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Date Added</label>
                                            <input type="date" class="form-control" value="<?php print(date('Y-m-d')); ?>" id="ekb-dateadded">
                                        </div>

                                    </form>
                                </div>
                               
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button onclick="updateKb();" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>

                    </div>
               
            </div>
        </div>