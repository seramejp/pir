<div class="modal fade" id="mReportDetails_Pir" tabindex="-1" role="dialog" aria-labelledby="mReportDetails_Pir_label" aria-hidden="true" >
            
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #3097D1;">
                            <button type="button" class="close" data-dismiss="modal" style="color: red !important;">&times;</button>
                            <h5 class="modal-title" style="color:white;"></span> <span class="glyphicon glyphicon-folder-open"></span>  Report Details</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-10 col-lg-offset-1">
                                    
                                    <div class="panel panel-info">
                                        <div class="panel-heading">PIR</div>
                                        <div class="panel-body">
                                                <table class="table table-hover">
                                                    <tbody>
                                                            <tr>
                                                                <td class="tdlabel">Date</td>
                                                                <td id="rpir-date" class="tdvalue"></td>
                                                                <td class="tdlabel">Type</td>
                                                                <td id="rpir-type"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdlabel">Staff</td>
                                                                <td class="tdvalue" id="rpir-staff"></td>
                                                                <td class="tdlabel">Note</td>
                                                                <td class="tdvalue" id="rpir-note"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdlabel">File Location</td>
                                                                <td class="tdvalue" colspan="3" id="rpir-filelocation"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdlabel">Related SKUs</td>
                                                                <td class="tdvalue" colspan="3" id="rpir-relatedskus"></td>
                                                            </tr>

                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>


                                    <div class="panel panel-info">
                                        <div class="panel-heading">Evaluated</div>
                                        <div class="panel-body">
                                                <table class="table table-hover">
                                                    <tbody>
                                                            <tr>
                                                                <td class="tdlabel">Date Applied</td>
                                                                <td id="rpir-evaldateapplied" class="tdvalue"></td>
                                                                <td class="tdlabel">ETA</td>
                                                                <td id="rpir-evaleta"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdlabel">PO Num</td>
                                                                <td class="tdvalue" id="rpir-evalponum"></td>
                                                                <td class="tdlabel">Staff</td>
                                                                <td class="tdvalue" id="rpir-evalstaff"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdlabel">Note</td>
                                                                <td class="tdvalue" colspan="3" id="rpir-evalnote"></td>
                                                            </tr>

                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>


                                    <div class="panel-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" style="background-color: #3097D1;">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" href="#collapse_pir">SKU Information</a>
                                                </h4>
                                            </div>

                                            <div id="collapse_pir" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <table class="table table-hover">
                                                            <tbody>
                                                                    <tr>
                                                                        <td class="tdlabelparent">SKU</td>
                                                                        <td class="tdvalueparent" colspan="3" id="parent-sku"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdlabelparent">Description</td>
                                                                        <td class="tdvalueparent" colspan="3" id="parent-description"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdlabelparent">Supplier</td>
                                                                        <td class="tdvalueparent" id="parent-supplier"></td>
                                                                        <td class="tdlabelparent">WISE ID</td>
                                                                        <td class="tdvalueparent" id="parent-wiseid"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdlabelparent">Category</td>
                                                                        <td class="tdvalueparent" id="parent-category"></td>
                                                                        <td class="tdlabelparent">Status</td>
                                                                        <td class="tdvalueparent" id="parent-skustatus"></td>
                                                                    </tr>
                                                                    

                                                            </tbody>
                                                        </table>
                                                        
                                                    </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                               
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="">Close</button>
                        </div>

                    </div>
               
            </div>
        </div>
