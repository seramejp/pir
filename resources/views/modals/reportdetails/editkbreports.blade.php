<div class="modal fade" id="mEditKbReports" tabindex="-1" role="dialog" aria-labelledby="mEditKbReportsLabel" aria-hidden="true" >
            
            <div class="modal-dialog">

                <!-- Modal content-->
                
                    <div class="modal-content">
                        <div class="modal-header">
                            <a role="button" class="pull-right dismiss-modal" data-dismiss="modal">&times;</a>
                            <h5 class="modal-title miakoona-property-title"><span class="text-danger"><i class="ti-alert"></i></span> <span class="glyphicon glyphicon-envelope"></span> Edit KB</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-10 col-lg-offset-1">
                                
                                    <form id="ekbr-form">
                                        <input type="text" class="form-control hidden" id="ekbr-id">
                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Date</label>
                                            <input type="date" class="form-control" value="" id="ekbr-date">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Submitted By</label>
                                            <input type="text" class="form-control" id="ekbr-subby">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Ticket ID</label>
                                            <input type="text" class="form-control" id="ekbr-ticketid">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Issue</label>
                                            <input type="text" class="form-control" id="ekbr-issue">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Notes</label>
                                            <textarea cols="15" rows="3" class="form-control" id="ekbr-advice"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Related SKUs</label>
                                            <select id="ekbr-relatedsku" class="form-control selectpicker" multiple>
                                                @foreach($skus as $sku)
                                                    <option value="{{$sku->id}}">{{$sku->sku}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Manual Link</label>
                                            <input type="text" class="form-control" id="ekbr-manlink">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Approved By</label>
                                            <select name="" class="form-control kbapprovedby" id="ekbr-approvedby">
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Status</label>
                                            <select name="" class="form-control kbstatus" id="ekbr-status">
                                                
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Added by</label>
                                            <input type="text" class="form-control" id="ekbr-addedby">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Date Added</label>
                                            <input type="date" class="form-control" value="<?php print(date('Y-m-d')); ?>" id="ekbr-dateadded">
                                        </div>

                                    </form>
                                </div>
                               
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button onclick="updateKbFromReports();" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>

                    </div>
               
            </div>
        </div>