<div class="modal fade" id="mReportDetails_Video" tabindex="-1" role="dialog" aria-labelledby="mReportDetails_Video_label" aria-hidden="true" >
            
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #3097D1;">
                            <button type="button" class="close" data-dismiss="modal" style="color: red !important;">&times;</button>
                            <h5 class="modal-title" style="color:white;"></span> <span class="glyphicon glyphicon-folder-open"></span>  Report Details</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-10 col-lg-offset-1">
                                    

                                    <div class="panel panel-info">
                                        <div class="panel-heading">Video</div>
                                        <div class="panel-body">
                                                <table class="table table-hover">
                                                    <tbody>
                                                            <tr>
                                                                <td class="tdlabel">Date</td>
                                                                <td id="rvid-viddate" class="tdvalue"></td>
                                                                <td class="tdlabel">Type</td>
                                                                <td id="rvid-type"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdlabel">Staff</td>
                                                                <td class="tdvalue" colspan="3" id="rvid-staff"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdlabel">Notes</td>
                                                                <td class="tdvalue" colspan="3" id="rvid-filelocation"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdlabel">File Location</td>
                                                                <td class="tdvalue" colspan="3" id="rvid-filelocation"></td>
                                                            </tr>

                                                            <tr>
                                                                <td class="tdlabel">Shoot Date</td>
                                                                <td class="tdvalue" id="rvid-shootdate"></td>
                                                                <td class="tdlabel">Shoot Location</td>
                                                                <td class="tdvalue" id="rvid-shootlocation"></td>
                                                            </tr>

                                                            <tr>
                                                                <td class="tdlabel">Added to KB?</td>
                                                                <td class="tdvalue" id="rvid-addedtokb"></td>
                                                                <td class="tdlabel">Manual?</td>
                                                                <td class="tdvalue" id="rvid-manual"></td>
                                                            </tr>

                                                            <tr>
                                                                <td class="tdlabel">Customer Email?</td>
                                                                <td class="tdvalue" id="rvid-customeremail"></td>
                                                                <td class="tdlabel">Youtube</td>
                                                                <td class="tdvalue" id="rvid-youtube"></td>
                                                            </tr>

                                                            <tr>
                                                                <td class="tdlabel">Title</td>
                                                                <td class="tdvalue" id="rvid-title"></td>
                                                                <td class="tdlabel">Status</td>
                                                                <td class="tdvalue" id="rvid-status"></td>
                                                            </tr>

                                                            <tr>
                                                                <td class="tdlabel">Youtube Link</td>
                                                                <td class="tdvalue" colspan="3" id="rvid-youtubelink"></td>
                                                            </tr>

                                                            <tr>
                                                                <td class="tdlabel">Related SKUs</td>
                                                                <td class="tdvalue" colspan="3" id="rvid-relatedskus"></td>
                                                            </tr>

                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>
                                    

                                    <div class="panel-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" style="background-color: #3097D1;">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" href="#collapse_video">SKU Information</a>
                                                </h4>
                                            </div>

                                            <div id="collapse_video" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <table class="table table-hover">
                                                            <tbody>
                                                                    <tr>
                                                                        <td class="tdlabelparent">SKU</td>
                                                                        <td class="tdvalueparent" colspan="3" id="parent-sku"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdlabelparent">Description</td>
                                                                        <td class="tdvalueparent" colspan="3" id="parent-description"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdlabelparent">Supplier</td>
                                                                        <td class="tdvalueparent" id="parent-supplier"></td>
                                                                        <td class="tdlabelparent">WISE ID</td>
                                                                        <td class="tdvalueparent" id="parent-wiseid"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdlabelparent">Category</td>
                                                                        <td class="tdvalueparent" id="parent-category"></td>
                                                                        <td class="tdlabelparent">Status</td>
                                                                        <td class="tdvalueparent" id="parent-skustatus"></td>
                                                                    </tr>
                                                                    

                                                            </tbody>
                                                        </table>
                                                        
                                                    </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                               
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="">Close</button>
                        </div>

                    </div>
               
            </div>
        </div>
