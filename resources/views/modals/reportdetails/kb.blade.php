<div class="modal fade" id="mReportDetails_KB" tabindex="-1" role="dialog" aria-labelledby="mReportDetails_KB_label" aria-hidden="true" >
            
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #3097D1;">
                            <button type="button" class="close" data-dismiss="modal" style="color: red !important;">&times;</button>
                            <h5 class="modal-title" style="color:white;"></span> <span class="glyphicon glyphicon-folder-open"></span>  Report Details</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-10 col-lg-offset-1">
                                    
                                    <div class="panel panel-info">
                                        <div class="panel-heading">KB</div>
                                        <div class="panel-body">
                                                <input type="text" class="hidden" id="rkb-kbid">
                                                <table class="table table-hover">
                                                    <tbody>
                                                            <tr>
                                                                <td class="tdlabel">Date</td>
                                                                <td class="tdvalue" id="rkb-kbdate"></td>
                                                                <td class="tdlabel">Ticket ID</td>
                                                                <td class="tdvalue" id="rkb-ticketid"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdlabel">Submitted by</td>
                                                                <td class="tdvalue" id="rkb-submitted_by"></td>
                                                                <td class="tdlabel">Issue</td>
                                                                <td class="tdvalue" id="rkb-issue"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdlabel">Notes</td>
                                                                <td class="tdvalue" id="rkb-advice"></td>
                                                                <td class="tdlabel">Manual Link</td>
                                                                <td class="tdvalue" id="rkb-manual_link"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdlabel">Approved by</td>
                                                                <td class="tdvalue" id="rkb-approved_by"></td>
                                                                <td class="tdlabel">Status</td>
                                                                <td class="tdvalue" id="rkb-status"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdlabel">Added by</td>
                                                                <td class="tdvalue" id="rkb-added_by"></td>
                                                                <td class="tdlabel">Date added</td>
                                                                <td class="tdvalue" id="rkb-date_added"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdlabel">Related SKUs</td>
                                                                <td class="tdvalue" colspan="3" id="rkb-relatedskus"></td>
                                                            </tr>

                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>


                                    <div class="panel-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" style="background-color: #3097D1;">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" href="#collapse_kb">SKU Information</a>
                                                </h4>
                                            </div>

                                            <div id="collapse_kb" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <table class="table table-hover">
                                                            <tbody>
                                                                    <tr>
                                                                        <td class="tdlabelparent">SKU</td>
                                                                        <td class="tdvalueparent" colspan="3" id="parent-sku"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdlabelparent">Description</td>
                                                                        <td class="tdvalueparent" colspan="3" id="parent-description"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdlabelparent">Supplier</td>
                                                                        <td class="tdvalueparent" id="parent-supplier"></td>
                                                                        <td class="tdlabelparent">WISE ID</td>
                                                                        <td class="tdvalueparent" id="parent-wiseid"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdlabelparent">Category</td>
                                                                        <td class="tdvalueparent" id="parent-category"></td>
                                                                        <td class="tdlabelparent">Status</td>
                                                                        <td class="tdvalueparent" id="parent-skustatus"></td>
                                                                    </tr>
                                                                    

                                                            </tbody>
                                                        </table>
                                                        
                                                    </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                               
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" onclick="showKbUpdateFromReports();" data-dismiss="modal">Modify</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="">Close</button>
                        </div>

                    </div>
               
            </div>
        </div>

