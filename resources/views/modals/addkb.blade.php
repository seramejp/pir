<div class="modal fade" id="mAddKb" tabindex="-1" role="dialog" aria-labelledby="mAddKbLabel" aria-hidden="true" >
            
            <div class="modal-dialog">

                <!-- Modal content-->
                
                    <div class="modal-content">
                        <div class="modal-header">
                            <a role="button" class="pull-right dismiss-modal" data-dismiss="modal">&times;</a>
                            <h5 class="modal-title miakoona-property-title"><span class="text-danger"><i class="ti-alert"></i></span> <span class="glyphicon glyphicon-envelope"></span> New KB</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-10 col-lg-offset-1">
                                
                                    <form id="kb-form">
                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Date</label>
                                            <input type="date" class="form-control" value="<?php print(date('Y-m-d')); ?>" id="kb-date">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Submitted By</label>
                                            <input type="text" class="form-control" id="kb-subby">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Ticket ID</label>
                                            <input type="text" class="form-control" id="kb-ticketid">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Issue</label>
                                            <textarea cols="15" rows="3" class="form-control" id="kb-issue"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Notes</label>
                                            <textarea cols="15" rows="3" class="form-control" id="kb-advice"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Related SKUs</label>
                                            <select id="kb-relatedskus" class="form-control selectpicker" multiple>
                                                @foreach($skus as $sku)
                                                    <option value='{{$sku->id}}'>{{$sku->sku}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Manual Link</label>
                                            <input type="text" class="form-control" id="kb-manlink">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Approved By</label>
                                            <select name="" class="form-control kbapprovedby" id="kb-approvedby">

                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Status</label>
                                            <select name="" class="form-control kbstatus" id="kb-status">
                                                
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Added by</label>
                                            <input type="text" class="form-control" id="kb-addedby">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Date Added</label>
                                            <input type="date" class="form-control" value="1970-01-01" id="kb-dateadded">
                                        </div>

                                    </form>
                                </div>
                               
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button onclick="addKb();" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>

                    </div>
               
            </div>
        </div>