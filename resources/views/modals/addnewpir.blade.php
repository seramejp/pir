<div class="modal fade" id="modalAddPIR" tabindex="-1" role="dialog" aria-labelledby="modalForImprovementLabel" aria-hidden="true" style="position: fixed; top: 20%;">
            
            <div class="modal-dialog">

                <!-- Modal content-->
                
                    <div class="modal-content">
                        <div class="modal-header" >
                            <a role="button" class="pull-right dismiss-modal" data-dismiss="modal">&times;</a>
                            <h5 class="modal-title miakoona-property-title"><span class="text-danger"><i class="fa fa-plus"></i></span> New PIR</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-10 col-lg-offset-1">
                                
                                    
                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Date</label>
                                            <input type="date" class="form-control" value="" id="pir-date">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Type</label>
                                            <select name="" class="form-control pirtype" id="pir-type">
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Staff</label>
                                            <select name="" class="form-control allstaff" id="pir-staff">
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Note</label>
                                            <textarea cols="15" rows="3" class="form-control" id="pir-notes"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Related SKUs</label>
                                            <select id="pir-relatedsku" class="form-control selectpicker" multiple>
                                                @foreach($skus as $sku)
                                                    <option value="{{$sku->id}}">{{$sku->sku}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">File Location</label>
                                            <input type="text" class="form-control" id="pir-filelocation">
                                        </div>
                                            
                                        
                                    
                                </div>
                               
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button onclick="addPIR();" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>

                    </div>
               
            </div>
        </div>