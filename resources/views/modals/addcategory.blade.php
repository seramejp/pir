<div class="modal fade" id="modalAddCategory" tabindex="-1" role="dialog" aria-labelledby="modalAddCategoryLabel" aria-hidden="true" style="position: fixed; top:25%;left:9%;">
            
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="width:70%">
            
            <div class="modal-header" style="background-color: #3097D1;">
                <a role="button" class="pull-right dismiss-modal" data-dismiss="modal">&times;</a>
                <h5 class="modal-title" style="color:white;font-weight:bold"><span class="text-danger"><i class="fa fa-plus"></i></span> New Category</h5>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                            <div class="form-group">
                                <label for="">Category Name</label>
                                <input type="text" class="form-control" required="true" id="e-category" autofocus>
                            </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-primary" onclick="addCategory();" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>

        </div>
    </div>
</div>