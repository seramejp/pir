<div class="modal fade" id="mEditVideo" tabindex="-1" role="dialog" aria-labelledby="mEditVideoLabel" aria-hidden="true" >
            
            <div class="modal-dialog">

                <!-- Modal content-->
                
                    <div class="modal-content">
                        <div class="modal-header">
                            <a role="button" class="pull-right dismiss-modal" data-dismiss="modal">&times;</a>
                            <h5 class="modal-title miakoona-property-title"><span class="text-danger"><i class="ti-alert"></i></span> <span class="glyphicon glyphicon-facetime-video"></span> Edit this video information</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-10 col-lg-offset-1">
                                
                                      <form id="evid-form">
                                        <input type="text" class="form-control hidden" id="evid-vidid">
                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Date</label>
                                            <input type="date" class="form-control" value="" id="evid-viddate">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Type</label>
                                            <select name="" class="form-control videotype" id="evid-type">
                                                <!-- <option value="How-to">How-to</option>
                                                <option value="Promotional">Promotional</option> -->
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Title</label>
                                            <input type="text" class="form-control" value="" id="evid-title">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Status</label>
                                            <select name="" class="form-control videostatus" id="evid-status">
                                                <!-- <option value="Preproduction">Preproduction</option>
                                                <option value="Production">Production</option>
                                                <option value="Post Production">Post Production</option>
                                                <option value="Complete">Complete</option> -->
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Staff</label>
                                            <!-- <input type="text" class="form-control" id="pir-staff"> -->
                                            <select name="" class="form-control allstaff" id="evid-staff">
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Notes</label>
                                            <textarea cols="15" rows="3" class="form-control" id="evid-details"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">File Location</label>
                                            <input type="text" class="form-control" id="evid-filelocation">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Related SKUs</label>
                                            <!-- <input type="text" class="form-control" id="evid-relatedskus"> -->
                                            <select id="evid-relatedskus" class="form-control selectpicker" multiple>
                                                @foreach($skus as $sku)
                                                    <option value="{{$sku->id}}">{{$sku->sku}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Shoot Date</label>
                                            <input type="date" class="form-control" value="" id="evid-shootdate">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Shoot Location</label>
                                            <input type="text" class="form-control" id="evid-shootlocation">
                                        </div>

                                        <div class="form-group">
                                            <label class="checkbox-inline miakoona-property-title">
                                                <input type="checkbox" value="addedtokb" id="evid-addedtokb">Added to KB
                                            </label>
                                            <label class="checkbox-inline miakoona-property-title">
                                                <input type="checkbox" value="manual" id="evid-manual">Manual
                                            </label>
                                            <label class="checkbox-inline miakoona-property-title">
                                                <input type="checkbox" value="customeremail" id="evid-customeremail">Customer Email
                                            </label>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Youtube Link</label>
                                            <input type="text" class="form-control" value="" id="evid-youtubelink">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="miakoona-property-title">Youtube </label>
                                            <div class="radio">
                                                <label class="radio-inline"><input type="radio" name="evid-optyoutube" value="unlisted">Unlisted (not public)</label>
                                                <label class="radio-inline"><input type="radio" name="evid-optyoutube" value="public">Public</label>
                                            </div>
                                        </div>

                                    </form>
                                </div> <!-- end col lg 10 offset 1 -->
                               
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button onclick="updateVideo();" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>

                    </div>
               
            </div>
</div>