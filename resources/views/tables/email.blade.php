<div class="row addthingsbutton">
    <div class="pull-left">
        <a class="btn btn-success buttonAddGeneric hidden" data-toggle="modal" data-target="#mAddEmail"><span class="glyphicon glyphicon-plus"></span> Add Email</a>
    </div>
</div><br>

<table class="table table-hover" id="tableEmail">

    <thead>
        <tr class="tr-header" style="color:black">
            <th>ID</th>
            <th width="8%">Date</th>
            <th>Type</th>
            <th width="10%">Staff</th>
            <th>Frequency</th>
            <th>File Location</th>
            <th>Related SKUs</th>
            <th width="8%">Cease Date</th>
            <th>Notes</th>
        </tr>
    </thead>

    <tbody style="cursor:pointer;">
        
    </tbody>
</table>