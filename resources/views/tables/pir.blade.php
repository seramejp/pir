<div class="row addthingsbutton">
    <div class="pull-left">
        <a class="btn btn-success buttonAddGeneric hidden" data-toggle="modal" data-target="#modalAddPIR"><span class="glyphicon glyphicon-plus"></span> Add PIR</a>
    &emsp;
        <a id="btnSendPIREmail" href="{{url("/pir/notification")}}" class="btn btn-primary" {{ ($pirUnsentEmailCount > 0 ? "" : "disabled='disabled'") }}><span class="glyphicon glyphicon-envelope"></span> Send e-Mail</a>
    </div>
</div><br>

<table class="table table-hover" id="tablePIR">
    <thead>
        <tr class="tr-header" style="color:black">
            <th class="hidden">ID</th>
            <th width="8%">Date</th>
            <th width="8%">Type</th>
            <th width="8%">Staff</th>
            <th>Note</th>
            <th>RelatedSKU</th>
            <th>File Location</th>
            <th width="5%" class="has-border-right"></th>
            <th width="8%">Date Applied</th>
            <th width="8%">Staff</th>
            <th>PO #</th>
            <th>Note</th>
            <th width="8%">ETA</th>
            <th width="5%"></th>
        </tr>
    </thead>

    <tbody style="cursor:pointer;">
        
    </tbody>
</table>

<input type="hidden" id="d-sku">
<input type="hidden" id="d-id">