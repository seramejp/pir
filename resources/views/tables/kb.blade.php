<div class="row addthingsbutton">
    <div class="pull-left">
        <a class="btn btn-success buttonAddGeneric hidden" data-toggle="modal" data-target="#mAddKb"><span class="glyphicon glyphicon-plus"></span> Add KB</a>
    </div>
</div><br>

<table class="table table-hover" id="tableKb">

    <thead>
        <tr class="tr-header" style="color:black">
            <th>ID</th>
            <th width="8%">Date</th>
            <th width="10%">Submitted By</th>
            <th width="10%">Ticket ID</th>
            <th>Issue</th>
            <th>Notes</th>
            <th>Related SKUs</th>
            <th>Manual Link</th>
            <th width="10%">Approved By</th>
            <th>Status</th>
            <th width="10%">Added by</th>
            <th width="8%">Date Added</th>
        </tr>
    </thead>

    <tbody style="cursor:pointer;">
        
    </tbody>
</table>