<div class="row addthingsbutton">
    <div class="pull-left">
        <a class="btn btn-success buttonAddGeneric hidden" data-toggle="modal" data-target="#mAddVideo"><span class="glyphicon glyphicon-plus"></span> Add Video</a>
    </div>
</div><br>

<table class="table table-hover" id="tableVideo">
    <thead>
        <tr class="tr-header" style="color:black">
            <th>ID</th>
            <th width="8%">Date</th>
            <th>Type</th>
            <th>Title</th>
            <th>Status</th>
            <th>Staff</th>
            <th>Notes</th>
            <th>File Location</th>
            <th>Related SKUs</th>
            <th width="8%">Shoot Date</th>
            <th>Shoot Location</th>
            <th width="5%">Added to KB</th>
            <th width="5%">Manual</th>
            <th width="5%">Customer Email</th>
            <th>Youtube Link</th>
            <th width="5%">Youtube</th>
        </tr>
    </thead>

    <tbody style="cursor:pointer;">
        
    </tbody>
</table>