@extends('layouts.main')

@section('main-content')
	<section class="content-header">
            <h1>
                Settings - on going development [Edit Staff DONE]. You can add/edit content here when it's done.
            </h1>
    </section>

    <!-- Main content -->
    <section class="content container-fluid settings">

        <!--------------------------
          | Your Page Content Here |
          -------------------------->
            
            <div class="col-lg-12">
                <div class="row">
                    <div id="divAlertWrapper">
                            
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-6">
                    <h3 class="miakoona-property-title"><a href="" data-toggle="modal" data-target="#modalAddStaff"><i class="fa fa-plus"></i></a> List of all Staff</h3>          
                    <p>Used in: PIR, Video, KB, EMail</p>

                    <div class="col-lg-8">
                        <div class="row">
                            <div class="panel panel-default">
                                <ul class="list-group allstaffli">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <h3 class="miakoona-property-title">PIR Types</h3>          
                    <p>Used in: PIR</p>

                    <div class="col-lg-8">
                        <div class="row">
                            <div class="panel panel-default">
                                <ul class="list-group pirtypeli">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <h3 class="miakoona-property-title">Video Statuses</h3>          
                    <p>Used in: Video</p>

                    <div class="col-lg-8">
                        <div class="row">
                            <div class="panel panel-default">
                                <ul class="list-group videostatusli">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <h3 class="miakoona-property-title">Video Types</h3>          
                    <p>Used in: Video</p>

                    <div class="col-lg-8">
                        <div class="row">
                            <div class="panel panel-default">
                                <ul class="list-group videotypeli">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-6">
                    <h3 class="miakoona-property-title">Who can Approve KB?</h3>          
                    <p>Used in: KB</p>

                    <div class="col-lg-8">
                        <div class="row">
                            <div class="panel panel-default">
                                <ul class="list-group kbapproverli">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <h3 class="miakoona-property-title">KB Statuses</h3>          
                    <p>Used in: KB</p>

                    <div class="col-lg-8">
                        <div class="row">
                            <div class="panel panel-default">
                                <ul class="list-group kbstatusli">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <h3 class="miakoona-property-title">Email Types</h3>          
                    <p>Used in: Email</p>

                    <div class="col-lg-8">
                        <div class="row">
                            <div class="panel panel-default">
                                <ul class="list-group emailtypeli">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            @include("modals.settings.addstaff")

    </section>
@endsection