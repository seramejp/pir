@extends('layouts.main')

@section('main-content')
	<section class="content-header">
            <h1>
                Mails / Notifications / E-Mails
            </h1>
    </section>

    <section class="content container-fluid dashboard">
    		<div class="col-lg-12">
    				<div class="row hidden" id="dashboard-spinner">
	                        <div class="col-lg-12">
	                                <h2 class="text-center"><i class="fa fa-spinner fa-spin"></i></h2>
	                        </div>
	                </div>
    				<!-- TOP : UNSENT -->
    				<div class="row">
    						<div class="panel panel-danger">

								<div class="panel-heading">Pending PIR for Sending Notification</div>
								<div class="panel-body">
										<table class="table table-hover table-striped" id="notification_unsent">
												<thead>
														<tr>
																<th class="hidden">ID</th>
																<th>Date Created</th>
													            <th width="8%">Type</th>
													            <th width="8%">Staff</th>
													            <th>Note</th>
													            <th>RelatedSKU</th>
													            <th>File Location</th>
													            <th></th>
														</tr>
												</thead>

												<tbody>
														@foreach($pirs->where("email_sent", "0")->all() as $sent)
															<tr>
																<td class="hidden">{{$sent->id}}</td>
																<td>{{$sent->date}}</td>
																<td>{{$sent->type}}</td>
																<td>{{$sent->staff}}</td>
																<td>{{$sent->note}}</td>
																<td>{{$sent->relatedsku}}</td>
																<td>{{$sent->filelocation}}</td>
																<td></td>
															</tr>
														@endforeach
												</tbody>
										</table>
								</div>
								
							</div>
    				</div>
					

					<!-- BOTTOM : SENT -->
    				<div class="row">
    						<div class="panel panel-info">

								<div class="panel-heading">PIR with Notification Already Sent</div>
								<div class="panel-body">
										<table class="table table-hover table-striped" id="notification_sent">
												<thead>
														<tr>
																<th class="hidden">ID</th>
																<th>Sent On</th>
																<th>Date Created</th>
													            <th width="8%">Type</th>
													            <th width="8%">Staff</th>
													            <th>Note</th>
													            <th>RelatedSKU</th>
													            <th>File Location</th>
														</tr>
												</thead>

												<tbody style="font-size: 11px;">
														@foreach($pirs->where("email_sent", "1")->all() as $sent)
															<tr>
																<td class="hidden">{{$sent->id}}</td>
																<td>{{ $sent->getLog->created_at or "-" }}</td>
																<td>{{$sent->date}}</td>
																<td>{{$sent->type}}</td>
																<td>{{$sent->staff}}</td>
																<td>{{$sent->note}}</td>
																<td>{{$sent->relatedsku}}</td>
																<td>{{$sent->filelocation}}</td>
															</tr>
														@endforeach
												</tbody>
										</table>
								</div>
								
							</div>
    				</div>


    				

    		</div>
    </section>
@endsection