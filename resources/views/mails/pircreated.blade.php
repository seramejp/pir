@extends("layouts.maillayout")


@section("main-content")
		

		<div class="row">
				<div class="col-lg-12">
						<h1 style="font-family: 'Source Sans Pro',sans-serif;">A New PIR has been created!</h1>
				</div>
		</div>

		<div class="row">
				<div class="col-lg-12">
						<p>The details of the New PIR are as follows:</p>
						<div class="panel panel-info">
                            <div class="panel-heading" style="font-weight: bold;">PIR</div>
	                            <div class="panel-body">
	                                    <table class="table table-hover">
	                                        <tbody>
	                                                <tr>
	                                                    <td class="tdlabel" style="border-top: 1px solid #f4f4f4;">Date</td>
	                                                    <td id="rpir-date" class="tdvalue">{{$pir->date or "undefined"}}</td>
	                                                </tr>
	                                                <tr>
	                                                    <td class="tdlabel" style="border-top: 1px solid #f4f4f4;">Staff</td>
	                                                    <td class="tdvalue" id="rpir-staff" style="border-top: 1px solid #f4f4f4;">{{$pir->staff or "undefined"}}</td>
	                                                </tr>
	                                                <tr>
	                                                	<td class="tdlabel" style="border-top: 1px solid #f4f4f4;">Type</td>
	                                                    <td id="rpir-type" style="border-top: 1px solid #f4f4f4;">{{$pir->type or "undefined"}}</td>
	                                                </tr>
	                                                <tr>
	                                                	<td class="tdlabel" style="border-top: 1px solid #f4f4f4;">Note</td>
	                                                    <td class="tdvalue" id="rpir-note" style="border-top: 1px solid #f4f4f4;">{{$pir->note or "undefined"}}</td>
	                                                </tr>
	                                                <tr>
	                                                    <td class="tdlabel" style="border-top: 1px solid #f4f4f4;">File Location</td>
	                                                    <td class="tdvalue" colspan="3" id="rpir-filelocation" style="border-top: 1px solid #f4f4f4;">{{$pir->filelocation or "undefined"}}</td>
	                                                </tr>
	                                                <tr>
	                                                    <td class="tdlabel" style="border-top: 1px solid #f4f4f4;">Related SKUs</td>
	                                                    <td class="tdvalue" colspan="3" id="rpir-relatedskus" style="border-top: 1px solid #f4f4f4;">{{$pir->relatedsku or "undefined"}}</td>
	                                                </tr>

	                                        </tbody>
	                                    </table>
	                            </div>
                        </div>
                        <br><br><br>
                        <div class="panel-group">
                            <div class="panel panel-primary">
                            	<div class="panel-heading" style="font-weight: bold;">SKU Information</div>
	                            <div class="panel-body">
                                            <table class="table table-hover">
                                                <tbody>
                                                        <tr>
                                                            <td class="tdlabelparent" style="border-top: 1px solid #f4f4f4;">SKU</td>
                                                            <td class="tdvalueparent" colspan="3" id="parent-sku" style="border-top: 1px solid #f4f4f4;">{{$sku->sku or "undefined"}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdlabelparent" style="border-top: 1px solid #f4f4f4;">Description</td>
                                                            <td class="tdvalueparent" colspan="3" id="parent-description" style="border-top: 1px solid #f4f4f4;">{{$sku->desc or "undefined"}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdlabelparent" style="border-top: 1px solid #f4f4f4;">Supplier</td>
                                                            <td class="tdvalueparent" id="parent-supplier" style="border-top: 1px solid #f4f4f4;">{{$sku->supplier or "undefined"}}</td>
                                                        </tr>
                                                        <tr>
                                                        	<td class="tdlabelparent" style="border-top: 1px solid #f4f4f4;">WISE ID</td>
                                                            <td class="tdvalueparent" id="parent-wiseid" style="border-top: 1px solid #f4f4f4;">{{$sku->wise_id or "undefined"}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdlabelparent" style="border-top: 1px solid #f4f4f4;">Category</td>
                                                            <td class="tdvalueparent" id="parent-category" style="border-top: 1px solid #f4f4f4;">{{$sku->category or "undefined"}}</td>
                                                        </tr>
                                                        <tr>
                                                        	<td class="tdlabelparent" style="border-top: 1px solid #f4f4f4;">Status</td>
                                                            <td class="tdvalueparent" id="parent-skustatus" style="border-top: 1px solid #f4f4f4;">{{$sku->status or "undefined"}}</td>
                                                        </tr>
                                                        

                                                </tbody>
                                            </table>
                                            
                                </div>
                            </div>
                        </div>
                       
				</div>
		</div>


		<div class="row">
			<p class="text-danger" style="font-size:10px;">
				<em>*You have received this email because you were registered to this notification for <b style="this.arrow().css(c?&quot;left&quot;: &quot;top&quot;,50*(1-a/b)+&quot;%&quot;).css(c?&quot;top&quot;:&quot;left&quot;,&quot;&quot;);return&quot;bottom&quot;==a?{top: b.top+b.height,left:b.left+b.width/2-c/2;var e={top: 0,left:0;var e=this.$target.scrolltop(),f=this.$element.offset(),g=this.$target.height(): ;if(null!=c&&&quot;top&quot;==this.affixed)return e&lt;c&&&quot;top&quot;: ;if(&quot;bottom&quot;==this.affixed)return null!=c?!(e+this.unpin&lt;=f.top)&&&quot;bottom&quot;: !(e+g&lt;=a-d)&&&quot;bottom&quot;;var h=null==this.affixed,i=h?e: f.top,j=h?g:b;return null!=c&&e&lt;=c?&quot;top&quot;: null!=d&&i+j&gt;=a-d&&&quot;bottom&quot;;">Product Management Register</b>.</em>

			</p>
		</div>
@endsection