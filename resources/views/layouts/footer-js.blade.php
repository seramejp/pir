
<!-- jQuery 3 -->
<script src="{{ asset('adminlte/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('adminlte/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}" type="text/javascript"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
    Both of these plugins are recommended to enhance the
    user experience. -->
<script src="{{ asset('js/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>

<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="{{url('/js/datatables-buttons.js')}}"></script>
<script src="{{url('/js/datatables-buttonshtml5.js')}}"></script>
<script src="https://cdn.datatables.net/select/1.2.4/js/dataTables.select.min.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

<script>
    var globalUrl = "{{url('/')}}";
    var globalToken = "{{ csrf_token() }}";
</script>

<!-- Custom -->
<script src="{{ url('/js/custom.js') }}" type="text/javascript"></script>
<script src="{{ url('/js/customSku.js') }}" type="text/javascript"></script>
<script src="{{ url('/js/customCategory.js') }}" type="text/javascript"></script>
<script src="{{ url('/js/customPir.js') }}" type="text/javascript"></script>
<script src="{{ url('/js/customVideo.js') }}" type="text/javascript"></script>
<script src="{{ url('/js/customKb.js') }}" type="text/javascript"></script>
<script src="{{ url('/js/customEmail.js') }}" type="text/javascript"></script>
<script src="{{ url('/js/customReport.js') }}" type="text/javascript"></script>
<script src="{{ url('/js/customSetting.js') }}" type="text/javascript"></script>

@yield('custom-js')