<!DOCTYPE html>
<html>

    @include('layouts.head')

    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <!-- Main Header -->
            @include('layouts.header')

            <!-- Left side column. contains the logo and sidebar -->
            @include('layouts.left-nav')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @yield('main-content')
            </div>
            <!-- /.content-wrapper -->

            <!-- Main Footer -->
            @include('layouts.footer')

        </div>
        <!-- ./wrapper -->

        <!-- REQUIRED JS SCRIPTS -->
        @include('layouts.footer-js')
        
    </body>
</html>