<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                
            </div>
            <div class="pull-left info">
                <p></p>
                <!-- Status -->
                <a href="#"></i> </a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <!-- Optionally, you can add icons to the links -->
            <li class="{{ Request::path() == '/' ? 'active' : '' }}"> <a href="{{route('Dashboard')}}"> <img src="{{ url("/images/dashboard.svg") }}" /> <span>Dashboard</span> </a> </li>

            <li class="{{ Request::path() == 'reports' ? 'active' : '' }}"><a href="{{ url("/reports") }}"> <img src="{{ asset('images/reports.svg') }}" /> <span>Reports</span> </a> </li>
           
            <li class="{{ Request::path() == 'settings' ? 'active' : '' }}"><a href="{{ url("/settings") }}"> <img src="{{ asset('images/settings.svg') }}" /> <span>Settings</span> </a> </li>
           
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>