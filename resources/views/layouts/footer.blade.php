<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs miakoona-property-title">
        MIT, IT Department. {{ date('Y') }}
    </div>
    <!-- Default to the left -->
    &nbsp;
</footer>

