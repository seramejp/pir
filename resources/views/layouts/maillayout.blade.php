
<!DOCTYPE html><html class=''>
<head>
    <meta charset='UTF-8'>
    <meta name="robots" content="noindex">
    

    <!-- <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css'> -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <!-- <link rel="stylesheet" href="{{ asset('adminlte/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{url("/css/mailcss.css")}}"> -->


</head>

<body>
    <div class="container">
        <div id="enter">
            <div class="logo-image">
                <img src="https://lh3.googleusercontent.com/RpDjuuyn6o4Y5upJZPZY0H8468LKwhieLbr4rh3pNj6LO5oxo9Vn07ozzld7fGTKqNgXYpkKOK6BbXfz3LuC3Lw-H2qUahj4sTm-W5ofkDtIFQ_y4KtHaHI0-Xgx-MQoEYN-eOvclbQV4sFhz3oAg1DOcI-ztphu2SnUzyb_4nifCgBg2Pp-tlm-0mmhNjxgJJtKDmD-PKQjcWlT4sONWlbPiW6X0ez52TV3FRZEzWOGY9sH7iQiA3vqdBCWa8ColQZdLPA5T7dmUzMeN9t3Iy5SiEzA2Ofxprd5AsT-Ru_zGZwxYnpdbY5AwsPWYrtfUOaQuxdIbATQ0mF44kaEWmbWuzfItElzt5QWA96bYgIar3MnkpRgWaRxrodqfmB40D0PS83MRtUUw9Vw5DULb_Irqe6-0LXbFo0g6YBYJgiF2NETHlL2sR_vTvT4pmbB80yEaCUq8srQ-jOfHtN4jrAC7ZeGyGby2RfWVY42a1VojY-sxl98yWfsxTCKI_9WzVIsGC5hZOXt4L5fZPOcCbINYFIe1_wDkI92dR8ggeAuWIHAc4_glCv5r8vxbMRH2CHXpGs7-2ze1aSeaP-_7tvlEhIlGJO88MsLink=w171-h164-no" />
                <img src="https://lh3.googleusercontent.com/a6rXb_zoBGFDzPYA2adgny2KLcubREr6IDG2kDoytuS3e9B54-J4GTnrv6gCoq2kVReS_qKNh8eioc4_N6jfP3TmWDeZYgd71AR95qapAoKhK2I5otVuQclRLXvGeLN1aMDW4MXJ_BAFNFj26UGilDIw5jNcTrjpNTnfdLcipOWUf9xn9idbveiq2bdqJwg5EsawJMfeFVfNvkvkGfufk6UcqLSaHBOWFy2lfgd8B7GUrPBVkAZHd3__gV1bjDkoh_9RAZcKmkrFNhnPxzweHv101fDtFIZqT5-TMK8HjClS1p0zM-20_c6H8RwVC7RZ6viwe8iVgXzCljc8QBRNu_pweOgbgyOqKhsFO-MI_0UXtMq9iG04ODzLH81EQS-bxXGNRe3PUfZ8GBr9eAnsnUFe9EEeq-tV2ZX1wtZSN-2cwxlbsgmVkh5Kt0NvReiiM1Fo7TDn_tPo37kb_U6vSmIgZpjyeVgBS542N8R2aF_ZpfJUjOGfb3_PS2ozRrdKKLDEuXNVC2GoH3G20it_JXRfPjSYxABdmGf-28a9RfjNcVlpW1C37rKmwH6fB2e5HgkpSLuN5qugz22Q6emCblYG3Pkzi0f71bWdyU4=w186-h163-no" alt="Logo" title="Logo" />
                
            </div>
          
          
            <form action="" method="" class="" role="form">
                

                @yield("main-content")
            </form>
        </div>
    </div>

</body>
</html>