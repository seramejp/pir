<header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
                    <img src="{{ url("/images/millsbrands-logo-only.png") }}" class="miakoona-logo" />
                    </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
                    <img src="{{ url("/images/millsbrands-logo-only.png") }}" class="miakoona-logo" />
                    <img src="{{ url("/images/millsbrands-white.png") }}" class="miakoona-logo" />
                    </span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="">
                    <a href="#" class="" aria-expanded="false">
                                <span class="pull-left">
                                    <span class="hidden-xs">{{ config("app.name") }}</span>
                                </span>
                    </a>
                    
                </li>
            </ul>
        </div>
    </nav>
</header>