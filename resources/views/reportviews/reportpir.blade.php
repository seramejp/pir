@extends('layouts.main')

@section('main-content')
        <section class="content-header">
                <h1>
                    Reports
                </h1>
        </section>
        <!-- Search fields -->
        <section class="content container-fluid">
            <div class="container" id="divadvsearch">
                <div class="row">
                    <div class="col-md-12">
                        
                            <div class="input-group" id="adv-search">
                                <input type="text" class="form-control" placeholder="Search SKU" id="search-sku" value="{{ $sku }}" />
                                {{csrf_field()}}
                                <div class="input-group-btn">
                                    <div class="btn-group" role="group">
                                        <div class="dropdown dropdown-lg">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                                            <div class="dropdown-menu dropdown-menu-right" role="menu">
                                                <form role="form" class="form-horizontal">  
                                                    <div class="form-group">
                                                        <label for="filter">Tab</label>
                                                        <select class="form-control" id="search-tab">
                                                            <option value="0">Show All</option>
                                                            <option value="1" selected="">PIR</option>
                                                            <option value="2">Video</option>
                                                            <option value="3">Email</option>
                                                            <option value="4">KB</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="contain">Supplier</label>
                                                        <input class="form-control" type="text" id="search-supplier" value="{{ $supplier }}" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="filter">Category</label>
                                                        <select class="form-control" id="search-category">
                                                            @if(!is_null($categories))
                                                                    <option value="0"> -- Any</option>
                                                                @foreach($categories as $cat)
                                                                    <option value="{{$cat->name}}" {{ ($category==$cat->name ? "selected" : "") }}>{{strtoupper($cat->name)}}</option>
                                                                @endforeach
                                                            @else
                                                                <option value="0">All</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </form> 
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-primary" onclick="searchFromReports();"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="row hidden">
                        <form id="formSearchReportSubmit" method="POST" action="{{url('/reports/search')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="text" id="inputSearchReportSku" name="sku">
                            <input type="text" id="inputSearchReportTab" name="tab">
                            <input type="text" id="inputSearchReportSupplier" name="supplier">
                            <input type="text" id="inputSearchReportCategory" name="category">

                        </form>
                </div>
            </div>

            <div class="table-responsive" style="padding: 40px;">
                <div class="row" id="search-tablewrapper">
                    <div id="loading" class="text-center hidden">
                        <span class="glyphicon glyphicon-repeat fast-right-spinner"></span>
                    </div>
                    <div id="errormessage" class="text-center hidden">
                        <h2><span class="glyphicon glyphicon-warning-sign"></span> Error!</h2>
                        <br><span id="spanerrormessage"></span>
                    </div>
                    
                    <table class="tbl tbl-hovered tbl-striped" id="reportpirmaintable">
                        <thead>
                                <th>SKUID</th>
                                <th>TYPEID</th>
                                <th width="10%">Date</th> 
                                <th width="10%">Category</th>
                                <th width="15%">SKU</th>
                                <th width="12%">Supplier</th>
                                <th width="8%">Tab</th>
                                <th width="10%">Type</th>
                                <!--8--><th>Note</th>

                                <th>Staff</th>
                                <th>File Location</th>
                                <th>Date Applied</th>
                                <th>Staff Responder</th>
                                <th>Note</th>
                                <th>PO Number</th>
                                <!--15--><th>ETA</th>
                                
                        </thead>

                        <tbody style="cursor:pointer;">
                                @if(!empty($result))
                                        @foreach($result as $sku)
                                            @foreach($sku->pirs as $pir)
                                                <tr>
                                                    <td>{{ $sku->id}}</td>
                                                    <td>{{$pir->id}}</td>
                                                    <td>{{$pir->date}}</td>
                                                    <td>{{$sku->category}}</td>
                                                    <td>{{$sku->sku}}</td>
                                                    <td>{{$sku->supplier}}</td>
                                                    <td>{{"PIR"}}</td>
                                                    <td>{{$pir->type}}</td>
                                                    <td>{{$pir->details}}</td>

                                                    <td>{{ $pir->staff }}</td>
                                                    <td>{{ $pir->filelocation }}</td>
                                                    <td>{{ $pir->date_applied }}</td>
                                                    <td>{{ $pir->q_staff }}</td>
                                                    <td>{{ $pir->q_note }}</td>
                                                    <td>{{ $pir->po_num }}</td>
                                                    <td>{{ $pir->eta }}</td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                @endif
                        </tbody>

                        <tfoot>
                            <th> </th>
                            <th> </th>
                            <th>Date</th>
                            <th>Category</th>
                            <th>SKU</th>
                            <th>Supplier</th>
                            <th>Tab</th>
                            <th>Type</th>
                            <th>Note</th>

                            <th>Staff</th>
                            <th>File Location</th>
                            <th>Date Applied</th>
                            <th>Staff Responder</th>
                            <th>Note</th>
                            <th>PO Number</th>
                            <!--15--><th>ETA</th>
                        </tfoot>
                    </table>
                </div>
            </div>
        </section>
        
    @include ('modals.reportdetails.pir')
    
@stop