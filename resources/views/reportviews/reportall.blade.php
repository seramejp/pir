@extends('layouts.main')

@section('main-content')
		<section class="content-header">
                <h1>
                    Reports
                </h1>
        </section>
        
        <section class="content container-fluid">
            <!-- Search fields -->
            <div class="container" id="divadvsearch">
                <div class="row">
                    <div class="col-md-12">
                        
                            <div class="input-group" id="adv-search">
                                <input type="text" class="form-control" placeholder="Search SKU" id="search-sku" value="{{ $sku }}" />
                                {{csrf_field()}}
                                <div class="input-group-btn">
                                    <div class="btn-group" role="group">
                                        <div class="dropdown dropdown-lg">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                                            <div class="dropdown-menu dropdown-menu-right" role="menu">
                                                <form role="form" class="form-horizontal">  
                                                    <div class="form-group">
                                                        <label for="filter">Tab</label>
                                                        <select class="form-control" id="search-tab">
                                                            <option value="0" selected="">Show All</option>
                                                            <option value="1">PIR</option>
                                                            <option value="2">Video</option>
                                                            <option value="3">Email</option>
                                                            <option value="4">KB</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="contain">Supplier</label>
                                                        <input class="form-control" type="text" id="search-supplier" value="{{ $supplier }}" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="filter">Category</label>
                                                        <select class="form-control" id="search-category">
                                                            @if(!is_null($categories))
                                                                    <option value="0"> -- Any</option>
                                                                @foreach($categories as $cat)
                                                                    <option value="{{$cat->name}}" {{ ($category==$cat->name ? "selected" : "") }}>{{strtoupper($cat->name)}}</option>
                                                                @endforeach
                                                            @else
                                                                <option value="0">All</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </form> 
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-primary" onclick="searchFromReports();"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="row hidden">
                        <form id="formSearchReportSubmit" method="POST" action="{{url('/reports/search')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="text" id="inputSearchReportSku" name="sku">
                            <input type="text" id="inputSearchReportTab" name="tab">
                            <input type="text" id="inputSearchReportSupplier" name="supplier">
                            <input type="text" id="inputSearchReportCategory" name="category">

                        </form>
                </div>
            </div>

            <div class="table-responsive" style="padding: 40px;">
                <div class="row" id="search-tablewrapper">
                    <div id="loading" class="text-center hidden">
                        <span class="glyphicon glyphicon-repeat fast-right-spinner"></span>
                    </div>
                    <div id="errormessage" class="text-center hidden">
                        <h2><span class="glyphicon glyphicon-warning-sign"></span> Error!</h2>
                        <br><span id="spanerrormessage"></span>
                    </div>
                    
                    <table class="tbl tbl-hovered tbl-striped" id="reportallmaintable">
                        <thead>
                                <th>SKUID</th>
                                <th>TYPEID</th>
                                <th width="8%">Date</th> 
                                <th width="10%">Category</th>
                                <th width="15%">SKU</th>
                                <th width="10%">Supplier</th>
                                <th width="8%">Tab</th>
                                <th width="10%">Type</th>
                                <!--8--><th>Note</th>

                                <!-- Video -->
                                <th>Staff</th>
                                <th>Details</th>
                                <th>File Location</th>
                                <th>Related SKUs</th>
                                <th>Shoot Date</th>
                                <th>Shoot Location</th>
                                <th>Added to KB</th>
                                <th>Manual</th>
                                <th>Customer Email</th>
                                <th>Youtube</th>
                                <th>Status</th>
                                <th>Title</th>
                                <th>Youtube Link</th>

                                <!-- Email -->
                                <th>Frequency</th>
                                <th>Related SKUs</th>
                                <th>Cease Date</th>

                                <!-- KB -->
                                <th>Submitted By</th>
                                <th>Ticket ID</th>
                                <th>Issue</th>
                                <th>Advice</th>
                                <th>Manual Link</th>
                                <th>Approved By</th>
                                <th>Added By</th>
                                <th>Date Added</th>

                                <!-- PIR -->
                                <th>Date Applied</th>
                                <th>Staff Responder</th>
                                <th>PO Number</th>
                                <th>ETA</th>
                        </thead>

                        <tbody style="cursor:pointer;">
                                @if(!empty($result))
                                        @foreach($result as $sku)

                                            @if(!empty($sku->videos))
                                                @foreach($sku->videos as $vid)
                                                    <tr>
                                                        <td>{{ $sku->id}}</td>
                                                        <td>{{$vid->id}}</td>
                                                        <td>{{$vid->viddate}}</td>
                                                        <td>{{$sku->category}}</td>
                                                        <td>{{$sku->sku}}</td>
                                                        <td>{{$sku->supplier}}</td>
                                                        <td>{{"Video"}}</td>
                                                        <td>{{$vid->type}}</td>
                                                        <td>{{$vid->details}}</td>

                                                        <td>{{ $vid->staff }}</td>
                                                        <td>{{ $vid->details }}</td>
                                                        <td>{{ $vid->filelocation }}</td>
                                                        <td>{{ $vid->relatedskus }}</td>
                                                        <td>{{ $vid->shootdate }}</td>
                                                        <td>{{ $vid->shootlocation }}</td>
                                                        <td>{{ ($vid->addedtokb == "1" ? "Yes" : "No") }}</td>
                                                        <td>{{ ($vid->manual == "1" ? "Yes" : "No") }}</td>
                                                        <td>{{ ($vid->customeremail == "1" ? "Yes" : "No") }}</td>
                                                        <td>{{ $vid->youtube }}</td>
                                                        <td>{{ $vid->status }}</td>
                                                        <td>{{ $vid->title }}</td>
                                                        <td>{{ $vid->youtubelink }}</td>

                                                        <td></td><td></td><td></td><td></td><td></td>
                                                        <td></td><td></td><td></td><td></td><td></td>
                                                        <td></td><td></td><td></td><td></td><td></td>
                                                    </tr>
                                                @endforeach
                                            @endif

                                            @if(!empty($sku->emails))
                                                @foreach($sku->emails as $em)
                                                    <tr>
                                                        <td>{{ $sku->id}}</td>
                                                        <td>{{$em->id}}</td>
                                                        <td>{{$em->edate}}</td>
                                                        <td>{{$sku->category}}</td>
                                                        <td>{{$sku->sku}}</td>
                                                        <td>{{$sku->supplier}}</td>
                                                        <td>{{"Email"}}</td>
                                                        <td>{{$em->type}}</td>
                                                        <td>{{$em->details}}</td>

                                                        <td>{{ $em->staff }}</td>
                                                        <td></td>
                                                        <td>{{ $em->filelocation }}</td>
                                                        <td>{{ $em->relatedskus }}</td>
                                                        <td></td><td></td><td></td><td></td><td></td><td></td>
                                                        <td></td><td></td><td></td>

                                                        <td>{{ $em->frequency }}</td>
                                                        <td>{{ $em->ceasedate }}</td>

                                                        <td></td><td></td><td></td><td></td><td></td><td></td>
                                                        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                                    </tr>
                                                @endforeach
                                            @endif


                                            @if(!empty($sku->kbs))
                                                    @foreach($sku->kbs as $kb)
                                                        <tr>
                                                            <td>{{ $sku->id}}</td>
                                                            <td>{{$kb->id}}</td>
                                                            <td>{{$kb->kbdate}}</td>
                                                            <td>{{$sku->category}}</td>
                                                            <td>{{$sku->sku}}</td>
                                                            <td>{{$sku->supplier}}</td>
                                                            <td>{{"KB"}}</td>
                                                            <td>{{$kb->type}}</td>
                                                            <td>{{$kb->details}}</td>

                                                            <td></td><td></td><td></td><td></td><td></td>
                                                            <td></td><td></td><td></td><td></td><td></td>
                                                            <td>{{ $kb->status }}</td>
                                                            <td></td><td></td>

                                                            <td></td><td></td>
                                                            <td>{{ $kb->submitted_by }}</td>
                                                            <td>{{ $kb->ticket_id }}</td>
                                                            <td>{{ $kb->issue }}</td>
                                                            <td>{{ $kb->advice }}</td>
                                                            <td>{{ $kb->manual_link }}</td>
                                                            <td>{{ $kb->approved_by }}</td>
                                                            <td>{{ $kb->added_by }}</td>
                                                            <td>{{ $kb->date_added }}</td>
                                                            
                                                            <td></td><td></td><td></td><td></td><td></td>
                                                            
                                                        </tr>
                                                    @endforeach
                                            @endif


                                            @if(!empty($sku->pirs))
                                                    @foreach($sku->pirs as $pir)
                                                        <tr>
                                                            <td>{{ $sku->id}}</td>
                                                            <td>{{$pir->id}}</td>
                                                            <td>{{$pir->date}}</td>
                                                            <td>{{$sku->category}}</td>
                                                            <td>{{$sku->sku}}</td>
                                                            <td>{{$sku->supplier}}</td>
                                                            <td>{{"PIR"}}</td>
                                                            <td>{{$pir->type}}</td>
                                                            <td>{{$pir->details}}</td>

                                                            <td>{{ $pir->staff }}</td>
                                                            <td></td>
                                                            <td>{{ $pir->filelocation }}</td>
                                                            <td></td><td></td><td></td><td></td><td></td>
                                                            <td></td><td></td><td></td><td></td><td></td>

                                                            <td></td><td></td><td></td><td></td><td></td>
                                                            <td></td><td></td><td></td><td></td><td></td>

                                                            <td>{{ $pir->date_applied }}</td>
                                                            <td>{{ $pir->q_staff }}</td>
                                                            <td>{{ $pir->q_note }}</td>
                                                            <td>{{ $pir->po_num }}</td>
                                                            <td>{{ $pir->eta }}</td>
                                                        </tr>
                                                    @endforeach
                                            @endif


                                        @endforeach
                                @endif
                        </tbody>

                        <tfoot>
                            <th> </th>
                            <th> </th>
                            <th>Date</th>
                            <th>Category</th>
                            <th>SKU</th>
                            <th>Supplier</th>
                            <th>Tab</th>
                            <th>Type</th>
                            <th>Note</th>

                            <!-- Video -->
                            <th>Staff</th>
                            <th>Details</th>
                            <th>File Location</th>
                            <th>Related SKUs</th>
                            <th>Shoot Date</th>
                            <th>Shoot Location</th>
                            <th>Added to KB</th>
                            <th>Manual</th>
                            <th>Customer Email</th>
                            <th>Youtube</th>
                            <th>Status</th>
                            <th>Title</th>
                            <th>Youtube Link</th>

                            <!-- Email -->
                            <th>Frequency</th>
                            <th>Cease Date</th>

                            <!-- KB -->
                            <th>Submitted By</th>
                            <th>Ticket ID</th>
                            <th>Issue</th>
                            <th>Advice</th>
                            <th>Manual Link</th>
                            <th>Approved By</th>
                            <th>Added By</th>
                            <th>Date Added</th>

                            <!-- PIR -->
                            <th>Date Applied</th>
                            <th>Staff Responder</th>
                            <th>Responder Note</th>
                            <th>PO Number</th>
                            <th>ETA</th>
                        </tfoot>
                    </table>
                </div>
            </div>
        </section>
        
        @include ('modals.reportdetails.video')
        @include ('modals.reportdetails.email')
        @include ('modals.reportdetails.pir')
        @include ('modals.reportdetails.kb')
    
@stop