@extends('layouts.main')

@section('main-content')
	<section class="content-header">
            <h1>
                Dashboard
            </h1>
    </section>

    <!-- Main content -->
    <section class="content container-fluid dashboard">

            <!--------------------------
            | Your Page Content Here |
            -------------------------->

            <div class="container-fluid">
                <div class="row hidden" id="dashboard-spinner">
                        <div class="col-lg-12">
                                <h2 class="text-center"><i class="fa fa-spinner fa-spin"></i></h2>
                        </div>
                </div>
                <div class="row add">
                    <div class="col-lg-12">
                        
                        <div id="divAlertWrapper">
                            
                        </div>


                        <div class="pull-right">
                            <a class="btn btn-success" data-toggle="modal" data-target="#modalAddSku"><span class="glyphicon glyphicon-plus"></span> Add SKU</a>

                        </div>
                    </div>
                </div> <!-- ./row add -->
                    
                <br><br><br>
                <div class="row" id="topdiv">
                    <div class="col-lg-2">
                        <label for="" class="miakoona-property-title">
                            <span id="spanSkuAdd" class="" data-toggle="tooltip" title="Add a new SKU!">
                                <a class="pointonhover dashboard-add-edit" data-toggle="modal" data-target="#modalAddSku"><i class="fa fa-plus"></i></a>
                            </span>

                            <span id="spanSkuEdit" class="hidden" data-toggle="tooltip" title="Edit this SKU?">
                                <a class="pointonhover dashboard-add-edit" onclick="enableModifySku(true);"><i class="fa fa-edit"></i></a>
                            </span>
                             SKU
                        </label>

                        <i id="spinner" class="fa fa-spinner fa-spin spinner" style="opacity: 0;"></i>
                        
                        <div class="input-group">
                            <input type="text" class="form-control " placeholder="Search SKU" id="searchsku" />
                            <span class="input-group-btn">
                                <button class="btn btn-info" id="search-btn" type="button" onclick="searchSku();">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </span>

                        </div>

                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="" class="miakoona-property-title">Description</label>
                            <textarea readonly="true" cols="15" rows="1" class="form-control " id="d-desc"></textarea>
                        </div>
                    </div>
                
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label for="" class="miakoona-property-title">Supplier</label>
                            <input type="text" class="form-control " readonly="true" id="d-supplier">
                        </div>
                    </div>

                    <div class="col-lg-1">
                        <div class="form-group">
                            <label for="" class="miakoona-property-title">Status</label>
                            <select name="" class="form-control " id="d-status" disabled="true">
                                <option value="Active">Active</option>
                                <option value="Inactive">Inactive</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="form-group">
                            <label for="" class="miakoona-property-title">Category</label>
                            <div class="input-group">
                                <select name="" class="form-control categoryselect" required="true" id="d-category" disabled="true">
                                   @foreach($categories as $category)
                                        <option value="{{ $category->name }}">{{ $category->name }}</option>
                                   @endforeach
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modalAddCategory">
                                        <i class="glyphicon glyphicon-plus"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-1">
                        <div class="form-group">
                            <label for="" class="miakoona-property-title">Wise ID</label>
                            <input type="text" class="form-control " readonly="true" id="d-wise_id">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row hidden" id="btnSaveModifiedSku">
                    <div class="col-lg-12">

                        <div class="pull-right">
                            <a class="btn btn-primary" onclick="saveModifySku();"><span class="glyphicon glyphicon-floppy-disk"></span> Save Sku</a>
                        </div>
                    </div>
                </div> <!-- ./row add -->

            </div>

            

        
            <div class="">
                <br>
                <br>
                <br>

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tabPIR">PIR</a></li>
                    <li><a data-toggle="tab" href="#tabVideo">Video</a></li>
                    <li><a data-toggle="tab" href="#tabKb">K B</a></li>
                    <li><a data-toggle="tab" href="#tabEmail">Email</a></li>
                </ul>

                <div class="tab-content">
                    <div id="tabPIR" class="tab-pane fade in active">
                        @include('tables.pir')
                    </div>

                    <div id="tabVideo" class="tab-pane fade">
                        @include('tables.video')
                    </div>

                    <div id="tabKb" class="tab-pane fade">
                        @include('tables.kb')
                    </div>

                    <div id="tabEmail" class="tab-pane fade">
                        @include('tables.email')
                    </div>
                </div>
            </div>

    </section>


    @include("modals.addsku")
    @include("modals.addcategory")
    @include("modals.editpir")
    @include("modals.addnewpir")
    @include("modals.addvideo")
    @include("modals.editvideo")
    @include("modals.editkb")
    @include("modals.addkb")
    @include("modals.addemail")
    @include("modals.editemail")
@endsection