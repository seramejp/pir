<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmaillogs extends Migration
{
    /**
     * Email Logs will be the audit logs for emails. If an email was sent, add one row containing the information
     * of the recipient of the email, timestamps and the PIR ID of the email sent
     * @return void
     */
    public function up()
    {
        Schema::create('emaillogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->nullable();
            $table->string('pir_id')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emaillogs');
    }
}
