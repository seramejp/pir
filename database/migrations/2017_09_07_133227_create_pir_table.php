<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pirs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku_id');
            $table->date('date');
            $table->text('type'); 
            $table->text('staff'); 
            $table->text('note'); 
            $table->text('filelocation')->nullable(); 
            $table->date('date_applied')->nullable(); 
            $table->text('q_staff')->nullable(); 
            $table->text('q_note')->nullable(); 
            $table->text('po_num')->nullable(); 
            $table->date('eta')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
