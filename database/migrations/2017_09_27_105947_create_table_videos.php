<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('viddate')->nullable();
            $table->string('type')->nullable();
            $table->string('staff')->nullable();
            $table->text('details')->nullable();
            $table->string('filelocation')->nullable();
            $table->text('relatedskus')->nullable();
            $table->date('shootdate')->nullable();
            $table->string('shootlocation')->nullable();
            $table->integer('addedtokb')->default(0);
            $table->integer('manual')->default(0);
            $table->integer('customeremail')->default(0);
            $table->string('status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
