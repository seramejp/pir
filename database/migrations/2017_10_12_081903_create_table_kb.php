<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kbs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku_id');
            $table->date('kbdate')->nullable();
            $table->string('submitted_by')->nullable();
            $table->string('ticket_id')->nullable();
            $table->string('issue')->nullable();
            $table->text('advice')->nullable();
            $table->string('manual_link')->nullable();
            $table->string('approved_by')->nullable();
            $table->string('status')->nullable();
            $table->string('added_by')->nullable();
            $table->date('date_added')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
