<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku_id')->nullable();
            $table->date('edate')->nullable();
            $table->string('type')->nullable();
            $table->string('staff')->nullable();
            $table->string('frequency')->nullable();
            $table->string('filelocation')->nullable();
            $table->text('relatedskus')->nullable();
            $table->date('ceasedate')->nullable();
            $table->text('notes')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
