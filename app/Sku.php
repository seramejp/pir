<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sku extends Model
{
   	public function pirs (){
        return $this->hasMany(Pir::class, "sku_id", "id");
    }

    public function videos (){
        return $this->hasMany(Video::class, "sku_id", "id");
    }

    public function kbs (){
        return $this->hasMany(Kb::class, "sku_id", "id");
    }

    public function emails (){
        return $this->hasMany(Email::class, "sku_id", "id");
    }
}
