<?php 

namespace App\Traits;

use App\Transaction;


trait SettingsTrait
{
		
    	/**
     * Create new entry for json file
     * @param String $itemToBeAdded, String $whereToAdd
     * @return void
     */
    public function createEntry ($itemToBeAdded, $whereToAdd){

            $jsonString = file_get_contents(url('/json/jsonFile.json'));
            $data = json_decode($jsonString, true);

            $formdata = array(
                'value'=> ucfirst($itemToBeAdded),
                'text'=> ucfirst($itemToBeAdded)
            );
  
            array_push( $data[$whereToAdd], $formdata );

            $encoded = json_encode($data, JSON_PRETTY_PRINT);

            file_put_contents(public_path('/json/jsonFile.json'), $encoded);


    } # End createEntry



    /**
     * Modify entry for json file
     * @param String $itemToBeModified, String $whereToAdd
     * @return void
     */
    public function modifyEntry ($oldItemName, $itemToBeModified, $whereToAdd){

            $jsonString = file_get_contents(url('/json/jsonFile.json'));
            $data = json_decode($jsonString, true);

            foreach ($data[$whereToAdd] as $key => $eachItem) {
                    if ($eachItem["value"] == $oldItemName) {
                            $data[$whereToAdd][$key]["value"] = $itemToBeModified;
                            $data[$whereToAdd][$key]["text"] = $itemToBeModified;
                    }
            }

            $encoded = json_encode($data, JSON_PRETTY_PRINT);

            file_put_contents(public_path('/json/jsonFile.json'), $encoded);


    } # End createEntry



}