<?php

function flash($type,$msg)
{
	session()->flash('flash_message_type', $type);
	session()->flash('flash_message', $msg);
}