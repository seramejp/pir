<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailLog extends Model
{
    //
   	protected $table = "emaillogs";

   	public function pirs (){
        return $this->belongsTo(Pir::class);
    }
}
