<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\Sku;

class VideoController extends Controller
{
    
	public function ajaxAddVideo(Request $request){

		$newVideo = new Video;
		$newVideo->sku_id = $request->skuid;
		$newVideo->viddate = $request->viddate;
		$newVideo->type = $request->type;
		$newVideo->title = $request->title;
		$newVideo->status = $request->status;
		$newVideo->staff = $request->staff;
		$newVideo->details = $request->details;
		$newVideo->filelocation = $request->filelocation;
		$newVideo->relatedskus = $request->relatedskus;
		$newVideo->shootdate = $request->shootdate;
		$newVideo->shootlocation = $request->shootlocation;
		$newVideo->addedtokb = $request->addedtokb;
		$newVideo->manual = $request->manual;
		$newVideo->customeremail = $request->customeremail;
		$newVideo->youtubelink = $request->youtubelink;
		$newVideo->youtube = $request->youtube;
		$newVideo->save();

		$newVideo->origin_id = $newVideo->id;
		$newVideo->save();
	
		if (substr_count($request->relatedskus, ",") > 0) {
			$haystack = explode(",", $request->relatedskus);

			foreach ($haystack as $value) {
				$relSku = Sku::where("sku", trim($value))->first();

				$anotherVideo = $newVideo->replicate();

				$anotherVideo->sku_id = $relSku->id;
				$anotherVideo->relatedskus .= ", "  . $newVideo->getsku->sku;
				$anotherVideo->relatedskus = str_replace($relSku->sku . ", ", "", $anotherVideo->relatedskus);
				$anotherVideo->save();
			}
		}else{

			if (trim($request->relatedskus) != "") {

				$relSku = Sku::where("sku", trim($request->relatedskus))->first();

				$anotherVideo = $newVideo->replicate();

				$anotherVideo->sku_id = $relSku->id;
				$anotherVideo->relatedskus .= ", "  . $newVideo->getsku->sku;
				$anotherVideo->relatedskus = str_replace($relSku->sku . ", ", "", $anotherVideo->relatedskus);
				$anotherVideo->save();

			}
		}
			

		return $newVideo->id;
	}



	public function ajaxGetVideosForSku(Sku $sku){
		$videos = Video::where("sku_id", $sku->id)
			->orderBy('created_at', 'DESC')
			->get();
		return response()->json([
			'videos' => $videos,		
		]);
	}

	public function ajaxUpdateVideo(Video $vid, Request $request){
		
		/*$vidDelete = Video::where([
					["origin_id",'=',$vid->id],
					["id",'<>',$vid->id],
		])->delete();*/

		$oldrelatedskus = $vid->relatedskus;

		$vid->viddate = $request->viddate;
		$vid->type = $request->type;
		$vid->title = $request->title;
		$vid->status = $request->status;
		$vid->staff = $request->staff;
		$vid->details = $request->details;
		$vid->filelocation = $request->filelocation;
		$vid->relatedskus = $request->relatedskus;
		$vid->shootdate = $request->shootdate;
		$vid->shootlocation = $request->shootlocation;
		$vid->addedtokb = $request->addedtokb;
		$vid->manual = $request->manual;
		$vid->customeremail = $request->customeremail;
		$vid->youtubelink = $request->youtubelink;
		$vid->youtube = $request->youtube;
		//$vid->origin_id = $vid->id;
		$vid->save();

		/*if (substr_count($request->relatedskus, ",") > 0) {
			$haystack = explode(",", $request->relatedskus);

			foreach ($haystack as $value) {
				$relSku = Sku::where("sku", trim($value))->first();

				$anotherVideo = $vid->replicate();

				$anotherVideo->sku_id = $relSku->id;
				$anotherVideo->relatedskus .= ", "  . $vid->getsku->sku;
				$anotherVideo->relatedskus = str_replace($relSku->sku . ", ", "", $anotherVideo->relatedskus);
				$anotherVideo->save();
			}
		}else{

			if (trim($request->relatedskus) != "") {

				$relSku = Sku::where("sku", trim($request->relatedskus))->first();

				if (!is_null($relSku)) {
					$anotherVideo = $vid->replicate();

					$anotherVideo->sku_id = $relSku->id;
					$anotherVideo->relatedskus .= ", "  . $vid->getsku->sku;
					$anotherVideo->relatedskus = str_replace($relSku->sku . ", ", "", $anotherVideo->relatedskus);
					$anotherVideo->save();
				}
			}
				
		}*/

		if (substr_count($request->relatedskus, ",") > 0) {
				$haystack = explode(",", $request->relatedskus);

				if ($oldrelatedskus != $request->relatedskus) {
						foreach ($haystack as $value) {

							if (strpos($oldrelatedskus, $value) !== false) {
								
							}else{
								$relSku = Sku::where("sku", trim($value))->first();

								$anotherObj = $vid->replicate();

								$anotherObj->sku_id = $relSku->id;
								$anotherObj->relatedskus .= ", "  . $vid->sku->sku;
								$anotherObj->relatedskus = str_replace($relSku->sku . ", ", "", $anotherObj->relatedskus);
								$anotherObj->save();   
							}
							
						} #end foreach
				}
					
			}else{

				if (trim($request->relatedskus) != "") {
					
					$relSku = Sku::where("sku", trim($request->relatedskus))->first();
					
					if ($oldrelatedskus != $request->relatedskus) {
						
						if (!is_null($relSku)) {
							$anotherObj = $vid->replicate();

							$anotherObj->sku_id = $relSku->id;
							$anotherObj->relatedskus .= ", "  . $vid->sku->sku;
							$anotherObj->relatedskus = str_replace($relSku->sku . ", ", "", $anotherObj->relatedskus);
							$anotherObj->save();
						}
					}
				}
					
			}
	}


}
