<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Traits\SettingsTrait;

class SettingsController extends Controller
{
    //
    use SettingsTrait;

    public function showsettings(){
    		return view('settings');
    }



    public function addStaffGet($name){
			$this->createEntry($name, "staff");
    }


    public function addStaff(Request $request){
    		$this->createEntry($request->name, "staff");
    }


    public function editStaff(Request $request){
    		$this->modifyEntry($request->oldname, $request->name, "staff");
    }
}
