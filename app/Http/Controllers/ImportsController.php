<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Excel;
use App\Video;
use App\Email;
use App\Sku;
use \Carbon\Carbon;
use Session;

class ImportsController extends Controller
{
    

    public function importfile(Request $request){
    	Excel::load($request->csvfile, function($reader) use ($request) {
    		$noErrors = true;
    		$tocontinue = true;
    		$headersAllowed = array("sku", "date", "type", "title", "staff", "status", "details", "filelocation", "relatedskus", "shootdate", "shootlocation", "addedtokb", "manual", "customeremail", "youtubelink", "youtube");

    		foreach ($reader->first()->keys()->toArray() as $header) {
    			$tocontinue = ( !in_array($header, $headersAllowed) ? false : true );
    			if($tocontinue == false)
    			break;
    		}

    		if ($tocontinue){
    			$failedSkus = "";

    			foreach ($reader->get() as  $row) {

				    	$theSku = Sku::where("sku", trim($row->sku))->first();
				    	
				    	if (!is_null($theSku)) {
				    		$newVideo = new Video;

				    		$newVideo->sku_id = $theSku->id;
							$newVideo->viddate = (!is_null($row->date) ? Carbon::createFromFormat('d/m/Y',$row->date) : Carbon::now()->format('Y-m-d'));
							$newVideo->type = $row->type;
							$newVideo->title = $row->title; // NOT YET MIGRATED
							$newVideo->status = $row->status;
							$newVideo->staff = $row->staff;
							$newVideo->details = $row->details;
							$newVideo->filelocation = $row->filelocation;
							$newVideo->relatedskus = $row->relatedskus;
							$newVideo->shootdate = (!is_null($row->shootdate) ? Carbon::createFromFormat('d/m/Y',$row->shootdate) : Carbon::now()->format('Y-m-d'));
							$newVideo->shootlocation = $row->shootlocation;
							$newVideo->addedtokb = ($row->addedtokb == "yes" ? 1: 0); //maybe use strncmp()
							$newVideo->manual = ($row->manual == "yes" ? 1: 0);
							$newVideo->customeremail = ($row->customeremail == "yes" ? 1: 0);
							$newVideo->youtubelink = $row->youtubelink; // NOT YET MIGRATED
							$newVideo->youtube = ($row->youtube == "public" ? "public": "unlisted");
							$newVideo->save();


							if (substr_count($row->relatedskus, ",") > 0) {
								$haystack = explode(",", $row->relatedskus);

								foreach ($haystack as $value) {
									$relSku = Sku::where("sku", trim($value))->first();
									
									if (!is_null($relSku)) {
										$anotherVideo = $newVideo->replicate();

										$anotherVideo->sku_id = $relSku->id;
										$anotherVideo->relatedskus .= ", "  . $newVideo->getsku->sku;
										$anotherVideo->relatedskus = str_replace($relSku->sku . ", ", "", $anotherVideo->relatedskus);
										$anotherVideo->save();
									}else{
										$failedSkus .= trim($value) . ", ";
									}
	
								}
									
							}else{

								$relSku = Sku::where("sku", trim($row->relatedskus))->first();
								if (!is_null($relSku)) {
									$anotherVideo = $newVideo->replicate();

									$anotherVideo->sku_id = $relSku->id;
									$anotherVideo->relatedskus .= ", "  . $newVideo->getsku->sku;
									$anotherVideo->relatedskus = str_replace($relSku->sku . ", ", "", $anotherVideo->relatedskus);
									$anotherVideo->save();
								}else{
									$failedSkus .= trim($row->relatedskus) . ", ";
								}
							}
				    	}
				}

				if (trim($failedSkus)!="") {
	    			$request->session()->flash('video-import-failed-relatedskus', rtrim(trim($failedSkus), ','));
	    			$noErrors = false;
	    		}

    		}else{
    			$request->session()->flash('video-import-failed-header', 'There are headers unknown to the system. Please remove unnecessary headers and retry the import again.');
    			$noErrors = false;
    		}

    		if ($noErrors) {
    			$request->session()->flash('video-import-success', 'The Import was successful!');
    		}

		}, null, true)->get();

    		
		return redirect("/");
    }




     public function importEmail(Request $request){
    	Excel::load($request->csvfile, function($reader) use ($request) {
    		$noErrors = true;
    		$tocontinue = true;
    		$headersAllowed = array("sku", "date", "type", "staff", "frequency", "filelocation", "relatedskus", "ceasedate", "notes");

    		foreach ($reader->first()->keys()->toArray() as $header) {
    			$tocontinue = ( !in_array($header, $headersAllowed) ? false : true );
    			if($tocontinue == false)
    			break;
    		}

    		if ($tocontinue){
    			$failedSkus = "";

    			foreach ($reader->get() as  $row) {

				    	$theSku = Sku::where("sku", trim($row->sku))->first();
				    	
				    	if (!is_null($theSku)) {
				    		$newEmail = new Email;

				    		$newEmail->sku_id = $theSku->id;
							$newEmail->edate = (!is_null($row->date) ? Carbon::createFromFormat('Y-m-d',$row->date) : Carbon::now()->format('Y-m-d'));
							$newEmail->type = $row->type;
							$newEmail->staff = $row->staff;
							$newEmail->frequency = $row->frequency;
							$newEmail->filelocation = $row->filelocation;
							$newEmail->relatedskus = $row->relatedskus;
							$newEmail->ceasedate = (!is_null($row->ceasedate) ? Carbon::createFromFormat('Y-m-d',$row->ceasedate) : Carbon::now()->format('Y-m-d'));
							$newEmail->notes = $row->notes;
							$newEmail->save();


							if (substr_count($row->relatedskus, ",") > 0) {
								$haystack = explode(",", $row->relatedskus);

								foreach ($haystack as $value) {
									$relSku = Sku::where("sku", trim($value))->first();
									
									if (!is_null($relSku)) {
										$anotherEmail = $newEmail->replicate();

										$anotherEmail->sku_id = $relSku->id;
										$anotherEmail->relatedskus .= ", "  . $newEmail->getsku->sku;
										$anotherEmail->relatedskus = str_replace($relSku->sku . ", ", "", $anotherEmail->relatedskus);
										$anotherEmail->save();
									}else{
										$failedSkus .= trim($value) . ", ";
									}
	
								}
									
							}else{

								$relSku = Sku::where("sku", trim($row->relatedskus))->first();
								if (!is_null($relSku)) {
									$anotherEmail = $newEmail->replicate();

									$anotherEmail->sku_id = $relSku->id;
									$anotherEmail->relatedskus .= ", "  . $newEmail->getsku->sku;
									$anotherEmail->relatedskus = str_replace($relSku->sku . ", ", "", $anotherEmail->relatedskus);
									$anotherEmail->save();
								}else{
									$failedSkus .= trim($row->relatedskus) . ", ";
								}
							}
				    	}
				}

				if (trim($failedSkus)!="") {
	    			$request->session()->flash('video-import-failed-relatedskus', rtrim(trim($failedSkus), ','));
	    			$noErrors = false;
	    		}

    		}else{
    			$request->session()->flash('video-import-failed-header', 'There are headers unknown to the system. Please remove unnecessary headers and retry the import again.');
    			$noErrors = false;
    		}

    		if ($noErrors) {
    			$request->session()->flash('video-import-success', 'The Import was successful!');
    		}

		}, null, true)->get();

    		
		return redirect("/");
    }




}
