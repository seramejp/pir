<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Email;
use App\Sku;

class EmailController extends Controller
{
    public function ajaxAddEmail(Request $request){
		$newEmail = new Email;
		$newEmail->sku_id = $request->skuid;
		$newEmail->edate = $request->edate;
		$newEmail->type = $request->type;
		$newEmail->staff = $request->staff;
		$newEmail->frequency = $request->frequency;
		$newEmail->filelocation = $request->filelocation;
		$newEmail->relatedskus = $request->relatedskus;
		$newEmail->ceasedate = $request->ceasedate;
		$newEmail->notes = $request->notes;
		$newEmail->save();

		$newEmail->origin_id = $newEmail->id;
		$newEmail->save();


		if (substr_count($request->relatedskus, ",") > 0) {
				$haystack = explode(",", $request->relatedskus);

				foreach ($haystack as $value) {
					$relSku = Sku::where("sku", trim($value))->first();

					$anotherObj = $newEmail->replicate();

					$anotherObj->sku_id = $relSku->id;
					$anotherObj->relatedskus .= ", "  . $newEmail->sku->sku;
					$anotherObj->relatedskus = str_replace($relSku->sku . ", ", "", $anotherObj->relatedskus);
					$anotherObj->save();
				}
		}else{

			if (trim($request->relatedskus) != "") {

				$relSku = Sku::where("sku", trim($request->relatedskus))->first();

				if (!is_null($relSku)) {
					$anotherObj = $newEmail->replicate();

					$anotherObj->sku_id = $relSku->id;
					$anotherObj->relatedskus .= ", "  . $newEmail->sku->sku;
					$anotherObj->relatedskus = str_replace($relSku->sku . ", ", "", $anotherObj->relatedskus);
					$anotherObj->save();
				}
			}
				
		}

		return $newEmail->id;
	}




	public function ajaxUpdateEmail(Email $email, Request $request){
		/*$pirDelete = Email::where([
						["origin_id",'=',$email->id],
						["id",'<>',$email->id],
					])->delete();*/

		$oldrelatedskus = $email->relatedskus;

		$email->edate = $request->edate;
		$email->type = $request->type;
		$email->staff = $request->staff;
		$email->frequency = $request->frequency;
		$email->filelocation = $request->filelocation;
		$email->relatedskus = $request->relatedskus;
		$email->ceasedate = $request->ceasedate;
		$email->notes = $request->notes;
		//$email->origin_id = $email->id;
		$email->save();


		/*if (substr_count($request->relatedskus, ",") > 0) {
				$haystack = explode(",", $request->relatedskus);

				foreach ($haystack as $value) {
					$relSku = Sku::where("sku", trim($value))->first();

					$anotherObj = $email->replicate();

					$anotherObj->sku_id = $relSku->id;
					$anotherObj->relatedskus .= ", "  . $email->sku->sku;
					$anotherObj->relatedskus = str_replace($relSku->sku . ", ", "", $anotherObj->relatedskus);
					$anotherObj->save();
				}
		}else{

			if (trim($request->relatedskus) != "") {

				$relSku = Sku::where("sku", trim($request->relatedskus))->first();

				if (!is_null($relSku)) {
					$anotherObj = $email->replicate();

					$anotherObj->sku_id = $relSku->id;
					$anotherObj->relatedskus .= ", "  . $email->sku->sku;
					$anotherObj->relatedskus = str_replace($relSku->sku . ", ", "", $anotherObj->relatedskus);
					$anotherObj->save();
				}
			}
				
		}*/

		if (substr_count($request->relatedskus, ",") > 0) {
				$haystack = explode(",", $request->relatedskus);

				if ($oldrelatedskus != $request->relatedskus) {
						foreach ($haystack as $value) {

							if (strpos($oldrelatedskus, $value) !== false) {
								
							}else{
								$relSku = Sku::where("sku", trim($value))->first();

								$anotherObj = $email->replicate();

								$anotherObj->sku_id = $relSku->id;
								$anotherObj->relatedskus .= ", "  . $email->sku->sku;
								$anotherObj->relatedskus = str_replace($relSku->sku . ", ", "", $anotherObj->relatedskus);
								$anotherObj->save();   
							}
							
						} #end foreach
				}
					
			}else{

				if (trim($request->relatedskus) != "") {
					
					$relSku = Sku::where("sku", trim($request->relatedskus))->first();
					
					if ($oldrelatedskus != $request->relatedskus) {
						
						if (!is_null($relSku)) {
							$anotherObj = $email->replicate();

							$anotherObj->sku_id = $relSku->id;
							$anotherObj->relatedskus .= ", "  . $email->sku->sku;
							$anotherObj->relatedskus = str_replace($relSku->sku . ", ", "", $anotherObj->relatedskus);
							$anotherObj->save();
						}
					}
				}
					
			}
	}



	public function ajaxGetEmailsForSku(Sku $sku){
		$emails = Email::where("sku_id", $sku->id)->get();
		return response()->json([
			'emails' => $emails,		
		]);
	}
}
