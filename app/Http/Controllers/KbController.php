<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kb;
use App\Sku;

class KbController extends Controller
{
    
	public function ajaxAddKb(Request $request){
		$newKb = new Kb;
		$newKb->sku_id = $request->sku_id;
		$newKb->kbdate = $request->date;
		$newKb->submitted_by = $request->submitted_by;
		$newKb->ticket_id = $request->ticket_id;
		$newKb->issue = $request->issue;
		$newKb->advice = $request->advice;
		$newKb->relatedsku = $request->relatedsku;
		$newKb->manual_link = $request->manual_link;
		$newKb->approved_by = $request->approved_by;
		$newKb->status = $request->status;
		$newKb->added_by = $request->added_by;
		$newKb->date_added = $request->date_added;
		$newKb->save();
		$newKb->origin_id = $newKb->id;
		$newKb->save();

		if (substr_count($request->relatedsku, ",") > 0) {
				$haystack = explode(",", $request->relatedsku);

				foreach ($haystack as $value) {
					$relSku = Sku::where("sku", trim($value))->first();

					$anotherObj = $newKb->replicate();

					$anotherObj->sku_id = $relSku->id;
					$anotherObj->relatedsku .= ", "  . $newKb->sku->sku;
					$anotherObj->relatedsku = str_replace($relSku->sku . ", ", "", $anotherObj->relatedsku);
					$anotherObj->save();
				}
		}else{

			if (trim($request->relatedsku) != "") {

				$relSku = Sku::where("sku", trim($request->relatedsku))->first();

				if (!is_null($relSku)) {
					$anotherObj = $newKb->replicate();

					$anotherObj->sku_id = $relSku->id;
					$anotherObj->relatedsku .= ", "  . $newKb->sku->sku;
					$anotherObj->relatedsku = str_replace($relSku->sku . ", ", "", $anotherObj->relatedsku);
					$anotherObj->save();
				}
			}
				
		}
		
		return $newKb->id;
	}

	public function ajaxUpdateKb(Kb $kb, Request $request){

		/*$kbDelete = Kb::where([
						["origin_id",'=',$kb->id],
						["id",'<>',$kb->id],
					])->delete();*/
		$oldRelatedSku = $kb->relatedsku;
		
		$kb->kbdate = $request->edate;
		$kb->submitted_by = $request->submitted_by;
		$kb->ticket_id = $request->ticket_id;
		$kb->issue = $request->issue;
		$kb->advice = $request->advice;
		$kb->relatedsku = $request->relatedsku;
		$kb->manual_link = $request->manual_link;
		$kb->approved_by = $request->approved_by;
		$kb->status = $request->status;
		$kb->added_by = $request->added_by;
		$kb->date_added = $request->date_added;
		//$kb->origin_id = $kb->id;

		$kb->save();


		/*if (substr_count($request->relatedsku, ",") > 0) {
				$haystack = explode(",", $request->relatedsku);

				foreach ($haystack as $value) {
					$relSku = Sku::where("sku", trim($value))->first();

					$anotherObj = $kb->replicate();

					$anotherObj->sku_id = $relSku->id;
					$anotherObj->relatedsku .= ", "  . $kb->sku->sku;
					$anotherObj->relatedsku = str_replace($relSku->sku . ", ", "", $anotherObj->relatedsku);
					$anotherObj->save();
				}
		}else{

			if (trim($request->relatedsku) != "") {

				$relSku = Sku::where("sku", trim($request->relatedsku))->first();

				if (!is_null($relSku)) {
					$anotherObj = $kb->replicate();

					$anotherObj->sku_id = $relSku->id;
					$anotherObj->relatedsku .= ", "  . $kb->sku->sku;
					$anotherObj->relatedsku = str_replace($relSku->sku . ", ", "", $anotherObj->relatedsku);
					$anotherObj->save();
				}
			}
				
		}*/

		if (substr_count($request->relatedsku, ",") > 0) {
				$haystack = explode(",", $request->relatedsku);

				if ($oldRelatedSku != $request->relatedsku) {
						foreach ($haystack as $value) {

							if (strpos($oldRelatedSku, $value) !== false) {	
							}else{
								$relSku = Sku::where("sku", trim($value))->first();

								$anotherObj = $kb->replicate();

								$anotherObj->sku_id = $relSku->id;
								$anotherObj->relatedsku .= ", "  . $kb->sku->sku;
								$anotherObj->relatedsku = str_replace($relSku->sku . ", ", "", $anotherObj->relatedsku);
								$anotherObj->save();   
							}
							
						} #end foreach
				}
					
			}else{

				if (trim($request->relatedsku) != "") {
					
					$relSku = Sku::where("sku", trim($request->relatedsku))->first();
					
					if ($oldRelatedSku != $request->relatedsku) {
						
						if (!is_null($relSku)) {
							$anotherObj = $kb->replicate();

							$anotherObj->sku_id = $relSku->id;
							$anotherObj->relatedsku .= ", "  . $kb->sku->sku;
							$anotherObj->relatedsku = str_replace($relSku->sku . ", ", "", $anotherObj->relatedsku);
							$anotherObj->save();
						}
					}
				}
					
			}
	}

	public function ajaxGetKbsForSku(Sku $sku){
		$kbs = Kb::where("sku_id", $sku->id)->get();
		return response()->json([
			'kbs' => $kbs,		
		]);
	}



	

}
