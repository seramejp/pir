<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Sku;
use App\Pir;
use App\Video;
use App\Email;
use App\Category;
use App\Kb;

use Excel;

class ReportsController extends Controller
{
    
    public function exportvideo(){
    	Excel::create('Template_for_Video', function($excel) {
            $excel->sheet('Sheet1', function($sheet) {
                $sheet->loadView('export.templatevideo');
            });
        })->export('csv');
    }

    public function exportemail(){
    	Excel::create('Template_for_Email', function($excel) {
            $excel->sheet('Sheet1', function($sheet) {
                $sheet->loadView('export.templateemail');
            });
        })->export('csv');
    }



    public function showreports (){
        $skus = Sku::select("id","sku")->orderBy('sku')->get();
        $categories = Category::orderBy("name")->get();
        return view('reports', compact('categories', 'skus'));
    }


    public function search (Request $request){
        //return ($request->sku . ", " . $request->supplier . ", " .  $request->category . ", " .  $request->tab);
        $sku = $request->sku;
        $supplier = $request->supplier;
        $category = $request->category;
        $tab = $request->tab;

        $categories = Category::orderBy("name")->get();
        $skus = Sku::select("id","sku")->orderBy('sku')->get();


        $result = Sku::where(function($query) use ($sku){
                if ($sku != "") {
                    $query->where("sku", $sku);
                }
            })
            ->where(function($query) use ($supplier){
                if ($supplier != "") {
                    $query->where("supplier", $supplier);
                }
            })
            ->where(function($query) use ($category){
                if ($category != "0") {
                    $query->where("category", $category);
                }
            })
            ->get();

            switch($tab){
                    case "0": $result->load("pirs", "videos", "kbs", "emails");
                            return view("reportviews.reportall", compact('result', 'categories'))->withSku($sku)->withSupplier($supplier)->withCategory($category);
                        break;
                    case "1": $result->load("pirs");
                            $result = $result->filter(function($value, $key){
                                return count($value->pirs) > 0;
                            });
                            return view("reportviews.reportpir", compact('result', 'categories'))->withSku($sku)->withSupplier($supplier)->withCategory($category);
                        break;
                    case "2": $result->load("videos");
                            $result = $result->filter(function($value, $key){
                                return count($value->videos) > 0;
                            });
                            //dd($result);
                            return view("reportviews.reportvideo", compact('result', 'categories'))->withSku($sku)->withSupplier($supplier)->withCategory($category);
                        break;
                    case "3": $result->load("emails");
                            $result = $result->filter(function($value, $key){
                                return count($value->emails) > 0;
                            });
                            return view("reportviews.reportemail", compact('result', 'categories'))->withSku($sku)->withSupplier($supplier)->withCategory($category);
                        break;
                    case "4": $result->load("kbs");
                            $result = $result->filter(function($value, $key){
                                return count($value->kbs) > 0;
                            });
                            return view("reportviews.reportkb", compact('result', 'categories', 'skus'))->withSku($sku)->withSupplier($supplier)->withCategory($category);
                        break;
                    default:
                        break;
            }

        /*return response()->json([
            'results' => $result,        
        ]);*/
    }





    public function testsearch (Request $request){

        

            dd($request->sku, $request->tab, $request->supplier, $request->category);

    }




    public function downloadeverything(){
            $sku = "";
            $supplier = "";
            $category = "0";
            $tab = "0";

            $result = Sku::where(function($query) use ($sku){
                if ($sku != "") {
                    $query->where("sku", $sku);
                }
            })
            ->where(function($query) use ($supplier){
                if ($supplier != "") {
                    $query->where("supplier", $supplier);
                }
            })
            ->where(function($query) use ($category){
                if ($category != "0") {
                    $query->where("category", $category);
                }
            })
            ->get();

            switch($tab){
                    case "0": $result->load("pirs", "videos", "kbs", "emails");
                        break;
                    case "1": $result->load("pirs");
                            $result = $result->filter(function($value, $key){
                                return count($value->pirs) > 0;
                            });
                        break;
                    case "2": $result->load("videos");
                            $result = $result->filter(function($value, $key){
                                return count($value->videos) > 0;
                            });
                        break;
                    case "3": $result->load("emails");
                            $result = $result->filter(function($value, $key){
                                return count($value->emails) > 0;
                            });
                        break;
                    case "4": $result->load("kbs");
                            $result = $result->filter(function($value, $key){
                                return count($value->kbs) > 0;
                            });
                        break;
                    default:
                        break;
            }


            Excel::create('PMR_Everything', function($excel) use ($result) {
                $excel->sheet('Sheet1', function($sheet) use ($result) {
                    $sheet->loadView('export.downloadeverything')->with("results", $result);
                });
            })->export('csv');
    }



    public function ajaxGetVideoDetails(Video $vid){
        $result = $vid->load('getsku');
            return response()->json([
                'video' => $result,        
            ]);
    }


    public function ajaxGetPirDetails(Pir $pir){
         $result = $pir->load('sku');
         return response()->json([
                'pir' => $result,        
            ]);
    }


    public function ajaxGetEmailDetails(Email $email){
         $result = $email->load('getsku');
         return response()->json([
                'email' => $result,        
            ]);
    }


    public function ajaxGetKBDetails(Kb $kb){
         $result = $kb->load('getsku');
         return response()->json([
                'kb' => $result,        
            ]);
    }



}
