<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Pir;
use App\Sku;
use App\Recipient;
use App\EmailLog;
use \Carbon\Carbon;


use App\Mail\PIRCreated;
use Illuminate\Support\Facades\Mail;


class PirController extends Controller
{
    public function add(Request $request){
    		
			$newPir = new Pir;
	        $newPir->date = ($request->date == "" ? \Carbon\Carbon::now()->format("Y-m-d") : $request->date);
	        $newPir->type = $request->type;
	        $newPir->staff = $request->staff;
	        $newPir->note = $request->note;
	        $newPir->relatedsku = $request->relatedsku;
	        $newPir->filelocation = $request->filelocation;
	        $newPir->sku_id = $request->sku_id;
	        $newPir->save();

	        $newPir->origin_id = $newPir->id;
	        $newPir->save();

	        if (substr_count($request->relatedsku, ",") > 0) {
				$haystack = explode(",", $request->relatedsku);

				foreach ($haystack as $value) {
					$relSku = Sku::where("sku", trim($value))->first();

					$anotherPir = $newPir->replicate();

					$anotherPir->sku_id = $relSku->id;
					$anotherPir->relatedsku .= ", "  . $newPir->sku->sku;
					$anotherPir->relatedsku = str_replace($relSku->sku . ", ", "", $anotherPir->relatedsku);
					$anotherPir->save();
				}
			}else{

				if (trim($request->relatedsku) != "") {

					$relSku = Sku::where("sku", trim($request->relatedsku))->first();

					if (!is_null($relSku)) {
						$anotherPir = $newPir->replicate();

						$anotherPir->sku_id = $relSku->id;
						$anotherPir->relatedsku .= ", "  . $newPir->sku->sku;
						$anotherPir->relatedsku = str_replace($relSku->sku . ", ", "", $anotherPir->relatedsku);
						$anotherPir->save();
					}
				}
					
			}

	        //Mail::to("millstrading.johnpaul@gmail.com")->send(new PIRCreated($newPir));

	        return $newPir->id;
    }

    public function update(Pir $pir, Request $request){
    		/*$pirDelete = Pir::where([
						["origin_id",'=',$pir->id],
						["id",'<>',$pir->id],
					])->delete();*/

    		$oldRelatedSku = $pir->relatedsku;


    		$pir->date = $request->date;
	    	$pir->type = $request->type;
	    	$pir->staff = $request->staff;
	    	$pir->note = $request->note;
	    	$pir->relatedsku = $request->relatedsku;
	    	$pir->filelocation = $request->filelocation;
	    	//$pir->origin_id = $pir->id;
	        $pir->save();


	        if (substr_count($request->relatedsku, ",") > 0) {
				$haystack = explode(",", $request->relatedsku);

				if ($oldRelatedSku != $request->relatedsku) {
						foreach ($haystack as $value) {

							if (strpos($oldRelatedSku, $value) !== false) {	
							}else{
								$relSku = Sku::where("sku", trim($value))->first();

								$anotherPir = $pir->replicate();

								$anotherPir->sku_id = $relSku->id;
								$anotherPir->relatedsku .= ", "  . $pir->sku->sku;
								$anotherPir->relatedsku = str_replace($relSku->sku . ", ", "", $anotherPir->relatedsku);
								$anotherPir->save();   
							}
							
						} #end foreach
				}
					
			}else{

				if (trim($request->relatedsku) != "") {
					
					$relSku = Sku::where("sku", trim($request->relatedsku))->first();
					
					if ($oldRelatedSku != $request->relatedsku) {
						
						if (!is_null($relSku)) {
							$anotherPir = $pir->replicate();

							$anotherPir->sku_id = $relSku->id;
							$anotherPir->relatedsku .= ", "  . $pir->sku->sku;
							$anotherPir->relatedsku = str_replace($relSku->sku . ", ", "", $anotherPir->relatedsku);
							$anotherPir->save();
						}
					}
				}
					
			}
    }








    public function evaluate(Pir $pir, Request $request){
	    	$pir->date_applied = ($request->date =="" ? null : $request->date );
	        $pir->q_staff = $request->staff;
	        $pir->q_note = $request->note;
	        $pir->po_num = $request->ponum;
	        $pir->eta = ($request->eta=="" ? null : $request->eta);
			$pir->save();
    }


    public function deleteLeftSide(Pir $pir){
    	$pir->delete();
    }

    public function deleteRightSide(Pir $pir){
    	// DO NOT ACTUALLY DELETE THE PIR
    	// JUST CLEAR THE CELLS AND THE DB VALUES

    	$pir->date_applied = null;
    	$pir->q_staff  = null;
    	$pir->q_note  = null;
    	$pir->po_num  = null;
    	$pir->eta  = null;
    	$pir->save();

    }



    public function testEmail(){
    	return view("mails.pircreated");
    }

    public function testSendEmail(){
    	$pir = Pir::find(5);
    	Mail::to(["millstrading.johnpaul@gmail.com"])->send(new PIRCreated("TEST EMAIL for PMR", $pir, $pir->sku));
    	return redirect("/");
    }


    public function testAPI(){
    	return view("mails.test");
    }

    public function updateEmailSentToOne(){
    	$affectedRows = Pir::where("email_sent", "0")->update(["email_sent" => "1"]);

    	$countRecipients = Recipient::count();

    	if ($countRecipients > 0 ) {
    			$defaultRecipient = new Recipient;
		    	$defaultRecipient->email = "millstrading.it.team@gmail.com";
		    	$defaultRecipient->is_active = "1";
		    	$defaultRecipient->save();
    	}
	    	
    	return $affectedRows;
    }

    public function sendEmailNotificationGet(Pir $pir){
    		$recipients = Recipient::where("is_active", "1")->select("email")->get()->pluck("email")->toArray();

    		if (!empty($pir)) {
    				if (!empty($recipients)) {
    						Mail::to($recipients)->send(new PIRCreated("New PIR Created - " . Carbon::now()->format("d-m-Y"), $pir, $pir->sku));
    				}
    		}
    }

    public function sendEmailNotificationPost(Request $request){
    		$return_message = "";
    		$recipients = Recipient::where("is_active", "1")->select("email")->get()->pluck("email")->toArray();
    		$pir = Pir::find($request->pir_id);

    		if (!empty($pir)) {
    				if (!empty($recipients)) {
    						Mail::to($recipients)->send(new PIRCreated("New PIR Created - " . Carbon::now()->format("d-m-Y"), $pir, $pir->sku));

    						$pir->email_sent = 1;
    						$pir->save();

    						foreach ($recipients as $receiver) {
    								$logThisAction =  new EmailLog;

    								$logThisAction->email = $receiver;
    								$logThisAction->pir_id = $pir->id;
    								$logThisAction->save();
    						}

    						
    						$return_message = "success";
    				}else{
    						$return_message = "Error, No Recipients!";
    				}
    		}else{
    			$return_message = "Error, PIR Not Found!";
    		}
    		return $return_message;
    }

    
    public function sendEmailNotificationPostMany(Request $request){
    		$return_message = "";
    		$recipients = Recipient::where("is_active", "1")->select("email")->get()->pluck("email")->toArray();

    		if ($request->pir_ids != "") {
    		
    			$pir_ids = explode(",", $request->pir_ids);


    			foreach($pir_ids as $apir){
    				if ($apir != "") {
    					
			    		$pir = Pir::find($apir);

			    		if (!empty($pir)) {
			    				if (!empty($recipients)) {
			    						Mail::to($recipients)->send(new PIRCreated("New PIR Created - " . Carbon::now()->format("d-m-Y"), $pir, $pir->sku));

			    						$pir->email_sent = 1;
			    						$pir->save();

			    						foreach ($recipients as $receiver) {
			    								$logThisAction =  new EmailLog;

			    								$logThisAction->email = $receiver;
			    								$logThisAction->pir_id = $pir->id;
			    								$logThisAction->save();
			    						}

			    						
			    						$return_message = "success";
			    				}else{
			    						$return_message = "Error, No Recipients!";
			    				}
			    		}else{
			    			$return_message = "Error, PIR Not Found!";
			    		}
			    	} # End pir != ""
		    	} #end foreach
		    } #end pir_ids != ""
    		return $return_message;
    }

    public function showEmailDashboard(){
    		$pirs = Pir::all();

    		$pirs->load("getLog");

    		return view("mails.dashboard", compact("pirs"));
    }







} # End