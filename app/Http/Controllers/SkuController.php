<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Sku;
use App\Category;
use App\Pir;

use DB;

class SkuController extends Controller
{
    public function index()
    {
        $skus = Sku::select("id","sku")->orderBy('sku')->get();
        $categories = Category::orderBy('name')->get();
        $pirUnsentEmailCount = Pir::where("email_sent", "0")->count();

        return view("dashboard", compact("categories", "skus", "pirUnsentEmailCount"));
    }

    public function add(Request $request){
        $newSku = new Sku;
        $newSku->sku = $request->sku;
        $newSku->supplier = $request->supplier;
        $newSku->status = $request->status;
        $newSku->category = $request->category;
        $newSku->wise_id = $request->wiseid;
        $newSku->desc = $request->description;
        $newSku->save();

        return $newSku->id;
    }

    public function update(Sku $sku, Request $request) {
        
        $sku->supplier = $request->supplier;
        $sku->status = $request->status;
        $sku->category = $request->category;
        $sku->wise_id = $request->wiseid;
        $sku->desc = $request->description;
        $sku->save();
    }

    public function search(Request $request){
        
        $skus = Sku::where("sku", $request->skutosearch)->first();

        if (!empty($skus)) {
            $skus->load('pirs', 'kbs', 'emails', 'videos');    
        }
        
        return response()->json([
            'skus' => $skus       
        ]);

    }

}


//JJ1n9c0_155