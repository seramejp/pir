<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;

class CategoryController extends Controller
{
    public function add()
    {
    	$category = $_POST["category"];

    	$newCategory = new Category;
    	$newCategory->name = $category;

        $newCategory->save();
		    	
		$response_array['message'] = "ok";
		header('Content-type: application/json');
	    echo json_encode($response_array);
    }
}
