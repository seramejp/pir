<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kb extends Model
{
    public function getsku (){
        return $this->belongsTo(Sku::class, "sku_id", "id");
    }


    public function sku (){
        return $this->belongsTo(Sku::class, "sku_id", "id");
    }
}
