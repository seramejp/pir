<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Pir;
use App\Sku;

class PIRCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $pir;
    public $sku;
    public $subject = "A New PIR has been created!";

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, Pir $pir, Sku $sku)
    {
        $this->subject = $subject;
        $this->pir = $pir;
        $this->sku = $sku;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)->view('mails.pircreated');
    }
}
