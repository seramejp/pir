<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pir extends Model
{
    public function sku (){
        return $this->belongsTo(Sku::class);
    }

    public function getLog(){
    		return $this->hasOne(EmailLog::class, "pir_id", "id");
    }
}
