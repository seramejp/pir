<?php

use Illuminate\Support\Facades\Redis;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get("/", "SkuController@index")->name('Dashboard');

Route::post("/sku/add", "SkuController@add");
Route::patch("/sku/{sku}/update", "SkuController@update");



Route::post("/sku/search", "SkuController@search");

Route::post("/category/add", "CategoryController@add");

Route::post("/pir/add", "PirController@add");

Route::patch("/pir/{pir}/update", "PirController@update");

Route::patch("/pir/{pir}/evaluate", "PirController@evaluate");

Route::post("/pir/delete/leftside/{pir}", "PirController@deleteLeftSide");
Route::post("/pir/delete/rightside/{pir}", "PirController@deleteRightSide");

Route::post("/video/add", "VideoController@ajaxAddVideo");
Route::patch("/video/update/{vid}", "VideoController@ajaxUpdateVideo");
Route::get("/videos/getvideos/{sku}", "VideoController@ajaxGetVideosForSku");

Route::post("/email/add", "EmailController@ajaxAddEmail");
Route::patch("/email/update/{email}", "EmailController@ajaxUpdateEmail");
Route::get("/email/getemails/{sku}", "EmailController@ajaxGetEmailsForSku");



Route::get('/export/template/video', 'ReportsController@exportvideo');
Route::get('/export/template/email', 'ReportsController@exportemail');
Route::post('/import/video', 'ImportsController@importfile');
Route::post('/import/email', 'ImportsController@importEmail');

Route::post("/kb/add", "KbController@ajaxAddKb");
Route::patch("/kb/update/{kb}", "KbController@ajaxUpdateKb");
Route::get("/kb/getkbs/{sku}", "KbController@ajaxGetKbsForSku");


Route::get("/reports", "ReportsController@showreports");
Route::post("/reports/search", "ReportsController@search");
Route::get("/reports/search/everything", "ReportsController@downloadeverything");
Route::get("/reports/testsearch", "ReportsController@testsearch");

Route::get("/reports/details/video/{vid}", "ReportsController@ajaxGetVideoDetails");
Route::get("/reports/details/pir/{pir}", "ReportsController@ajaxGetPirDetails");
Route::get("/reports/details/email/{email}", "ReportsController@ajaxGetEmailDetails");
Route::get("/reports/details/kb/{kb}", "ReportsController@ajaxGetKBDetails");



Route::get("/settings", "SettingsController@showsettings");
Route::get("/settings/add/staff/{name}", "SettingsController@addStaffGet"); #Just for testing
Route::post("/settings/add/staff", "SettingsController@addStaff"); 
Route::patch("/settings/edit/staff", "SettingsController@editStaff"); 

Route::get("/pir/notification/send/{pir}", "PirController@sendEmailNotificationGet");
Route::post("/pir/notification/send", "PirController@sendEmailNotificationPost");
Route::post("/pir/notification/sendmany", "PirController@sendEmailNotificationPostMany");
Route::get("/pir/notification", "PirController@showEmailDashboard");


# TESTS
Route::get("/mailing/test", "PirController@testEmail");
Route::get("/mailing/send", "PirController@testSendEmail");
Route::get("/test/api", "PirController@testAPI");
Route::get("/pir/{pir}/updatetest", "PirController@updatexx");
Route::get("/pir/mailing/email/sent/1", "PirController@updateEmailSentToOne"); # Run this route after migration to set all email_sent fields to 1. This way, all past entries will be tagged as email sent.