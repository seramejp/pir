



function setModalEventAddPIR(){
        $("#modalAddPIR").on('show.bs.modal', function(event) {
                $("#pir-date").val(formatDate(utc)); 
                $("#modalAddPIR #pir-relatedsku").selectpicker("deselectAll");
        });
}


function setModalEventEditPIR(){
	
	$("#modalForImprovementEdit").on('show.bs.modal', function(event) {
            let anchorTag = $(event.relatedTarget);
            
            $("#e-pir-date").val(anchorTag.data("pirdate"));
            $("#e-pir-type option[value='"+anchorTag.data("pirtype")+"']").prop("selected", "selected");
            $("#e-pir-staff option[value='"+anchorTag.data("pirstaff")+"']").prop("selected", "selected");
            $("#e-pir-note").val(anchorTag.data("pirnote"));

            setRelatedSkuSelectedItems(anchorTag.data("pirrelatedsku"), $("#e-pir-relatedsku"));

            $("#e-pir-filelocation").val(anchorTag.data("pirfilelocation"));
            $("#e-pir-id").val(anchorTag.data("pirid"));

    });


    $("#modalForImprovementEvaluate").on('show.bs.modal', function(event) {
    		let anchorTag = $(event.relatedTarget);

    		$("#q-pir-date_applied").val(anchorTag.data("pirdate"));
			$("#q-pir-staff option[value='"+anchorTag.data("pirstaff")+"']").prop("selected", "selected");
			$("#q-pir-note").val(anchorTag.data("pirnote"));
			$("#q-pir-po_num").val(anchorTag.data("pirponum"));
			$("#q-pir-eta").val(anchorTag.data("pireta"));
			$("#q-pir-id").val(anchorTag.data("pirid"));
    	
    });
}


function saveModifyPirLeft(){
		let date = $("#e-pir-date").val();
	    let type = $("#e-pir-type").val();
	    let staff = $("#e-pir-staff").val();
	    let note = $("#e-pir-note").val();
	    let filelocation = $("#e-pir-filelocation").val();
	    let pirid = $("#e-pir-id").val();
	    let relatedskus = getRelatedSkuItemsToBeSentToDb("e-pir-relatedsku");

	    $.ajax({
		    	url: globalUrl+"/pir/"+pirid+"/update",
		    	type: 'POST',
		    	data: {
		    		_token: globalToken,
                    _method: 'PATCH',
                    date: date,
                    type: type,
                    staff: staff,
                    note: note,
                    relatedsku: relatedskus,
                    filelocation: filelocation,
		    	},
	    })
	    .done(function() {
	    	console.log("success modifying pir left side details");
            
            var d = tablePIR.row(pirRowIndx).data();
                    d[1] = date;
                    d[2] = type;
                    d[3] = staff;
                    d[4] = note;
                    d[5] = relatedskus;
                    d[6] = filelocation;
            tablePIR.row(pirRowIndx).data(d).draw();

	    	showMessage("alert-success", "Modified PIR details. Date: " + date + "; Type: " + type + "; Staff: " + staff + "; Note: " + note + "; Related SKU/s: " + relatedskus + "; File Location: " + filelocation );
	    })
	    .fail(function() {
	    	console.log("error modifying pir details");
	    	showMessage("alert-danger", "Failed in modifying PIR details. Please contact IT. Error PIR ID: " +pirid);
	    })
	    .always(function() {

	    	$("#e-pir-date").val("");
			$("#e-pir-type option").removeAttr("selected");
			$("#e-pir-staff option").removeAttr("selected");
			$("#e-pir-note").val("");
			$("#e-pir-filelocation").val("");
			$("#e-pir-id").val();
			
	    });
	    
}


function deletepir(side){

        swal({
                title: "Are you sure you want to delete this PIR? This cannot be undone.",
                icon: "warning",
                buttons: {
                    delete: "Delete, I understand.",
                    cancel: "Nooo pls dont.",
                },
                dangerMode: true,
        })
        .then((value) => {
            if (value == "delete") {
                if (side == "left") {
                    let pirId = $("#e-pir-id").val();

                    $("#modalForImprovementEdit").modal('toggle');
                    
                    $.post(globalUrl + '/pir/delete/leftside/' + pirId, 
                        {
                            _token: globalToken,
                            _method: 'POST',
                        }, 
                        function(data, textStatus, xhr) {
                            //var table = $("#tablePIR").DataTable();
                            tablePIR.row(":eq("+ pirRowIndx +")").remove();
                            tablePIR.draw();

                            swal("Deleted!", "PIR was deleted.", "success");      

                            $("#e-pir-date").val("");
							$("#e-pir-type option").removeAttr("selected");
							$("#e-pir-staff option").removeAttr("selected");
							$("#e-pir-note").val("");
							$("#e-pir-filelocation").val("");
							$("#e-pir-id").val();     
                    });

                } else {
                    let pirId = $("#q-pir-id").val();

                    $("#modalForImprovementEvaluate").modal('toggle');

                    $.post(globalUrl + '/pir/delete/rightside/' + pirId, 
                        {
                            _token: globalToken,
                            _method: 'POST',
                        }, 
                        function(data, textStatus, xhr) {

                            var d = tablePIR.row(pirRowIndx).data();
                            d[8] = d[9] = d[10] = d[11] = d[12] = "";
                            tablePIR.row(pirRowIndx).data(d).draw();

                            swal("Deleted!", "PIR was deleted.", "success");  

                            $("#q-pir-date_applied").val("");
							$("#q-pir-staff option").removeAttr("selected");
							$("#q-pir-note").val("");
							$("#q-pir-po_num").val("");
							$("#q-pir-eta").val("");
							$("#q-pir-id").val("");         
                    });
                }
            }
        });

}



function saveModifyPirRight(){

		let date = $("#q-pir-date_applied").val();
		let staff = $("#q-pir-staff").val();
		let note = $("#q-pir-note").val();
		let ponum = $("#q-pir-po_num").val();
		let eta = $("#q-pir-eta").val();
		let pirid = $("#q-pir-id").val();

		$.ajax({
			url: globalUrl + '/pir/' +pirid+ '/evaluate',
			type: 'POST',
			data: {
				_token: globalToken,
                _method: 'PATCH',
                date: date,
                staff: staff,
                note: note,
                ponum: ponum,
                eta: eta,
			},
		})
		.done(function() {
			console.log("successfully evaluated pir - right side");

			var d = tablePIR.row(pirRowIndx).data();
                    d[8] = date;
                    d[9] = staff;
                    d[10] = ponum;
                    d[11] = note;
                    d[12] = eta;
                    d[13] = '<a href="" role="button" class="btn btn-success" data-toggle="modal" data-target="#modalForImprovementEvaluate" data-pirid="'+pirid+'" data-pirdate="'+date+'" data-pirstaff="'+staff+'" data-pirnote="'+note+'" data-pirponum="'+ponum+'" data-pireta="'+eta+'">Edit</a>';
            tablePIR.row(pirRowIndx).data(d).draw();
            showMessage("alert-success", "Evaluated PIR details. Date Applied: " + date + "; Staff: " + staff + "; Note: " + note + "; PO Num: " + ponum + "; ETA: " + eta );
            
		})
		.fail(function() {
			console.log("error evaluating pir details");
	    	showMessage("alert-danger", "Failed in evaluating PIR details. Please contact IT. Error PIR ID: " +pirid);

		})
		.always(function() {
			$("#q-pir-date_applied").val("");
			$("#q-pir-staff option").removeAttr("selected");
			$("#q-pir-note").val("");
			$("#q-pir-po_num").val("");
			$("#q-pir-eta").val("");
			$("#q-pir-id").val("");
		});
		
}



function addPIR(){
    let date = $("#pir-date").val();
    let type = $("#pir-type option:selected").val();
    let staff = $("#pir-staff option:selected").val();
    let note = $("#pir-notes").val();
    let relatedskus = getRelatedSkuItemsToBeSentToDb("pir-relatedsku");
    let filelocation = $("#pir-filelocation").val();
    let sku_id = $("#d-id").val();


    $.ajax({
        url: globalUrl + '/pir/add',
        type: 'POST',
        data: {
            _token: globalToken,
            _method: 'POST',
            date: date,
            type: type,
            staff: staff,
            note: note,
            relatedsku: relatedskus,
            filelocation: filelocation,
            sku_id: sku_id,
        },
    })
    .done(function(id) {
        console.log("successfully created new PIR entry");
        
        let btnEdt1 = '<a href="" role="button" class="btn btn-success" data-toggle="modal" data-target="#modalForImprovementEdit" data-pirid="'+id+'" data-pirdate="'+date+'" data-pirtype="'+type+'" data-pirstaff="'+staff+'" data-pirnote="'+note+'" data-pirrelatedsku="'+relatedskus+'" data-pirfilelocation="'+filelocation+'">Edit</a>';
        let btnEdt2 = '<a href="" role="button" class="btn btn-success" data-toggle="modal" data-target="#modalForImprovementEvaluate" data-pirid="'+id+'" data-pirdate="" data-pirstaff="" data-pirnote="" data-pirponum="" data-pireta="">Edit</a>';
        tablePIR.row.add([
                id,
                date,
                type,
                staff,
                note,
                relatedskus,
                filelocation,
                btnEdt1,
                "",
                "",
                "",
                "",
                "",
                btnEdt2,
                ""
        ]).draw();

        showMessage("alert-success", "Added New PIR- Type: " + type + "; Staff: " + staff + "; Note: " + note + "; Related SKUs: " + relatedskus + "; File Location: " + filelocation);

        swal({
            title: "Send Notification?",
            text: "Would you want to send an email notification now?",
            icon: "warning",
            buttons: {
                    cancel: "Later!",
                    send: {
                            text: "Send Now!",
                            value: "send",
                    }
                },
            allowOutsideClick: false
            })
            .then((value) => {
                    if (value == "send") {
                        sendEmailNotificationPost(id);
                    }
        }); // end swal


    })
    .fail(function(x) {
        console.log("error: " + x);
        showMessage("alert-danger", "Failed in creating new PIR. Please contact IT.");

    })
    .always(function() {
            $("#pir-date").val("");
            $("#pir-type option").removeAttr("selected");
            $("#pir-staff option").removeAttr('selected');
            $("#pir-notes").val("");
            $("#pir-filelocation").val("");
            $("#d-id").val(formatDate(utc));
    });
    

}


function showSendEmailButton(boolx){

        if (boolx) {
                $("#btnSendPIREmail").removeAttr("disabled");
        }else{
                $("#btnSendPIREmail").attr("disabled", "disabled");
        }
}


       
function sendEmailNotificationPost(pir_id){
           
           $.ajax({
               url: globalUrl + '/pir/notification/send', 
               type: 'POST',
               data: {
                    _token: globalToken,
                    _method: 'POST',
                    pir_id: pir_id,
               },
               beforeSend: function(){
                    $("#dashboard-spinner").removeClass('hidden');
               },
           })
           .done(function() {

                    swal("Email Sent!", "Notification Email has been sent!", "success");
           })
           .fail(function() {

                    swal("Sending Error!", data, "error");
           })
           .always(function() {
                    $("#dashboard-spinner").addClass('hidden');
           });
           
        
}


function sendEmailNotificationPostMany(pir_ids){
           
           $.ajax({
               url: globalUrl + '/pir/notification/sendmany', 
               type: 'POST',
               data: {
                    _token: globalToken,
                    _method: 'POST',
                    pir_ids: pir_ids,
               },
               beforeSend: function(){
                    $("#dashboard-spinner").removeClass('hidden');
               },
           })
           .done(function() {

                    swal("Email Sent!", "Notification Email has been sent!", "success");
           })
           .fail(function() {

                    swal("Sending Error!", data, "error");
           })
           .always(function() {
                    $("#dashboard-spinner").addClass('hidden');
           });
           
        
}