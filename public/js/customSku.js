
/*
* @param String skutosearch
* @desc Will search for a SKU, and return an alert if there are none.
* @phpfunction SkuController::search
*/
function searchSku(skutosearch = ""){

        if (skutosearch == "") {
                skutosearch = $("#searchsku").val();
        }else{
                $("#searchsku").val(skutosearch);
        }



		if(skutosearch.trim() == ""){
                    swal("Reminder", "Enter SKU to search!", "error");
        }
        else {
            $("#spinner").css("opacity", 1);

            $.post(globalUrl+"/sku/search",
                {
                    _token: globalToken,
                    _method: 'POST',
                    skutosearch: skutosearch,
                }, 
                function(data, textStatus, xhr) {
                        if(textStatus == "success"){
                                
                                tablePIR.clear().draw();
                                tableVideo.clear().draw();
                                tableKb.clear().draw();
                                tableEmail.clear().draw();
                                

                                if(data.skus){
                                            toggleAddEditSku("edit", data.skus.id);

                                            $(".buttonAddGeneric").removeClass('hidden');
                                            
                                            $("#d-desc").val(data.skus.desc);
                                            $("#d-id").val(data.skus.id);
                                            $("#d-supplier").val(data.skus.supplier);
                                            $("#d-status").val(data.skus.status);
                                            $("#d-category").val(data.skus.category);
                                            $("#d-wise_id").val(data.skus.wise_id);

                                            if ( data.skus.pirs.length > 0 ) {
                                                    
                                                    $.each(data.skus.pirs, function(i, pir) {
                                                        let btnEdt1 = '<a href="" role="button" class="btn btn-success" data-toggle="modal" data-target="#modalForImprovementEdit" data-pirid="'+pir.id+'" data-pirdate="'+pir.date+'" data-pirtype="'+pir.type+'" data-pirstaff="'+pir.staff+'" data-pirnote="'+pir.note+'" data-pirrelatedsku="'+pir.relatedsku+'" data-pirfilelocation="'+pir.filelocation+'">Edit</a>';
                                                        let btnEdt2 = '<a href="" role="button" class="btn btn-success" data-toggle="modal" data-target="#modalForImprovementEvaluate" data-pirid="'+pir.id+'" data-pirdate="'+pir.date_applied+'" data-pirstaff="'+pir.q_staff+'" data-pirnote="'+pir.q_note+'" data-pirponum="'+pir.po_num+'" data-pireta="'+pir.eta+'">Edit</a>';

                                                            tablePIR.row.add([
                                                                    pir.id,
                                                                    pir.date,
                                                                    pir.type,
                                                                    pir.staff,
                                                                    pir.note,
                                                                    pir.relatedsku,
                                                                    pir.filelocation,
                                                                    btnEdt1,
                                                                    pir.date_applied,
                                                                    pir.q_staff,
                                                                    pir.po_num,
                                                                    pir.q_note,
                                                                    pir.eta,
                                                                    btnEdt2,
                                                                    pir.q_id
                                                            ]).draw();
                                                    });
                                                        
                                            } // end data.skus.pirs.length > 0

                                            if ( data.skus.videos.length > 0 ) {
                                                    
                                                    $.each(data.skus.videos, function(i, vid) {
                                                            tableVideo.row.add([
                                                                vid.id,
                                                                vid.viddate,
                                                                vid.type,
                                                                vid.title,
                                                                vid.status,
                                                                vid.staff,
                                                                vid.details,
                                                                vid.filelocation,
                                                                vid.relatedskus,
                                                                vid.shootdate,
                                                                vid.shootlocation,
                                                                (vid.addedtokb == 1 ? "Yes" : "No"), 
                                                                (vid.manual == 1 ? "Yes" : "No"), 
                                                                (vid.customeremail == 1 ? "Yes" : "No"),
                                                                vid.youtubelink,
                                                                (vid.youtube == "public" ? "Public" : "Unlisted (not public)"),
                                                            ] ).draw();   
                                                    });
                                                        
                                            } // end data.skus.pirs.length > 0

                                            if ( data.skus.kbs.length > 0 ) {
                                                    
                                                    $.each(data.skus.kbs, function(i, kb) {
                                                            tableKb.row.add([
                                                                    kb.id,
                                                                    kb.kbdate,
                                                                    kb.submitted_by,
                                                                    kb.ticket_id,
                                                                    kb.issue,
                                                                    kb.advice,
                                                                    kb.relatedsku,
                                                                    kb.manual_link,
                                                                    kb.approved_by,
                                                                    kb.status,
                                                                    kb.added_by,
                                                                    kb.date_added,
                                                                ] ).draw();  
                                                    });
                                                        
                                            } // end data.skus.pirs.length > 0

                                            if ( data.skus.emails.length > 0 ) {
                                                    
                                                    $.each(data.skus.emails, function(i, em) {
                                                            tableEmail.row.add( [
                                                                    em.id,
                                                                    em.edate,
                                                                    em.type,
                                                                    em.staff,
                                                                    em.frequency,
                                                                    em.filelocation,
                                                                    em.relatedskus,
                                                                    em.ceasedate,
                                                                    em.notes,
                                                                ] ).draw();   
                                                    });
                                                        
                                            } // end data.skus.pirs.length > 0


                                }else{
                                            swal("No Result", "No SKU found!", "error");
                                            $("#searchsku").val("").focus();
                                            $(".buttonAddGeneric").addClass('hidden');
                                            
                                }
                                $("#spinner").css("opacity", 0);

                        }else{ //textStatus != success
                            swal("Error", "Invalid SKU!", "error");
                            $("#spinner").css("opacity", 0);
                            $(".buttonAddGeneric").addClass('hidden');
                            $("#searchsku").val("").focus();
                        }
                }); // end post function
                    
        } // end else skutosearch != ""
		
}


/*
* @param String showme, Integer skuid
* @desc Will toggle the <span> containing the button to show: add or edit
* 
*/
function toggleAddEditSku(showme, skuid){
		if (showme == "add") {
				$("#spanSkuAdd").removeClass('hidden');
				$("#spanSkuEdit").addClass('hidden');
				$("#spanSkuEdit").removeAttr('data-skuid');
                $("#btnSaveModifiedSku").addClass('hidden');

		}else{

				$("#spanSkuAdd").addClass('hidden');
				$("#spanSkuEdit").removeClass('hidden');
				$("#spanSkuEdit").attr("data-skuid", skuid);
		}
}


/*
* @param Boolean boolx
* @desc Toggles the description, supplier, status, category and wiseid fields for editing
* 
*/
function enableModifySku(boolx){

        if (boolx) {

                $("#btnSaveModifiedSku").removeClass('hidden');

                $("#searchsku").attr("readonly", "true");
                $("#search-btn").attr("disabled", "true");

                
                $("#d-sku").removeAttr("readonly");
                $("#d-supplier").removeAttr("readonly");
                $("#d-status").removeAttr("disabled");
                $("#d-category").removeAttr("disabled");
                $("#d-wise_id").removeAttr("readonly");
                $("#d-desc").removeAttr("readonly").focus();

        }else{
                $("#btnSaveModifiedSku").addClass('hidden');

                $("#searchsku").removeAttr("readonly");
                $("#search-btn").removeAttr("disabled");

                $("#d-desc").attr("readonly", "true");
                $("#d-sku").attr("readonly", "true");
                $("#d-supplier").attr("readonly", "true");
                $("#d-status").attr("disabled", "disabled");
                $("#d-category").attr("disabled", "disabled");
                $("#d-wise_id").attr("readonly", "true");
        }
        
}


/*
* @param 
* @desc Saves the modified SKU details via Ajax call
* @phpfunction SkuController::update
*/
function saveModifySku(){
        let skuidFromDID = $("#d-id").val();

        let skuName = $("#searchsku").val();
        let description = $("#d-desc").val();
        let supplier = $("#d-supplier").val();
        let status = $("#d-status option:selected").val();
        let category = $("#d-category option:selected").val();
        let wiseid = $("#d-wise_id").val();

        $.ajax({
                url: globalUrl + '/sku/'+skuidFromDID+'/update',
                type: 'POST',
                data: {
                    _token: globalToken,
                    _method: 'PATCH',
                    description: description,
                    supplier: supplier,
                    status: status,
                    category: category,
                    wiseid: wiseid,
                },
        })
        .done(function() {
            console.log("success modifying sku details");
            showMessage("alert-success", "Modified " + skuName + " details.");
        })
        .fail(function() {
            console.log("failed modifying sku details");
            showMessage("alert-danger", "Failed in modifying " + skuName + " details. Please contact IT. Error SKU ID: " +skuidFromDID);
        });
        

        enableModifySku(false);
}


/*
* @param 
* @desc Creates a SKU and saves it to the db
* @phpfunction SkuController::add
*/
function addSku(){
        let sku = $("#sku").val();
        let supplier = $("#supplier").val();
        let status = $("#status option:selected").val();
        let category = $("#category option:selected").val();
        let wiseid = $("#wise_id").val();
        let desc = $("#desc").val();


        if (sku.trim() != "" || supplier.trim() != "") {


            $.ajax({
                url: globalUrl + '/sku/add',
                type: 'POST',
                data: {
                    _token: globalToken,
                    _method: 'POST',
                    sku: sku,
                    supplier: supplier,
                    status: status,
                    category: category,
                    wiseid: wiseid,
                    description: desc,
                },
            })
            .done(function(id) {
                    console.log("success adding new sku");
                    showMessage("alert-success", "Successfully added a New SKU: " + sku + ", Description: " + desc + ", Supplier: " + supplier + ", Status: " + status + ", Category: " + category + ", Wise ID: " + wiseid);

                    searchSku(sku);
            })
            .fail(function() {
                    console.log("failed in adding a new sku.");
                    showMessage("alert-danger", "Failed in adding new SKU " + sku + " and its details. Please contact IT.");
            }).always(function(){
                    $("#sku").val("");
                    $("#supplier").val("");
                    $("#status option:first").attr("selected", "selected");
                    $("#category option:first").attr("selected", "selected");
                    $("#wise_id").val("");
                    $("#desc").val("");
            });

        }else{ /* Required fields are lacking */
                swal("Failed", "Please fill-in empty fields.", "error");
        }

}