        var vidRowIndx = -1;
        

        function addVideo(){
            let skuid = $("#d-id").val();
            let date = $("#vid-viddate").val();
            let title = $("#vid-title").val();
            let type = $("#vid-type").val();
            let status = $("#vid-status option:selected").val();
            let staff = $("#vid-staff option:selected").val();
            let details = $("#vid-details").val();
            let filelocation = $("#vid-filelocation").val();
            let selectedrelatedskus = $("#vid-relatedskus :selected");
            let shootdate = $("#vid-shootdate").val();
            let shootlocation = $("#vid-shootlocation").val();
            
            let addedtokb = $('#vid-addedtokb').is(':checked') ? 1 : 0;
            let manual = $('#vid-manual').is(':checked') ? 1 : 0;
            let customeremail = $('#vid-customeremail').is(':checked') ? 1 : 0;
            let youtubelink = $("#vid-youtubelink").val();
            let youtube = $("#mAddVideo input[name='vid-optyoutube']:checked").val();

            var relatedskus ="";
            selectedrelatedskus.each(function(index, el) {
                relatedskus += el.text + ", ";
            });
            relatedskus=relatedskus.trim().slice(0,-1); 

            $.post(globalUrl + '/video/add', 
                {
                    _token: globalToken,
                    _method: 'POST',
                    skuid: skuid,
                    viddate: date,
                    title: title,
                    type: type,
                    status: status,
                    staff: staff,
                    details: details,
                    filelocation: filelocation,
                    relatedskus: relatedskus,
                    shootdate: shootdate,
                    shootlocation: shootlocation,
                    addedtokb: addedtokb,
                    manual: manual,
                    customeremail: customeremail,
                    youtubelink: youtubelink,
                    youtube: youtube,
                }, 
                function(data, textStatus, xhr) {
                    let kb = (addedtokb == 1 ? "Yes" : "No");
                    let manwal = (manual == 1 ? "Yes" : "No");
                    let cxemail = (customeremail == 1 ? "Yes" : "No");
                    let st = (youtube == "public" ? "Public" : "Unlisted (not public)");
                    
                    $("#tableVideo").dataTable().fnAddData( 
                        [
                            data,
                            date,
                            type,
                            title,
                            status,
                            staff,
                            details,
                            filelocation,
                            relatedskus,
                            shootdate,
                            shootlocation,
                            kb, manwal, cxemail, youtubelink,
                            st
                        ] );
                    //swal("Success!", "A new video has been added", "info");
                    showMessage("alert-success", "Added New Video- : Date: " + date + "; Type: " + type+ "; Title: " + title+ "; Status: " + status+ "; Staff: " + staff+ "; Details: " + details + "; File Location: " + filelocation+ "; Relatedskus: " + relatedskus+ "; Shoot Date: " + shootdate+ "; Shootlocation: " + shootlocation);
                    document.getElementById('vid-form').reset();

            });

        }


        function updateVideo(){
            let vidid = $("#evid-vidid").val();
            let date = $("#evid-viddate").val();
            let type = $("#evid-type").val();
            let title = $("#evid-title").val();
            let status = $("#evid-status option:selected").val();
            let staff = $("#evid-staff option:selected").val();
            let details = $("#evid-details").val();
            let filelocation = $("#evid-filelocation").val();
            let selectedrelatedskus = $("#evid-relatedskus :selected");
            let shootdate = $("#evid-shootdate").val();
            let shootlocation = $("#evid-shootlocation").val();
            
            let addedtokb = $('#evid-addedtokb').is(':checked') ? 1 : 0;
            let manual = $('#evid-manual').is(':checked') ? 1 : 0;
            let customeremail = $('#evid-customeremail').is(':checked') ? 1 : 0;
            let youtubelink = $("#evid-youtubelink").val();
            let youtube = $("#mEditVideo input[name='evid-optyoutube']:checked").val();

            var relatedskus ="";
            selectedrelatedskus.each(function(index, el) {
                relatedskus += el.text + ", ";
            });
            relatedskus=relatedskus.trim().slice(0,-1); 

            $.post(globalUrl + '/video/update/' + vidid, 
                {
                    _token: globalToken,
                    _method: 'PATCH',
                    viddate: date,
                    type: type,
                    title: title,
                    status: status,
                    staff: staff,
                    details: details,
                    filelocation: filelocation,
                    relatedskus: relatedskus,
                    shootdate: shootdate,
                    shootlocation: shootlocation,
                    addedtokb: addedtokb,
                    manual: manual,
                    customeremail: customeremail,
                    youtubelink: youtubelink,
                    youtube: youtube,
                }, 
                function(data, textStatus, xhr) {
                    let kb = (addedtokb == 1 ? "Yes" : "No");
                    let manwal = (manual == 1 ? "Yes" : "No");
                    let cxemail = (customeremail == 1 ? "Yes" : "No");
                    let st = (youtube == "public" ? "Public" : "Unlisted (not public)");
                    
                    var vTable = $("#tableVideo").DataTable();
                    var d = vTable.row(vidRowIndx).data();
                    d[1] = date;
                    d[2] = type;
                    d[3] = title;
                    d[4] = status;
                    d[5] = staff;
                    d[6] = details;
                    d[7] = filelocation;
                    d[8] = relatedskus;
                    d[9] = shootdate;
                    d[10] = shootlocation;
                    d[11] = kb;
                    d[12] = manwal;
                    d[13] = cxemail;
                    d[14] = youtubelink;
                    d[15] = st;
                    vTable.row(vidRowIndx).data(d).draw();

                    //swal("Success!", "The video information has been updated.", "info");
                    showMessage("alert-success", "Modified Video details- : Date: " + date + "; Type: " + type+ "; Title: " + title+ "; Status: " + status+ "; Staff: " + staff+ "; Details: " + details + "; File Location: " + filelocation+ "; Relatedskus: " + relatedskus+ "; Shoot Date: " + shootdate+ "; Shootlocation: " + shootlocation);
                    document.getElementById('evid-form').reset();

            });
        }


        function setModalEventDoubleClickVideo(){
                tableVideo.on('dblclick', 'tbody tr', function() {
                        let t = tableVideo.row(this).data();
                        
                        $("#mEditVideo").modal('toggle');
                        document.getElementById('evid-form').reset();

                        $("#evid-vidid").val(t[0]);
                        $("#evid-viddate").val(t[1]);
                        $("#evid-type option[value='"+t[2]+"']").prop("selected", "selected");
                        $("#evid-title").val(t[3]);
                        $("#evid-status option[value='"+t[4]+"']").prop("selected", "selected");
                        $("#evid-staff option[value='"+t[5]+"']").prop("selected", "selected");
                        $("#evid-details").val(t[6]);
                        $("#evid-filelocation").val(t[7]);

                        /*$("#evid-relatedskus").val(t[7]);*/
                        let relatedskusAll = t[8].split(",");
                    
                        let arr = [];
                        for (var i = 0; i < relatedskusAll.length; i++) {
                            $('#evid-relatedskus option').each(function(index, element) {
                                if (element.text == relatedskusAll[i].trim()) {
                                    arr.push(element.value);
                                }
                            });
                            
                        }

                        $('#evid-relatedskus').selectpicker('val', arr);

                        $("#evid-shootdate").val(t[9]);
                        $("#evid-shootlocation").val(t[10]);
                        
                        (t[11] == "Yes" ? $('#evid-addedtokb').prop("checked", "checked") :  $('#evid-addedtokb').removeProp('checked'));
                        (t[12] == "Yes" ? $('#evid-manual').prop("checked", "checked") :  $('#evid-manual').removeProp('checked'));
                        (t[13] == "Yes" ? $('#evid-customeremail').prop("checked", "checked") :  $('#evid-customeremail').removeProp('checked'));
                        $("#evid-youtubelink").val(t[14]);
                        (t[15].indexOf("Unlisted") >= 0 ? $("#mEditVideo :radio[value='unlisted']").prop("checked", "checked") : $("#mEditVideo :radio[value='public']").prop("checked", "checked") );
                        
                        window.vidRowIndx = tableVideo.row(this).index();
                });
        }


        function setModalEventAddVideo(){
                $("#mAddVideo").on('show.bs.modal', function(event) {
                        $("vid-viddate").val(formatDate(utc)); 
                        $("#mAddVideo #vid-relatedskus").selectpicker("deselectAll");
                });
        }