

function modalAddStaffEvents(){

		$("#modalAddStaff").on("show.bs.modal", function(){
				$("#settingaddstaff-name").val('');
		});

		//
		$("#modalEditStaff").on("show.bs.modal", function(e){
				let name = $(e.relatedTarget).data("staffname");
				$("#settingeditstaff-name").val(name);
				$("#settingeditstaff-oldname").val(name);
		});
}


function settingsAddStaff(){

		let staffname = $("#settingaddstaff-name").val();
		let staffNameUcFirst = staffname.charAt(0).toUpperCase() + staffname.slice(1);

		if (staffname.trim() != "") {
				$.ajax({
					url: globalUrl+ '/settings/add/staff',
					type: 'POST',
        			data: {
        				_token: globalToken,
        				_method: 'POST',
        				name: staffNameUcFirst,
        			},
					
				})
				.done(function() {
					console.log("success");
					showMessage("alert-success", "Successfully added a New Staff: " + staffNameUcFirst);

					$("ul.allstaffli").append(addSpanEditHtmlForJsonData("#modalEditStaff", staffNameUcFirst));
				})
				.fail(function() {
					console.log("error");
					showMessage("alert-danger", "Failed in adding New Staff: " + staffNameUcFirst + " Please contact IT.");
				})
				.always(function() {
					console.log("complete");
					$("#settingaddstaff-name").val('');
				});
				
		}else{
				swal("Error", "Empty Fields!", "error");
		}

		

}



function settingsEditStaff(){

		let staffname = $("#settingeditstaff-name").val();
		let oldname = $("#settingeditstaff-oldname").val();
		let staffNameUcFirst = staffname.charAt(0).toUpperCase() + staffname.slice(1);

		if (staffNameUcFirst.trim() != "") {

				$.post(globalUrl+ '/settings/edit/staff', 
						{
							_token: globalToken,
	        				_method: 'PATCH',
	        				name: staffNameUcFirst,
	        				oldname: oldname,
						}, 
					function(data, textStatus, xhr) {
					/*optional stuff to do after success */
						if (textStatus == "success") {
							console.log("success");
							showMessage("alert-success", "Modified Staff Name From " + oldname + " To: " + staffNameUcFirst);
							loadSettingsPartialSelectOptionsFromJson("allstaff");

						}else if(textStatus == "error"){
							console.log("error");
							showMessage("alert-danger", "Failed in modifying Staff: " + oldname + " To "+staffNameUcFirst+". Please contact IT.");
						}

						$("#settingeditstaff-name").val('');
						$("#settingeditstaff-oldname").val('');
				});
				
		}else{
				swal("Error", "Empty Fields!", "error");
		}

}