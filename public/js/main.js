// RAW FUNCTION TO SELECT TABLE VIEWS
function selectView(selectedView, btn, addbtn){
    $("#divTables").find(".dataTables_wrapper.no-footer").addClass('hidden');
    $("#" + selectedView).removeClass('hidden');

    $(".viewbtns").css("display","block");
    $(".view").removeClass("border-width-zero");
    $( "#"+selectedView + " #"+btn).addClass("border-width-zero");
    $(".viewaddbtn").addClass('hidden');

    $("#"+addbtn).removeClass('hidden');
} 



$(document).ready(function() {

        $.ajax({
            url: globalUrl + '/json/jsonfile.json',
            type: 'GET',
            dataType: 'JSON',
            async: false,
            success: function(data){
                    let pirtypeHtml = "";
                    let staffHtml = "";
                    
                    $.each(data.pirtype, function(index, el) {
                            pirtypeHtml += '<option value="'+el.value+'">'+el.text+'</option>';
                    });
                    $(".pirtype").append(pirtypeHtml);

                    $.each(data.staff, function(index, el) {
                            staffHtml += '<option value="'+el.value+'">'+el.text+'</option>';
                    });
                    $(".allstaff").append(staffHtml);
                
            },

        })
        .fail(function(jqxhr, textStatus, error) {
            console.log("ERROR!: " + textStatus + " : " + error);
        });

        $('#topdiv').on('keyup', '#searchsku', function(event) {
            
            if(event.keyCode == 13)
            {
                searchsku();
            }
        });

        //e-category
        $('#modalAddCategory').on('keyup', '#e-category', function(event) {
            
            if(event.keyCode == 13)
            {
                addcategory();
            }
        });

        var table = $("#tablePIR").DataTable({
             "columnDefs": [
                    {
                        "targets": [4,5],
                        "render": $.fn.dataTable.render.ellipsis(10)
                    },
                    {
                        "targets": [3,9,10],
                        "render": $.fn.dataTable.render.ellipsis(15)
                    }
                    
                ],
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "ordering": false
        });

        table.on( 'click', 'tbody tr', function () {
            window.pirRowIndx = table.row(this).index();
        } );

           


        var tableVideo = $("#tableVideo").DataTable({
            "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": true
                    },
                    {
                        "targets": [3,7,8,10,14],
                        "render": $.fn.dataTable.render.ellipsis(10)
                    },
                    {
                        "targets": [6],
                        "render": $.fn.dataTable.render.ellipsis(15)
                    }
                    
                ],
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "ordering": false
        });

        tableVideo.on('click', 'tbody tr', function() {
            let t = tableVideo.row(this).data();
            
            $("#mEditVideo").modal('toggle');
            document.getElementById('evid-form').reset();

            $("#evid-vidid").val(t[0]);
            $("#evid-viddate").val(t[1]);
            $("#evid-type option[value='"+t[2]+"']").prop("selected", "selected");
            $("#evid-title").val(t[3]);
            $("#evid-status option[value='"+t[4]+"']").prop("selected", "selected");
            $("#evid-staff option[value='"+t[5]+"']").prop("selected", "selected");
            $("#evid-details").val(t[6]);
            $("#evid-filelocation").val(t[7]);

            /*$("#evid-relatedskus").val(t[7]);*/
            let relatedskusAll = t[8].split(",");
        
            let arr = [];
            for (var i = 0; i < relatedskusAll.length; i++) {
                $('#evid-relatedskus option').each(function(index, element) {
                    if (element.text == relatedskusAll[i].trim()) {
                        arr.push(element.value);
                    }
                });
                
            }

            $('#evid-relatedskus').selectpicker('val', arr);

            $("#evid-shootdate").val(t[9]);
            $("#evid-shootlocation").val(t[10]);
            
            (t[11] == "Yes" ? $('#evid-addedtokb').prop("checked", "checked") :  $('#evid-addedtokb').removeProp('checked'));
            (t[12] == "Yes" ? $('#evid-manual').prop("checked", "checked") :  $('#evid-manual').removeProp('checked'));
            (t[13] == "Yes" ? $('#evid-customeremail').prop("checked", "checked") :  $('#evid-customeremail').removeProp('checked'));
            $("#evid-youtubelink").val(t[14]);
            (t[15].indexOf("Unlisted") >= 0 ? $("#mEditVideo :radio[value='unlisted']").prop("checked", "checked") : $("#mEditVideo :radio[value='public']").prop("checked", "checked") );
            
            window.vidRowIndx = tableVideo.row(this).index();
        });


        var tableEmail = $("#tableEmail").DataTable({
             "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": true
                    },
                    {
                        "targets": [4,5,6],
                        "render": $.fn.dataTable.render.ellipsis(10)
                    },
                    {
                        "targets": [8],
                        "render": $.fn.dataTable.render.ellipsis(15)
                    }
                    
                ],
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "ordering": false
        });

        tableEmail.on('click', 'tbody tr', function() {
            let t = tableEmail.row(this).data();
            
            $("#mEditEmail").modal('toggle');
            document.getElementById('eem-form').reset();

            $("#eem-id").val(t[0]);
            $("#eem-date").val(t[1]);
            $("#eem-type option[value='"+t[2]+"']").prop("selected", "selected");
            $("#eem-staff option[value='"+t[3]+"']").prop("selected", "selected");
            $("#eem-frequency").val(t[4]);
            $("#eem-filelocation").val(t[5]);
            $("#eem-relatedskus").val(t[6]);
            $("#eem-ceasedate").val(t[7]);
            $("#eem-notes").val(t[8]);
            
            window.emRowIndx = tableEmail.row(this).index();
        });

        var tableKb = $("#tableKb").DataTable({
            "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": true
                    },
                    {
                        "targets": [4,5,6],
                        "render": $.fn.dataTable.render.ellipsis(10)
                    }
                    
                ],
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "ordering": false
        });

        tableKb.on('click', 'tbody tr', function() {
            let t = tableKb.row(this).data();
            
            $("#mEditKb").modal('toggle');
            document.getElementById('ekb-form').reset();

            $("#ekb-id").val(t[0]);
            $("#ekb-date").val(t[1]);
            $("#ekb-subby").val(t[2]);
            $("#ekb-ticketid").val(t[3]);
            $("#ekb-issue").val(t[4]);
            $("#ekb-advice").val(t[5]);
            $("#ekb-manlink").val(t[6]);
            $("#ekb-approvedby option[value='"+t[7]+"']").prop("selected", "selected");
            $("#ekb-status option[value='"+t[8]+"']").prop("selected", "selected");
            $("#ekb-addedby").val(t[9]);
            $("#ekb-date_added").val(t[10]);
            
            window.kbRowIndx = tableKb.row(this).index();
        });



        $("#tableVideo_wrapper").addClass('hidden');
        $("#tableEmail_wrapper").addClass('hidden');
        $("#tableKb_wrapper").addClass('hidden');

        
});
    
    $.fn.dataTable.render.ellipsis = function ( cutoff ) {
        return function ( data, type, row ) {
            if (data != null) {
                return type === 'display' && data.length > cutoff ?
                data.substr( 0, cutoff ) +'…' :
                data;    
            }else{
                return "";
            }
            
        }
    };


    function loadJSON_mReportDetails(tableData){
        $.ajax({
            url: globalUrl + '/json/jsonfile.json',
            type: 'GET',
            dataType: 'JSON',
            async: false,
            success: function(data){
                //console.log(tableData);

                switch(tableData[6]){
                    case "Video": $("#mReportDetails_Video").modal(); mReportDetails_Video(data, tableData); break;
                    case "PIR": $("#mReportDetails_Pir").modal(); mReportDetails_Pir(data, tableData); break;
                    case "Email": $("#mReportDetails_Email").modal(); mReportDetails_Email(data, tableData); break;
                    case "KB": $("#mReportDetails_KB").modal(); mReportDetails_KB(data, tableData); break;
                    default: break;
                }
            },

        })
        .fail(function(jqxhr, textStatus, error) {
            console.log("ERROR!: " + textStatus + " : " + error);
        });
    }


    function mReportDetails_Video(data, table){
        let vid_id = table[1];
        let sku_id = parseInt(table[0]);

            $.ajax({
                url: globalUrl + '/reports/details/video/' + vid_id,
                type: 'GET',
                async: false,
                success: function(res){

                            $("#rvid-viddate").text(res.video.viddate);
                            $("#rvid-type").text(res.video.type);
                            $("#rvid-staff").text(res.video.staff);
                            $("#rvid-filelocation").text(res.video.filelocation);
                            $("#rvid-filelocation").text(res.video.filelocation);
                            $("#rvid-shootdate").text(res.video.shootdate);
                            $("#rvid-shootlocation").text(res.video.shootlocation);
                            $("#rvid-addedtokb").text( (res.video.addedtokb == "1" ? "Yes" : "No") );
                            $("#rvid-manual").text( (res.video.manual == "1" ? "Yes" : "No") );
                            $("#rvid-customeremail").text( (res.video.customeremail == "1" ? "Yes" : "No") );
                            $("#rvid-youtube").text(res.video.youtube);
                            $("#rvid-title").text(res.video.title);
                            $("#rvid-status").text(res.video.status);
                            $("#rvid-youtubelink").text(res.video.youtubelink);
                            $("#rvid-relatedskus").text(removeLastComma(res.video.relatedskus));

                            $("#mReportDetails_Video #parent-sku").text(table[4]);
                            $("#mReportDetails_Video #parent-supplier").text(table[5]);
                            $("#mReportDetails_Video #parent-wiseid").text(res.video.getsku.wise_id);
                            $("#mReportDetails_Video #parent-description").text(res.video.getsku.desc);
                            $("#mReportDetails_Video #parent-skustatus").text(res.video.getsku.status);
                            $("#mReportDetails_Video #parent-category").text(table[3]);
                },
            });
            
            
    }



    function mReportDetails_Pir(data, table){
        let pir_id = table[1];
        let sku_id = parseInt(table[0]);


        $.ajax({
            url: globalUrl + '/reports/details/pir/' + pir_id,
            type: 'GET',
            async: false,
            success: function(res){
                        $("#rpir-date").text(res.pir.date);
                        $("#rpir-staff").text(res.pir.staff);
                        $("#rpir-type").text(res.pir.type);
                        $("#rpir-note").text(res.pir.note);
                        $("#rpir-filelocation").text(res.pir.filelocation);
                        //$("#rpir-relatedskus").text(removeLastComma(res.pir.relatedskus));

                        $("#rpir-evaldateapplied").text(res.pir.date_applied);
                        $("#rpir-evaleta").text(res.pir.eta);
                        $("#rpir-evalponum").text(res.pir.po_num);
                        $("#rpir-evalstaff").text(res.pir.q_staff);
                        $("#rpir-evalnote").text(res.pir.q_note);

                        $("#mReportDetails_Pir #parent-sku").text(table[4]);
                        $("#mReportDetails_Pir #parent-supplier").text(table[5]);
                        $("#mReportDetails_Pir #parent-wiseid").text(res.pir.sku.wise_id);
                        $("#mReportDetails_Pir #parent-description").text(res.pir.sku.desc);
                        $("#mReportDetails_Pir #parent-skustatus").text(res.pir.sku.status);
                        $("#mReportDetails_Pir #parent-category").text(table[3]);

            },
        });
        
            
    }


    function mReportDetails_Email(data, table){
        let email_id = table[1];
        let sku_id = parseInt(table[0]);


        $.ajax({
            url: globalUrl + '/reports/details/email/' + email_id,
            type: 'GET',
            async: false,
            success: function(res){
                    
                        $("#remail-edate").text(res.email.edate);
                        $("#remail-type").text(res.email.type);
                        $("#remail-staff").text(res.email.staff);
                        $("#remail-frequency").text(res.email.frequency);
                        $("#remail-filelocation").text(res.email.filelocation);
                        $("#remail-ceasedate").text(res.email.ceasedate);
                        $("#remail-notes").text(res.email.notes);
                        $("#remail-relatedskus").text(removeLastComma(res.email.relatedskus));

                        $("#mReportDetails_Email #parent-sku").text(table[4]);
                        $("#mReportDetails_Email #parent-supplier").text(table[5]);
                        $("#mReportDetails_Email #parent-wiseid").text(res.email.getsku.wise_id);
                        $("#mReportDetails_Email #parent-description").text(res.email.getsku.desc);
                        $("#mReportDetails_Email #parent-skustatus").text(res.email.getsku.status);
                        $("#mReportDetails_Email #parent-category").text(table[3]);

            },
        });
    }


    function mReportDetails_KB(data, table){
        let kb_id = table[1];
        let sku_id = parseInt(table[0]);


        $.ajax({
            url: globalUrl + '/reports/details/kb/' + kb_id,
            type: 'GET',
            async: false,
            success: function(res){
                    
                        $("#rkb-kbdate").text(res.kb.kbdate);
                        $("#rkb-ticketid").text(res.kb.ticketid);
                        $("#rkb-submitted_by").text(res.kb.submitted_by);
                        $("#rkb-issue").text(res.kb.issue);
                        $("#rkb-advice").text(res.kb.advice);
                        $("#rkb-manual_link").text(res.kb.manual_link);
                        $("#rkb-approved_by").text(res.kb.approved_by);
                        $("#rkb-status").text(res.kb.status);
                        $("#rkb-added_by").text(res.kb.added_by);
                        $("#rkb-date_added").text(res.kb.date_added);
                        //$("#rkb-relatedskus").text(removeLastComma(res.kb.relatedskus));

                        $("#mReportDetails_KB #parent-sku").text(table[4]);
                        $("#mReportDetails_KB #parent-supplier").text(table[5]);
                        $("#mReportDetails_KB #parent-wiseid").text(res.kb.getsku.wise_id);
                        $("#mReportDetails_KB #parent-description").text(res.kb.getsku.desc);
                        $("#mReportDetails_KB #parent-skustatus").text(res.kb.getsku.status);
                        $("#mReportDetails_KB #parent-category").text(table[3]);

            },
        });
    }

    function removeLastComma(str) {
        if (str != "") {
            return str.replace(/,(\s+)?$/, '');       
        }
        return "";
    }
    