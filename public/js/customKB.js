        var kbRowIndx = -1;


        function addKb(){
            let sku_id = $("#d-id").val();
            let date = $("#kb-date").val();
            let submitted_by = $("#kb-subby").val();
            let ticket_id = $("#kb-ticketid").val();
            let issue = $("#kb-issue").val();
            let advice = $("#kb-advice").val();
            let manual_link = $("#kb-manlink").val();
            let approved_by = $("#kb-approvedby").val();
            let status = $("#kb-status").val();
            let added_by = $("#kb-addedby").val();
            let date_added = $("#kb-dateadded").val();
            let relatedskus = getRelatedSkuItemsToBeSentToDb("kb-relatedskus");
            
            $.post(globalUrl + '/kb/add', 
                {
                    _token: globalToken,
                    _method: 'POST',
                    date:date,
                    sku_id: sku_id,
                    submitted_by: submitted_by,
                    ticket_id: ticket_id,
                    issue: issue,
                    advice: advice,
                    relatedsku: relatedskus,
                    manual_link: manual_link,
                    approved_by: approved_by,
                    status: status,
                    added_by: added_by,
                    date_added: date_added,
                }, 
                function(data, textStatus, xhr) {
                    
                    tableKb.row.add( 
                        [
                            data,
                            date,
                            submitted_by,
                            ticket_id,
                            issue,
                            advice,
                            relatedskus,
                            manual_link,
                            approved_by,
                            status,
                            added_by,
                            date_added,
                        ] ).draw();
                    //swal("Success!", "A new kb has been added", "info");
                    showMessage("alert-success", "A new KB has been added. Date: " + date + "; Submitted by: " + submitted_by +"; Ticket ID: "+ ticket_id + "; Issue: " + issue + "; Advice: " + advice + "; Related SKU/s: " + relatedskus + "; Manual Link: " + manual_link );
                    document.getElementById('kb-form').reset();

            });

        }


        function updateKb(){
            let id = $("#ekb-id").val();
            let edate = $("#ekb-date").val();
            let submitted_by = $("#ekb-subby").val();
            let ticket_id = $("#ekb-ticketid").val();
            let issue = $("#ekb-issue").val();
            let advice = $("#ekb-advice").val();
            let manual_link = $("#ekb-manlink").val();
            let approved_by = $("#ekb-approvedby").val();
            let status = $("#ekb-status").val();
            let added_by = $("#ekb-addedby").val();
            let date_added = $("#ekb-dateadded").val();
            let relatedskus = getRelatedSkuItemsToBeSentToDb("ekb-relatedsku");
            

            //alert(id + "|" + edate + "|" + submitted_by + "|" + ticket_id + "|" + issue + "|" + advice + "|" + manual_link + "|" +approved_by + "|" +status + "|" +added_by + "|" + date_added);
            
            $.post(globalUrl + '/kb/update/' + id, 
                {
                    _token: globalToken,
                    _method: 'PATCH',
                    edate:edate,
                    id: id,
                    submitted_by: submitted_by,
                    ticket_id: ticket_id,
                    issue: issue,
                    advice: advice,
                    relatedsku: relatedskus,
                    manual_link: manual_link,
                    approved_by: approved_by,
                    status: status,
                    added_by: added_by,
                    date_added: date_added,
                }, 
                function(data, textStatus, xhr) {
                    var d = tableKb.row(kbRowIndx).data();
                    d[1] = edate;
                    d[2] = submitted_by;
                    d[3] = ticket_id;
                    d[4] = issue;
                    d[5] = advice;
                    d[6] = relatedskus;
                    d[7] = manual_link;
                    d[8] = approved_by;
                    d[9] = status;
                    d[10] = added_by;
                    d[11] = date_added;
                    tableKb.row(kbRowIndx).data(d).draw();

                    //swal("Success!", "The kb information has been updated.", "info");
                    showMessage("alert-success", "A new KB has been updated. Date: " + edate + "; Submitted by: " + submitted_by +"; Ticket ID: "+ ticket_id + "; Issue: " + issue + "; Advice: " + advice + "; Related SKU/s: " + relatedskus + "; Manual Link: " + manual_link );
                    document.getElementById('ekb-form').reset();

            });
        }


function setModalEventDoubleClickKb(){
    tableKb.on('dblclick', 'tbody tr', function() {
            let t = tableKb.row(this).data();
            
            console.log(t);
            $("#mEditKb").modal('toggle');
            document.getElementById('ekb-form').reset();

            $("#ekb-id").val(t[0]);
            $("#ekb-date").val(t[1]);
            $("#ekb-subby").val(t[2]);
            $("#ekb-ticketid").val(t[3]);
            $("#ekb-issue").val(t[4]);
            $("#ekb-advice").val(t[5]);
            
            setRelatedSkuSelectedItems(t[6], $("#ekb-relatedsku"));

            $("#ekb-manlink").val(t[7]);
            $("#ekb-approvedby option[value='"+t[8]+"']").prop("selected", "selected");
            $("#ekb-status option[value='"+t[9]+"']").prop("selected", "selected");
            $("#ekb-addedby").val(t[10]);
            $("#ekb-date_added").val(t[11]);
            
            window.kbRowIndx = tableKb.row(this).index();
        });
}

function setModalEventAddKb(){
        $("#mAddKb").on('show.bs.modal', function(event) {
                $("kb-date").val(formatDate(utc)); 
                $("#mAddKb #kb-relatedskus").selectpicker("deselectAll");
        });
}