        var emRowIndx = -1;


        function addEmail(){
            let skuid = $("#d-id").val();

            let date = $("#em-date").val();
            let type = $("#em-type option:selected").val();
            let staff = $("#em-staff option:selected").val();
            let frequency = $("#em-frequency").val();
            let filelocation = $("#em-filelocation").val();
            let relatedskus = getRelatedSkuItemsToBeSentToDb("em-relatedskus");
            let ceasedate = $("#em-ceasedate").val();
            let notes = $("#em-notes").val();
            
            $.post(globalUrl + '/email/add', 
                {
                    _token: globalToken,
                    _method: 'POST',
                    skuid: skuid,
                    edate: date,
                    type: type,
                    staff: staff,
                    frequency: frequency,
                    filelocation: filelocation,
                    relatedskus: relatedskus,
                    ceasedate: ceasedate,
                    notes: notes,
                }, 
                function(data, textStatus, xhr) {
                    tableEmail.row.add( 
                        [
                            data,
                            date,
                            type,
                            staff,
                            frequency,
                            filelocation,
                            relatedskus,
                            ceasedate,
                            notes,
                        ] ).draw();
                    //swal("Success!", "A new Email has been added!", "info");
                    showMessage("alert-success", "A new Email has been added! Date: " + date + "; Type: " + type +"; Staff: "+ staff + "; Frequency: " + frequency + "; File Location: " + filelocation + "; Related SKU/s: " + relatedskus + "; Cease Date: " + ceasedate + "; Notes: " + notes );
                    document.getElementById('em-form').reset();

            });

        }


        function updateEmail(){
            let emid = $("#eem-id").val();
            let edate = $("#eem-date").val();
            let type = $("#eem-type option:selected").val();
            let staff = $("#eem-staff option:selected").val();
            let frequency = $("#eem-frequency").val();
            let filelocation = $("#eem-filelocation").val();
            let relatedskus = getRelatedSkuItemsToBeSentToDb("eem-relatedskus");
            let ceasedate = $("#eem-ceasedate").val();
            let notes = $("#eem-notes").val();
            
            $.post(globalUrl + '/email/update/' + emid, 
                {
                    _token: globalToken,
                    _method: 'PATCH',
                    edate: edate,
                    type: type,
                    staff: staff,
                    frequency: frequency,
                    filelocation: filelocation,
                    relatedskus: relatedskus,
                    ceasedate: ceasedate,
                    notes: notes,
                }, 
                function(data, textStatus, xhr) {
                    
                    
                    var d = tableEmail.row(emRowIndx).data();
                    d[1] = edate;
                    d[2] = type;
                    d[3] = staff;
                    d[4] = frequency;
                    d[5] = filelocation;
                    d[6] = relatedskus;
                    d[7] = ceasedate;
                    d[8] = notes;
                    tableEmail.row(emRowIndx).data(d).draw();

                    //swal("Success!", "The Email info has been updated.", "info");
                    showMessage("alert-success", "A new Email has been added! Date: " + edate + "; Type: " + type +"; Staff: "+ staff + "; Frequency: " + frequency + "; File Location: " + filelocation + "; Related SKU/s: " + relatedskus + "; Cease Date: " + ceasedate + "; Notes: " + notes );
                    document.getElementById('eem-form').reset();

            });
        }


function setModalEventDoubleClickEmail(){
        tableEmail.on('dblclick', 'tbody tr', function() {
                let t = tableEmail.row(this).data();
                
                $("#mEditEmail").modal('toggle');
                document.getElementById('eem-form').reset();

                $("#eem-id").val(t[0]);
                $("#eem-date").val(t[1]);
                $("#eem-type option[value='"+t[2]+"']").prop("selected", "selected");
                $("#eem-staff option[value='"+t[3]+"']").prop("selected", "selected");
                $("#eem-frequency").val(t[4]);
                $("#eem-filelocation").val(t[5]);
                setRelatedSkuSelectedItems(t[6], $("#eem-relatedskus"));
                $("#eem-ceasedate").val(t[7]);
                $("#eem-notes").val(t[8]);
                
                window.emRowIndx = tableEmail.row(this).index();
            });
}

function setModalEventAddEmail(){
        $("#mAddEmail").on('show.bs.modal', function(event) {
                $("em-date").val(formatDate(utc)); 
                $("#mAddEmail #em-relatedskus").selectpicker("deselectAll");
        });
}