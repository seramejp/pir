            
            var utc = new Date().toJSON().slice(0,10).replace(/-/g,'/');
            var pirRowIndx = -1;
            
            function updatepirshow(id,date,type,staff,note,filelocation)
            {
                $("#q-id2").val(id);
                $("#e-pir-date").val(date);
                $("#e-pir-type").val(type);
                $("#e-pir-staff").val(staff);
                $("#e-pir-note").val(note);
                $("#e-pir-filelocation").val(filelocation);
                $('#modalForImprovementEdit').modal('show');
            }


            function addpir()
            {
                var date = $("#pir-date").val();
                var type = $("#pir-type").val();
                var staff = $("#pir-staff").val();
                var note = $("#pir-notes").val();
                var filelocation = $("#pir-filelocation").val();
                var sku_id = $("#d-id").val();

                if(type.trim() == "" || staff.trim() == "" || note.trim() == "")
                {
                    swal("Reminder", "Type, staff and note is required!", "error");
                }
                else
                {
                    $("#spinner4").css("opacity", "1");
                    var data = 
                    {
                        "sku_id":sku_id,
                        "date":date,
                        "type": type,
                        "staff": staff,
                        "note": note,
                        "filelocation": filelocation,
                        "_token": globalToken,
                    }
                    jQuery.ajax
                    (
                      {
                        type: "POST",
                        url: globalUrl+"/pir/add",
                        dataType: "json",
                        data: data,
                        success: function(msg)
                          {
                            swal("Good job!", "PIR successfully added!", "success");
                            //addrow(date,type,staff,note,filelocation,null,null,null,null,null,msg.insertedId);
                            $("#pir-date").val(formatDate(utc));
                            //$("#pir-type").val("");
                            $("#pir-type").val($("#pir-type option:first").val());
                            //$("#pir-staff").val("");
                            $("#pir-staff").val($("#pir-staff option:first").val());
                            $("#pir-notes").val("");
                            $("#pir-filelocation").val("");

                            var table = $("#tablePIR").DataTable();
                                table.clear().draw();

                                if(msg.count != 0)
                                {
                                    for(var x=0;x<msg.count;x++)
                                    {
                                        //alert(msg.date_applied[x] + "|" + msg.q_staff[x] + "|" + msg.q_note[x] + "|" + msg.po_num[x] + "|" + msg.eta[x]);
                                        addrow(msg.date[x],msg.type[x],msg.staff[x],msg.note[x],msg.filelocation[x],msg.date_applied[x],msg.q_staff[x],msg.q_note[x],msg.po_num[x],msg.eta[x],msg.q_id[x]);
                                    }
                                }
                            $("#modalForImprovement").modal('toggle');
                            $("#spinner4").css("opacity", "0");

                          },
                          error: function(ts) 
                          { 
                            // alert(ts.responseText) 
                            $("#tx").val(ts.responseText);
                            //swal("Error", "Invalid PIR", "error");
                            //$("#spinner4").css("opacity", "0");
                          }
                        }
                      );
                }

            }


            function updatepir()
            {
                var date = $("#e-pir-date").val();
                var type = $("#e-pir-type").val();
                var staff = $("#e-pir-staff").val();
                var note = $("#e-pir-note").val();
                var filelocation = $("#e-pir-filelocation").val();
                var sku_id = $("#d-id").val();
                var q_id = $("#q-id2").val();
                var selectedrelatedskus = $("#e-pir-relatedskus :selected");

                var relatedskus ="";
                selectedrelatedskus.each(function(index, el) {
                    relatedskus += el.text + ", ";
                });
                relatedskus=relatedskus.trim().slice(0,-1); 

                //alert(type+"|"+staff+"|"+note);

                if(type.trim() == "" || staff.trim() == "" || note.trim() == "")
                {
                    swal("Reminder", "Type, staff and note is needed!", "error");
                }
                else
                {
                    $("#spinner6").css("opacity", "1");
                    var data = 
                    {
                        "sku_id":sku_id,
                        "q_id":q_id,
                        "date":date,
                        "type": type,
                        "staff": staff,
                        "note": note,
                        "relatedsku": relatedskus,
                        "filelocation": filelocation,
                        "_token": globalToken,
                    }
                    jQuery.ajax
                    (
                      {
                        type: "POST",
                        url: globalUrl+"/pir/update",
                        dataType: "json",
                        data: data,
                        success: function(msg)
                          {
                            swal("Good job!", "PIR successfully added!", "success");
                            
                            var table = $("#tablePIR").DataTable();
                                table.clear().draw();

                            if(msg.count != 0)
                                {
                                    for(var x=0;x<msg.count;x++)
                                    {
                                        addrow(msg.date[x],msg.type[x],msg.staff[x],msg.note[x],msg.relatedsku[x],msg.filelocation[x],msg.date_applied[x],msg.q_staff[x],msg.q_note[x],msg.po_num[x],msg.eta[x],msg.q_id[x]);
                                    }
                                }
                            

                            $("#spinner6").css("opacity", "0");

                          },
                          error: function(ts) 
                          { 
                            // alert(ts.responseText) 
                            // $("#tx").val(ts.responseText);
                            swal("Error", "Invalid PIR", "error");
                            $("#spinner6").css("opacity", "0");
                          }
                        }
                      );
                }

            }


            function formatDate(date) 
            {
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;

                return [year, month, day].join('-');
            }
            

            function addrow(date,type,staff,note,relatedsku,filelocation,date_applied,q_staff,q_note,po_num,eta,id) 
            {
                //alert(q_staff+"|"+q_note+"|"+po_num+"|"+eta+"|"+filelocation+"|"+date_applied);

                $("#tablePIR").dataTable().fnAddData( [
                date,type,staff,note,relatedsku,filelocation,'<button class="btn btn-success" onclick="updatepirshow(\''+id+'\',\''+date+'\',\''+type+'\',\''+staff+'\',\''+note+'\',\''+filelocation+'\')">Edit</button>',date_applied,q_staff,q_note,po_num,eta,'<button class="btn btn-success" onclick="updatepirforpur(\''+id+'\',\''+date_applied+'\',\''+q_staff+'\',\''+q_note+'\',\''+po_num+'\',\''+eta+'\')">Edit</button>'] );
            }


            

           

            

            

            


            function updatepirforpur(id,q_date_applied,q_staff,q_note,q_po_num,q_eta)
            {
                $("#q-id").val(id);

                q_date_applied = "" + q_date_applied;
                q_staff = "" + q_staff;
                q_note = "" + q_note;
                q_po_num = "" + q_po_num;
                q_eta = "" + q_eta;


                if(q_date_applied != "null"){ 
                $("#q-pir-date_applied").val(q_date_applied);}
                else{ 
                $("#q-pir-date_applied").val(formatDate(utc));}

                if(q_staff != "null"){
                $("#q-pir-staff").val(q_staff);}
                else{
                $("#q-pir-staff").val("");}

                if(q_note != "null")
                $("#q-pir-note").val(q_note);
                else
                $("#q-pir-note").val("");

                if(q_po_num != "null" || q_po_num != "")
                $("#q-pir-po_num").val(q_po_num);
                else
                $("#q-pir-po_num").val("");

                if(q_eta != "null")
                $("#q-pir-eta").val(q_eta);
                else
                $("#q-pir-eta").val(formatDate(utc));

                $('#modalForImprovementPur').modal('show');
            }

            function updatepirforpurchasing()
            {
                var date_applied = $("#q-pir-date_applied").val();
                var q_staff = $("#q-pir-staff").val();
                var q_note = $("#q-pir-note").val();
                var po_num = $("#q-pir-po_num").val();
                var eta = $("#q-pir-eta").val();
                var q_id = $("#q-id").val();
                var sku_id = $("#d-id").val();


                if(q_staff.trim() == "" || q_note.trim() == "")
                {
                    swal("Reminder", "Staff and note is needed!", "error");
                }
                else
                {
                    $("#spinner5").css("opacity", "1");
                    var data = 
                    {
                        "date_applied":date_applied,
                        "staff":q_staff,
                        "note": q_note,
                        "po_num": po_num,
                        "eta": eta,
                        "q_id": q_id,
                        "sku_id": sku_id,
                        "_token": globalToken,
                    }
                    jQuery.ajax
                    (
                      {
                        type: "POST",
                        url: globalUrl+"/pir/updateforpur",
                        dataType: "json",
                        data: data,
                        success: function(msg)
                          {
                            swal("Good job!", "PIR successfully updated!", "success");
                            $('#modalForImprovementPur').modal('hide');
                            
                            var table = $("#tablePIR").DataTable();
                                table.clear().draw();

                                if(msg.count != 0)
                                {
                                    for(var x=0;x<msg.count;x++)
                                    {
                                        addrow(msg.date[x],msg.type[x],msg.staff[x],msg.note[x],msg.relatedsku[x],msg.filelocation[x],msg.date_applied[x],msg.q_staff[x],msg.q_note[x],msg.po_num[x],msg.eta[x],msg.q_id[x]);
                                    }
                                }

                            $("#q-pir-date_applied").val(formatDate(utc));
                            $("#q-pir-po_num").val("");
                            $("#q-pir-staff").val("");
                            $("#q-pir-note").val("");
                            $("#q-pir-eta").val(formatDate(utc));
                            
                            $("#spinner5").css("opacity", "0");

                          },
                          error: function(ts) 
                          { 
                            // alert(ts.responseText) 
                            // $("#tx").val(ts.responseText);
                            swal("Error", "Invalid PIR", "error");
                            $("#spinner5").css("opacity", "0");
                          }
                        }
                      );
                }

            }

            

            function deletepir(side){

                swal({
                        title: "Are you sure you want to delete this PIR? This cannot be undone.",
                        icon: "warning",
                        buttons: {
                            delete: "Delete, I understand.",
                            cancel: "Nooo pls dont.",
                        },
                        dangerMode: true,
                })
                .then((value) => {
                    if (value == "delete") {
                        if (side == "left") {
                            let pirId = $("#q-id2").val();

                            $("#modalForImprovementEdit").modal('toggle');
                            
                            $.post(globalUrl + '/pir/delete/leftside/' + pirId, 
                                {
                                    _token: globalToken,
                                    _method: 'POST',
                                }, 
                                function(data, textStatus, xhr) {
                                    var table = $("#tablePIR").DataTable();
                                    table.row(":eq("+ pirRowIndx +")").remove();
                                    table.draw();

                                    swal("Deleted!", "PIR was deleted.", "success");           
                            });

                        } else {
                            let pirId = $("#q-id").val();

                            $("#modalForImprovementPur").modal('toggle');

                            $.post(globalUrl + '/pir/delete/rightside/' + pirId, 
                                {
                                    _token: globalToken,
                                    _method: 'POST',
                                }, 
                                function(data, textStatus, xhr) {
                                    var table = $("#tablePIR").DataTable();

                                    var d = table.row(pirRowIndx).data();
                                    d[6] = d[7] = d[8] = d[9] = d[10] = "";
                                    table.row(pirRowIndx).data(d).draw();

                                    swal("Deleted!", "PIR was deleted.", "success");           
                            });
                        }
                    }
                });

            }