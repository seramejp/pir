var pirRowIndx = -1;

function showMessage(className, message){
		let htmlAlert = '<div class="alert '+className+' fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span>'+message+'</span></div>';
		$("#divAlertWrapper").empty().append(htmlAlert);		
}

function addSpanEditHtmlForJsonData(modalELement, name){
    return '<span class="list-group-item">'+name+'<a class="pull-right" data-toggle="modal" data-target="'+modalELement+'" data-staffname="'+name+'">Edit</a></span>';
}

function loadSelectOptionsFromJson(){
        //$.ajaxSetup({ cache:false });
		$.ajax({
            url: globalUrl + '/json/jsonfile.json',
            type: 'GET',
            dataType: 'JSON',
            async: false,
            cache: false,
            success: function(data){
                    let pirtypeHtml = ""; let pirtypeHtmlLI = "";
                    let staffHtml = ""; let staffHtmlLI = "";
                    let videoTypeHtml = ""; let videoTypeHtmlLI = "";
                    let videostatusHtml = ""; let videostatusHtmlLI = "";
                    let kbapproverHtml = ""; let kbapproverHtmlLI = "";
                    let kbstatusHtml = ""; let kbstatusHtmlLI = "";
                    let emailtypeHtml = "";let emailtypeHtmlLI = "";
                    
                    $.each(data.pirtype, function(index, el) {
                            pirtypeHtml += '<option value="'+el.value+'">'+el.text+'</option>';
                            pirtypeHtmlLI +='<span class="list-group-item">'+el.text+'<a class="pull-right">Edit</a></span>';
                    });
                    $(".pirtype").append(pirtypeHtml); $(".pirtypeli").append(pirtypeHtmlLI); 

                    $.each(data.staff, function(index, el) {
                            staffHtml += '<option value="'+el.value+'">'+el.text+'</option>';
                            staffHtmlLI += addSpanEditHtmlForJsonData("#modalEditStaff", el.text);
                    });
                    $(".allstaff").append(staffHtml); $(".allstaffli").append(staffHtmlLI);

                    $.each(data.videotype, function(index, el) {
                            videoTypeHtml += '<option value="'+el.value+'">'+el.text+'</option>';
                            videoTypeHtmlLI += '<span class="list-group-item">'+el.text+'<a class="pull-right">Edit</a></span>';
                    });
                    $(".videotype").append(videoTypeHtml);$(".videotypeli").append(videoTypeHtmlLI);

                    $.each(data.videostatus, function(index, el) {
                            videostatusHtml += '<option value="'+el.value+'">'+el.text+'</option>';
                            videostatusHtmlLI += '<span class="list-group-item">'+el.text+'<a class="pull-right">Edit</a></span>';
                    });
                    $(".videostatus").append(videostatusHtml); $(".videostatusli").append(videostatusHtmlLI);

                
                    $.each(data.kbapprover, function(index, el) {
                            kbapproverHtml += '<option value="'+el.value+'">'+el.text+'</option>';
                            kbapproverHtmlLI += '<span class="list-group-item">'+el.text+'<a class="pull-right">Edit</a></span>';
                    });
                    $(".kbapprovedby").append(kbapproverHtml); $(".kbapproverli").append(kbapproverHtmlLI);

                    $.each(data.kbstatus, function(index, el) {
                            kbstatusHtml += '<option value="'+el.value+'">'+el.text+'</option>';
                            kbstatusHtmlLI += '<span class="list-group-item">'+el.text+'<a class="pull-right">Edit</a></span>';
                    });
                    $(".kbstatus").append(kbstatusHtml);$(".kbstatusli").append(kbstatusHtmlLI);

                    $.each(data.emailtype, function(index, el) {
                            emailtypeHtml += '<option value="'+el.value+'">'+el.text+'</option>';
                            emailtypeHtmlLI += '<span class="list-group-item">'+el.text+'<a class="pull-right">Edit</a></span>';
                    });
                    $(".emailtype").append(emailtypeHtml);$(".emailtypeli").append(emailtypeHtmlLI);


                
            },

        })
        .fail(function(jqxhr, textStatus, error) {
            console.log("ERROR!: " + textStatus + " : " + error);
        });
}


function loadSettingsPartialSelectOptionsFromJson($loadOnlyThis){
        $.ajax({
            url: globalUrl + '/json/jsonfile.json',
            type: 'GET',
            dataType: 'JSON',
            async: false,
            success: function(data){
                    let pirtypeHtmlLI = "";
                    let staffHtmlLI = "";
                    let videoTypeHtmlLI = "";
                    let videostatusHtmlLI = "";
                    let kbapproverHtmlLI = "";
                    let kbstatusHtmlLI = "";
                    let emailtypeHtmlLI = "";
                    
                    switch($loadOnlyThis){

                        case "pirtype":
                            $.each(data.pirtype, function(index, el) {
                                    pirtypeHtmlLI +='<span class="list-group-item">'+el.text+'<a class="pull-right">Edit</a></span>';
                            });
                            $(".pirtypeli").empty().append(pirtypeHtmlLI); 
                            break;

                        case "allstaff": 
                            $.each(data.staff, function(index, el) {
                                    staffHtmlLI += addSpanEditHtmlForJsonData("#modalEditStaff", el.text);
                            });
                            $(".allstaffli").empty().append(staffHtmlLI);
                            break;

                        case "videotype": 
                            $.each(data.videotype, function(index, el) {
                                    videoTypeHtmlLI += '<span class="list-group-item">'+el.text+'<a class="pull-right">Edit</a></span>';
                            });
                            $(".videotypeli").empty().append(videoTypeHtmlLI);
                            break;

                        case "videostatus": 
                            $.each(data.videostatus, function(index, el) {
                                    videostatusHtmlLI += '<span class="list-group-item">'+el.text+'<a class="pull-right">Edit</a></span>';
                            });
                            $(".videostatusli").empty().append(videostatusHtmlLI);
                            break;

                        case "kbapprovedby": 
                            $.each(data.kbapprover, function(index, el) {
                                    kbapproverHtmlLI += '<span class="list-group-item">'+el.text+'<a class="pull-right">Edit</a></span>';
                            });
                            $(".kbapproverli").empty().append(kbapproverHtmlLI);
                            break;

                        case "kbstatus": 
                            $.each(data.kbstatus, function(index, el) {
                                    kbstatusHtmlLI += '<span class="list-group-item">'+el.text+'<a class="pull-right">Edit</a></span>';
                            });
                            $(".kbstatusli").empty().append(kbstatusHtmlLI);
                            break;

                        case "emailtype": 
                            $.each(data.emailtype, function(index, el) {
                                    emailtypeHtmlLI += '<span class="list-group-item">'+el.text+'<a class="pull-right">Edit</a></span>';
                            });
                            $(".emailtypeli").empty().append(emailtypeHtmlLI);
                            break;

                        default: break;
                    }
                
            },

        })
        .fail(function(jqxhr, textStatus, error) {
            console.log("ERROR!: " + textStatus + " : " + error);
        });
}


function assignEditClickIndexes(){
		tablePIR.on('click', 'tbody tr', function(event) {
        		window.pirRowIndx = tablePIR.row(this).index();
        });
}

var utc = new Date().toJSON().slice(0,10).replace(/-/g,'/');
function formatDate(date) {
    var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

/*
* @param String relatedskusFromDB, DOMElement theSelectElementToSetOptions
* @desc This fn will set the select options given a string of relatedskus from the database
* 
*/
function setRelatedSkuSelectedItems(relatedskusFromDB, theSelectElementToSetOptions){
	let arr = [];
  
    if (relatedskusFromDB != null) {  
    	let relatedskusAll = relatedskusFromDB.split(",");

        for (var i = 0; i < relatedskusAll.length; i++) {

            theSelectElementToSetOptions.find('option').each(function(index, element) {
                if (element.text == relatedskusAll[i].trim()) {
                    arr.push(element.value);
                }
            });
        }

        theSelectElementToSetOptions.selectpicker('deselectAll');
    	theSelectElementToSetOptions.selectpicker('val', arr);

   	}else{
        theSelectElementToSetOptions.selectpicker('deselectAll');
    }
}


/*
* @param DOMElement theSelectElementToGetTheOptions
* @desc This fn will get the selected related skus from the select element and return a string
* 
*/
function getRelatedSkuItemsToBeSentToDb(theSelectElementToGetTheOptions){
		let selectedrelatedskus = $("#"+ theSelectElementToGetTheOptions +" :selected");
		var relatedskus ="";

	    selectedrelatedskus.each(function(index, el) {
	        	relatedskus += el.text + ", ";
	    });

	    return relatedskus.trim().slice(0,-1); 
}



$.fn.dataTable.render.ellipsis = function ( cutoff ) {
    return function ( data, type, row ) {
        if (data != null) {
            return type === 'display' && data.length > cutoff ?
            data.substr( 0, cutoff ) +'…' :
            data;    
        }else{
            return "";
        }
        
    }
};



var tablePIR = $("#tablePIR").DataTable({
    columnDefs: [
        { 
            className: "has-border-right", 
            "targets": [ 7 ] 
        },
        { 
            className: "has-bgcolor-ltblue", 
            "targets": [ 8,9,10,11,12,13 ] 
        },
        {
            targets: [ 0 ],
            visible: false,
            searchable: false
        },
        {
            targets: [4,6,11],
            render: $.fn.dataTable.render.ellipsis(15)
        },
        {
            targets: [5,10],
            render: $.fn.dataTable.render.ellipsis(10)
        }
    ],
    responsive: true,
    select: {
        style: 'single'
    }
});

var tableVideo = $("#tableVideo").DataTable({
    columnDefs: [
        {
            targets: [ 0,5,6,7,9,10,15 ],
            visible: false,
            searchable: false
        },
        {
            targets: [14],
            render: $.fn.dataTable.render.ellipsis(15)
        },
        {
            targets: [3],
            render: $.fn.dataTable.render.ellipsis(10)
        }
    ],
    select: {
        style: 'single'
    }
});


var tableKb = $("#tableKb").DataTable({
    columnDefs: [
        {
            targets: [ 0,3,8,10,11 ],
            visible: false,
            searchable: false
        },
        {
            targets: [5,7],
            render: $.fn.dataTable.render.ellipsis(15)
        },
        {
            targets: [4,6],
            render: $.fn.dataTable.render.ellipsis(10)
        }
    ],
    select: {
        style: 'single'
    }
});

var tableEmail = $("#tableEmail").DataTable({
    columnDefs: [
        {
            targets: [ 0 ],
            visible: false,
            searchable: false
        },
        {
            targets: [8],
            render: $.fn.dataTable.render.ellipsis(15)
        },
        {
            targets: [4,5],
            render: $.fn.dataTable.render.ellipsis(10)
        }
    ],
    select: {
        style: 'single'
    }
});

var tableNotificationSent = $("#notification_sent").DataTable({
    columnDefs: [
        {
            targets: [ 0 ],
            visible: false,
            searchable: false
        },
        {
            targets: [4,6],
            render: $.fn.dataTable.render.ellipsis(20)
        }
    ]
});

var tableNotificationUnsent = $("#notification_unsent").DataTable({
    columnDefs: [
        {
            targets: [ 0 ],
            visible: false,
            searchable: false
        },
        {
            targets: [4,6],
            render: $.fn.dataTable.render.ellipsis(20)
        },
        {
            orderable: false,
            className: 'select-checkbox',
            targets:   7
        }
    ],
    dom: 'Bfrtip',
    select: {
            style:    'multi',
            selector: 'td:last-child'
        },
    buttons: [
        {
            text: 'Send as Email',
            action: function () {
                if (tableNotificationUnsent.rows( { selected: true } ).count() > 0) {
                    var sel = tableNotificationUnsent.rows( { selected: true } );
                    var pirids = "";
                    for (var i = 0; i < sel.data().length; i++) {
                        pirids += sel.data()[i][0].toString() + ",";
                    }
                    pirds = pirids.slice(0,-1);
                    sendEmailNotificationPostMany(pirids);
                }else{
                    swal("Select a row.", "Please select at least one row to send an email notification to.", "error");
                }

            }
        }
    ]
});


/* DOCUMENT ON READY */

$(function() {

		$('[data-toggle="tooltip"]').tooltip(); 
		$('#topdiv').on('keyup', '#searchsku', function(event) {
            if(event.keyCode == 13)
                searchSku();
        });
        $('body').on('hidden.bs.modal', function () {
            $('.modal.in').length > 0 ? $('body').addClass('modal-open') : '';

        });



        loadSelectOptionsFromJson();
        assignEditClickIndexes();
        setModalEventAddPIR();
        setModalEventEditPIR();
        setModalEventAddVideo();
        setModalEventAddEmail();
        setModalEventAddKb();
        setModalEventDoubleClickVideo();
        setModalEventDoubleClickKb();
        setModalEventDoubleClickEmail();
        loadReportsDependenciesOnDocReady();
        modalAddStaffEvents();
        

}); // End document ready