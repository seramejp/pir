
        function searchFromReports(){
            let sku = $("#search-sku").val();
            let tab = $("#search-tab option:selected").val();
            let supplier = $("#search-supplier").val();
            let category = $("#search-category option:selected").val();

            $("#inputSearchReportSku").val(sku);
            $("#inputSearchReportTab").val(tab);
            $("#inputSearchReportSupplier").val(supplier);
            $("#inputSearchReportCategory").val(category);
            $("#formSearchReportSubmit").submit();
            
        
        }

        

        function loadReportsDependenciesOnDocReady(){
            var reportsearchmain = $("#reportsearchmain").DataTable({
                    "bPaginate": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    "ordering": false
            });



            var reportvideomaintable = $("#reportvideomaintable").DataTable({
                    "dom": 'Bfrltip',
                    "buttons": [
                        {
                            extend: 'csv',
                            text: '<i class="fa fa-download"></i> Search and Filtered Results (CSV)',
                            className: 'btn-primary',
                            title: 'PMR_Report_Export_',
                            exportOptions: {
                                columns: [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]
                            }
                        },
                    ],
                    "columnDefs": [
                            {
                                "targets": [ 0, 1, 9,10,11,12,13,14,15,16,17,18,19,20,21 ],
                                "visible": false,
                                "searchable": true
                            },
                            
                        ],
                    "bPaginate": true,
                    "bLengthChange": true,
                    "pageLength": 10,
                    "bFilter": true,
                    "ordering": false,
                    select: {
                        style: 'single'
                    }
            });

            $("<br /><br /><br />").insertAfter($("#reportvideomaintable_wrapper div.dt-buttons"));
            $("#reportvideomaintable_filter").addClass('hidden');
            $('#reportvideomaintable tfoot th').each( function () {
                    var title = $(this).text();
                    $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
            } );

            reportvideomaintable.columns().every( function () {
                var that = this;
         
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );

            $('#reportvideomaintable tbody').off('dblclick');
            $("#reportvideomaintable tbody").on('dblclick', 'tr', function(event) {
                var data = reportvideomaintable.row( this ).data();
                loadJSON_mReportDetails(data);
            });


            var reportpirmaintable = $("#reportpirmaintable").DataTable({
                    "dom": 'Bfrltip',
                    "buttons": [
                        {
                            extend: 'csv',
                            text: '<i class="fa fa-download"></i> Search and Filtered Results (CSV)',
                            className: 'btn-primary',
                            title: 'PMR_Report_Export_',
                            exportOptions: {
                                columns: [2,3,4,5,6,7,8,9,10,11,12,13,14,15]
                            }
                        },
                        
                    ],
                    "columnDefs": [
                            {
                                "targets": [ 0, 1, 9,10,11,12,13,14,15 ],
                                "visible": false,
                                "searchable": true
                            },
                            
                        ],
                    "bPaginate": true,
                    "bLengthChange": true,
                    "pageLength": 10,
                    "bFilter": true,
                    "ordering": false,
                    select: {
                        style: 'single'
                    }
            });

            $("<br /><br /><br />").insertAfter($("#reportpirmaintable_wrapper div.dt-buttons"));
            $("#reportpirmaintable_filter").addClass('hidden');
            $('#reportpirmaintable tfoot th').each( function () {
                    var title = $(this).text();
                    $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
            } );

            reportpirmaintable.columns().every( function () {
                var that = this;
         
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );

            $('#reportpirmaintable tbody').off('dblclick');
            $("#reportpirmaintable tbody").on('dblclick', 'tr', function(event) {
                var data = reportpirmaintable.row( this ).data();
                loadJSON_mReportDetails(data);
            });

            


            var reportkbmaintable = $("#reportkbmaintable").DataTable({
                    "dom": 'Bfrltip',
                    "buttons": [
                        {
                            extend: 'csv',
                            text: '<i class="fa fa-download"></i> Search and Filtered Results (CSV)',
                            className: 'btn-primary',
                            title: 'PMR_Report_Export_',
                            exportOptions: {
                                columns: [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]
                            }
                        },
                        
                    ],
                    "columnDefs": [
                            {
                                "targets": [ 0, 1, 9,10,11,12,13,14,16,17 ],
                                "visible": false,
                                "searchable": true
                            },
                            
                        ],
                    "bPaginate": true,
                    "bLengthChange": true,
                    "pageLength": 10,
                    "bFilter": true,
                    "ordering": false,
                    select: {
                        style: 'single'
                    }
            });

            $("<br /><br /><br />").insertAfter($("#reportkbmaintable_wrapper div.dt-buttons"));
            $("#reportkbmaintable_filter").addClass('hidden');
            $('#reportkbmaintable tfoot th').each( function () {
                    var title = $(this).text();
                    $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
            } );

            reportkbmaintable.columns().every( function () {
                var that = this;
         
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );

            $('#reportkbmaintable tbody').off('dblclick');
            $("#reportkbmaintable tbody").on('dblclick', 'tr', function(event) {
                var data = reportkbmaintable.row( this ).data();
                loadJSON_mReportDetails(data);
            });



            
            var reportemailmaintable = $("#reportemailmaintable").DataTable({
                    "dom": 'Bfrltip',
                    "buttons": [
                        {
                            extend: 'csv',
                            text: '<i class="fa fa-download"></i> Search and Filtered Results (CSV)',
                            className: 'btn-primary',
                            title: 'PMR_Report_Export_',
                            exportOptions: {
                                columns: [2,3,4,5,6,7,8,9,10,11,12,13]
                            }
                        },
                        
                    ],
                    "columnDefs": [
                            {
                                "targets": [ 0, 1, 9,10,11,12,13 ],
                                "visible": false,
                                "searchable": true
                            },
                            
                        ],
                    "bPaginate": true,
                    "bLengthChange": true,
                    "pageLength": 10,
                    "bFilter": true,
                    "ordering": false,
                    select: {
                        style: 'single'
                    }
            });

            $("<br /><br /><br />").insertAfter($("#reportemailmaintable_wrapper div.dt-buttons"));
            $("#reportemailmaintable_filter").addClass('hidden');
            $('#reportemailmaintable tfoot th').each( function () {
                    var title = $(this).text();
                    $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
            } );

            reportemailmaintable.columns().every( function () {
                var that = this;
         
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );

            $('#reportemailmaintable tbody').off('dblclick');
            $("#reportemailmaintable tbody").on('dblclick', 'tr', function(event) {
                var data = reportemailmaintable.row( this ).data();
                loadJSON_mReportDetails(data);
            });



            
            var reportallmaintable = $("#reportallmaintable").DataTable({
                    "dom": 'Bfrltip',
                    "buttons": [
                        {
                            extend: 'csv',
                            text: '<i class="fa fa-download"></i> Search and Filtered Results (CSV)',
                            className: 'btn-primary',
                            title: 'PMR_Report_Export_',
                            exportOptions: {
                                columns: [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]
                            }
                        },
                        
                    ],
                    "columnDefs": [
                            {
                                "targets": [ 0, 1,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36 ],
                                "visible": false,
                                "searchable": true
                            },
                            
                        ],
                    "bPaginate": true,
                    "bLengthChange": true,
                    "pageLength": 10,
                    "bFilter": true,
                    "ordering": false,
                    select: {
                        style: 'single'
                    }
            });

            $("<br /><br /><br />").insertAfter($("#reportallmaintable_wrapper div.dt-buttons"));
            $("#reportallmaintable_filter").addClass('hidden');
            $('#reportallmaintable tfoot th').each( function () {
                    var title = $(this).text();
                    $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
            } );

            reportallmaintable.columns().every( function () {
                var that = this;
         
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );

            $('#reportallmaintable tbody').off('dblclick');
            $("#reportallmaintable tbody").on('dblclick', 'tr', function(event) {
                var data = reportallmaintable.row( this ).data();
                loadJSON_mReportDetails(data);
            });


        } // end LOAD Reports Dependencies


       
        function loadJSON_mReportDetails(tableData){
            $.ajax({
                url: globalUrl + '/json/jsonfile.json',
                type: 'GET',
                dataType: 'JSON',
                async: false,
                success: function(data){
                    //console.log(tableData);

                    switch(tableData[6]){
                        case "Video": $("#mReportDetails_Video").modal(); mReportDetails_Video(data, tableData); break;
                        case "PIR": $("#mReportDetails_Pir").modal(); mReportDetails_Pir(data, tableData); break;
                        case "Email": $("#mReportDetails_Email").modal(); mReportDetails_Email(data, tableData); break;
                        case "KB": $("#mReportDetails_KB").modal(); mReportDetails_KB(data, tableData); break;
                        default: break;
                    }
                },

            })
            .fail(function(jqxhr, textStatus, error) {
                console.log("ERROR!: " + textStatus + " : " + error);
            });
        }


        function mReportDetails_Video(data, table){
            let vid_id = table[1];
            let sku_id = parseInt(table[0]);

                $.ajax({
                    url: globalUrl + '/reports/details/video/' + vid_id,
                    type: 'GET',
                    async: false,
                    success: function(res){

                                $("#rvid-viddate").text(res.video.viddate);
                                $("#rvid-type").text(res.video.type);
                                $("#rvid-staff").text(res.video.staff);
                                $("#rvid-filelocation").text(res.video.filelocation);
                                $("#rvid-filelocation").text(res.video.filelocation);
                                $("#rvid-shootdate").text(res.video.shootdate);
                                $("#rvid-shootlocation").text(res.video.shootlocation);
                                $("#rvid-addedtokb").text( (res.video.addedtokb == "1" ? "Yes" : "No") );
                                $("#rvid-manual").text( (res.video.manual == "1" ? "Yes" : "No") );
                                $("#rvid-customeremail").text( (res.video.customeremail == "1" ? "Yes" : "No") );
                                $("#rvid-youtube").text(res.video.youtube);
                                $("#rvid-title").text(res.video.title);
                                $("#rvid-status").text(res.video.status);
                                $("#rvid-youtubelink").text(res.video.youtubelink);
                                $("#rvid-relatedskus").text(removeLastComma(res.video.relatedskus));

                                $("#mReportDetails_Video #parent-sku").text(table[4]);
                                $("#mReportDetails_Video #parent-supplier").text(table[5]);
                                $("#mReportDetails_Video #parent-wiseid").text(res.video.getsku.wise_id);
                                $("#mReportDetails_Video #parent-description").text(res.video.getsku.desc);
                                $("#mReportDetails_Video #parent-skustatus").text(res.video.getsku.status);
                                $("#mReportDetails_Video #parent-category").text(table[3]);
                    },
                });
                
                
        }



        function mReportDetails_Pir(data, table){
            let pir_id = table[1];
            let sku_id = parseInt(table[0]);


            $.ajax({
                url: globalUrl + '/reports/details/pir/' + pir_id,
                type: 'GET',
                async: false,
                success: function(res){
                            $("#rpir-date").text(res.pir.date);
                            $("#rpir-staff").text(res.pir.staff);
                            $("#rpir-type").text(res.pir.type);
                            $("#rpir-note").text(res.pir.note);
                            $("#rpir-filelocation").text(res.pir.filelocation);
                            $("#rpir-relatedskus").text(removeLastComma(res.pir.relatedsku));

                            $("#rpir-evaldateapplied").text(res.pir.date_applied);
                            $("#rpir-evaleta").text(res.pir.eta);
                            $("#rpir-evalponum").text(res.pir.po_num);
                            $("#rpir-evalstaff").text(res.pir.q_staff);
                            $("#rpir-evalnote").text(res.pir.q_note);

                            $("#mReportDetails_Pir #parent-sku").text(table[4]);
                            $("#mReportDetails_Pir #parent-supplier").text(table[5]);
                            $("#mReportDetails_Pir #parent-wiseid").text(res.pir.sku.wise_id);
                            $("#mReportDetails_Pir #parent-description").text(res.pir.sku.desc);
                            $("#mReportDetails_Pir #parent-skustatus").text(res.pir.sku.status);
                            $("#mReportDetails_Pir #parent-category").text(table[3]);

                },
            });
            
                
        }


        function mReportDetails_Email(data, table){
            let email_id = table[1];
            let sku_id = parseInt(table[0]);


            $.ajax({
                url: globalUrl + '/reports/details/email/' + email_id,
                type: 'GET',
                async: false,
                success: function(res){
                        
                            $("#remail-edate").text(res.email.edate);
                            $("#remail-type").text(res.email.type);
                            $("#remail-staff").text(res.email.staff);
                            $("#remail-frequency").text(res.email.frequency);
                            $("#remail-filelocation").text(res.email.filelocation);
                            $("#remail-ceasedate").text(res.email.ceasedate);
                            $("#remail-notes").text(res.email.notes);
                            $("#remail-relatedskus").text(removeLastComma(res.email.relatedskus));

                            $("#mReportDetails_Email #parent-sku").text(table[4]);
                            $("#mReportDetails_Email #parent-supplier").text(table[5]);
                            $("#mReportDetails_Email #parent-wiseid").text(res.email.getsku.wise_id);
                            $("#mReportDetails_Email #parent-description").text(res.email.getsku.desc);
                            $("#mReportDetails_Email #parent-skustatus").text(res.email.getsku.status);
                            $("#mReportDetails_Email #parent-category").text(table[3]);

                },
            });
        }


        function mReportDetails_KB(data, table){
            let kb_id = table[1];
            let sku_id = parseInt(table[0]);


            $.ajax({
                url: globalUrl + '/reports/details/kb/' + kb_id,
                type: 'GET',
                async: false,
                success: function(res){
                            $("#rkb-kbid").val(kb_id);
                            $("#rkb-kbdate").text(res.kb.kbdate);
                            $("#rkb-ticketid").text(res.kb.ticket_id);
                            $("#rkb-submitted_by").text(res.kb.submitted_by);
                            $("#rkb-issue").text(res.kb.issue);
                            $("#rkb-advice").text(res.kb.advice);
                            $("#rkb-manual_link").text(res.kb.manual_link);
                            $("#rkb-approved_by").text(res.kb.approved_by);
                            $("#rkb-status").text(res.kb.status);
                            $("#rkb-added_by").text(res.kb.added_by);
                            $("#rkb-date_added").text(res.kb.date_added);
                            $("#rkb-relatedskus").text(removeLastComma(res.kb.relatedsku));

                            $("#mReportDetails_KB #parent-sku").text(table[4]);
                            $("#mReportDetails_KB #parent-supplier").text(table[5]);
                            $("#mReportDetails_KB #parent-wiseid").text(res.kb.getsku.wise_id);
                            $("#mReportDetails_KB #parent-description").text(res.kb.getsku.desc);
                            $("#mReportDetails_KB #parent-skustatus").text(res.kb.getsku.status);
                            $("#mReportDetails_KB #parent-category").text(table[3]);

                },
            });
        }

        function removeLastComma(str) {
            if (str != "" || str != null) {
                return str.replace(/,(\s+)?$/, '');       
            }
            return "";
        }



        function showKbUpdateFromReports(){
                let kbid = $("#rkb-kbid").val();
                let kbdate = $("#rkb-kbdate").text();
                let kbticketid = $("#rkb-ticketid").text();
                let kbsubmittedby = $("#rkb-submitted_by").text();
                let kbissue = $("#rkb-issue").text();
                let kbadvice = $("#rkb-advice").text();
                let kbmanuallink = $("#rkb-manual_link").text();
                let kbapprovedby = $("#rkb-approved_by").text();
                let kbstatus = $("#rkb-status").text();
                let kbaddedby = $("#rkb-added_by").text();
                let kbdateadded = $("#rkb-date_added").text();
                let kbrelatedsku = $("#rkb-relatedskus").text();

                $("#mEditKbReports").modal("toggle");


                document.getElementById("ekbr-form").reset();
                $("#ekbr-relatedsku").selectpicker("deselectAll");
                setRelatedSkuSelectedItems(kbrelatedsku, $("#ekbr-relatedsku"));

                $("#ekbr-id").val(kbid);
                $("#ekbr-date").val(kbdate);
                $("#ekbr-subby").val(kbsubmittedby);
                $("#ekbr-ticketid").val(kbticketid);
                $("#ekbr-issue").val(kbissue);
                $("#ekbr-advice").val(kbadvice);
                
                $("#ekbr-manlink").val(kbmanuallink);
                $("#ekbr-approvedby").val(kbapprovedby);
                $("#ekbr-status").val(kbstatus);
                $("#ekbr-addedby").val(kbaddedby);
                $("#ekbr-dateadded").val(kbdateadded);
                
        }


        function updateKbFromReports(){
            let id = $("#ekbr-id").val();
            let edate = $("#ekbr-date").val();
            let submitted_by = $("#ekbr-subby").val();
            let ticket_id = $("#ekbr-ticketid").val();
            let issue = $("#ekbr-issue").val();
            let advice = $("#ekbr-advice").val();
            let manual_link = $("#ekbr-manlink").val();
            let approved_by = $("#ekbr-approvedby").val();
            let status = $("#ekbr-status").val();
            let added_by = $("#ekbr-addedby").val();
            let date_added = $("#ekbr-dateadded").val();
            let relatedskus = getRelatedSkuItemsToBeSentToDb("ekbr-relatedsku");
            
            
            $.post(globalUrl + '/kb/update/' + id, 
                {
                    _token: globalToken,
                    _method: 'PATCH',
                    edate:edate,
                    id: id,
                    submitted_by: submitted_by,
                    ticket_id: ticket_id,
                    issue: issue,
                    advice: advice,
                    relatedsku: relatedskus,
                    manual_link: manual_link,
                    approved_by: approved_by,
                    status: status,
                    added_by: added_by,
                    date_added: date_added,
                }, 
                function(data, textStatus, xhr) {
                    
                    if (textStatus == "success") {
                        //swal("Success!", "The kb information has been updated.", "info");
                        
                        swal("Success!", "The kb information has been updated.", "info")
                            .then((value) => {
                              location.reload();
                        });

                    }

                    
                    document.getElementById('ekbr-form').reset();

            });
        }