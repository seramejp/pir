

function addCategory(){
	
		let category = $("#e-category").val();

		if(category.trim() == ""){
            	swal("Required Fields.", "Category name is needed!", "error");
        }else {

        		$.ajax({
        			url: globalUrl+"/category/add",
        			type: 'POST',
                    data: {
                        _token: globalToken,
                        _method: 'POST',
                        category: category,
                    },
        		})
        		.done(function() {
        				let htmlCategory = "<option value='"+category+"'>"+category+"</option>";
        				$(".categoryselect").append(htmlCategory);

        				showMessage("alert-success", "Successfully created a Category: " + category + ".");
        		})
        		.fail(function() {
	        			console.log("failed in adding a category");
	        			showMessage("alert-danger", "Failed in adding category.");
        		})
        		.always(function(){
        				$("#e-category").val("");
        		});
        		
        }
}